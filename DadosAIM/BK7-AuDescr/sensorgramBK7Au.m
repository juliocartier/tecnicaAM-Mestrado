close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataBK7Au_A);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3] = get_D(dataBK7Au_D);

DTS9 = get_DTS(resultA1(:,1:1));
iDTS100 = get_MonolayerSPR(resultA1(:,1:1), 0.00001);
iDTS200 = get_MonolayerSPR(resultA1(:,1:1), 0.009);
iDTS300 = get_MonolayerSPR(resultA1(:,1:1), 0.07);


iDTS9 = get_descrit_inver(DTS9);

idx = iDTS9 > 0;
result_inv_A = idx.*iDTS9;
 
% idx2 = iDTS100 > 0;
% result_inv100 = idx2.*iDTS100;
% 
% idx3 = iDTS200 > 0;
% result_inv200 = idx3.*iDTS200;
% 
% idx4 = iDTS300 > 0;
% result_inv300 = idx4.*iDTS300;
% 
% errors = [0.025; 0.0009; 0.0001; 0.00005];
% 
% names = {'9'; '100'; '200'; '300'};
% 
% figure(264115)
% plot(errors,'.r');
% set(gca,'xtick',[1:4],'xticklabel',names)
% xlabel('Qtd. Coeficientes');
% ylabel('$ERM (\hat{\Theta})$','interpreter','latex')

resultPlot = [98 result_inv_A(98); 291 result_inv_A(291); 490 result_inv_A(490);
              641 result_inv_A(641); 813 result_inv_A(813); 1161 result_inv_A(1161);
              1465 result_inv_A(1465); 1727 result_inv_A(1727); 1987 result_inv_A(1987)];

figure(222)
plot(resultA1(:,1:1));
hold on;
plot(find(idx~=0), result_inv_A(idx~=0), '.');
%hold on;
%plot(resultPlot(:,1:1), resultPlot(:,2:2), '.', 'MarkerSize', 20);
% hold on;
% plot(find(idx2~=0), result_inv100(idx2~=0));
% hold on;
% plot(find(idx3~=0), result_inv200(idx3~=0));
% hold on;
% plot(find(idx4~=0), result_inv300(idx4~=0));
xlabel('Tempo (s)');
ylabel('\theta_r (º)');

% RegimePer = {'RP-1';'RP-2';'RP-3';'RP-4';'RP-5'; 'RP-6'; 'RP-7'; 'RP-8'; 'RP-9'};
% T = table(real(DTS9(1:2:end)), imag(DTS9(1:2:end)),'RowNames', RegimePer);
% 
% figure(987)
% uitable('Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
%     'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);

% accuracyC = [];
% res = [];
% class = [];

% for i = 1:1:length(resultDCT(:,1:1))
%     a = resultDCT(i:i,1:end);
%     result = setdiff(resultDCT, a, 'stable', 'rows');
%     
%     mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors',  3, 'Distance' , 'euclidean');
%     res = predict(mdl, a(:,1:end-1));
%     
%     class = a(:,end:end);
%    
%     accuracyC = [accuracyC; sum(class == res) / numel(class)];
%     
% end
%  
% accuracy = sum(accuracyC)./length(resultDCT(:,1:1))