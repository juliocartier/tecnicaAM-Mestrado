function [ result ] = get_DTS( data )
%GET_INVDTS Summary of this function goes here
%   Detailed explanation goes here

result = [complex(mean(data(1:187,1:1)), length(data(1:187,1:1)));
                   complex(mean(data(188:212,1:1)), length(data(188:212,1:1)));
                   complex(mean(data(213:365,1:1)), length(data(213:365,1:1)));
                   complex(mean(data(366:470,1:1)), length(data(366:470,1:1)));
                   complex(mean(data(471:548,1:1)), length(data(471:548,1:1)));
                   complex(mean(data(549:575,1:1)), length(data(549:575,1:1)));
                   complex(mean(data(576:712,1:1)), length(data(576:712,1:1)));
                   complex(mean(data(713:742,1:1)), length(data(713:742,1:1)));
                   complex(mean(data(743:875,1:1)), length(data(743:875,1:1)));
                   complex(mean(data(876:1051,1:1)), length(data(876:1051,1:1)));
                   complex(mean(data(1052:1264,1:1)), length(data(1052:1264,1:1)));
                   complex(mean(data(1265:1362,1:1)), length(data(1265:1362,1:1)));
                   complex(mean(data(1363:1556,1:1)), length(data(1363:1556,1:1)));
                   complex(mean(data(1557:1579,1:1)), length(data(1557:1579,1:1)));
                   complex(mean(data(1580:1850,1:1)), length(data(1580:1850,1:1)));
                   complex(mean(data(1851:1917,1:1)), length(data(1851:1917,1:1)));
                   complex(mean(data(1918:2069,1:1)), length(data(1918:2069,1:1)))];

end

