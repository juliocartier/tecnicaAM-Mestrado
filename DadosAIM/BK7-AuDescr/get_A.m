function [resultData, resultData1, resultData2, resultData3] = get_A(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = data;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 1263, 1355, 50);
result4 = get_region(result4Aux, 1851, 1918, 50);

result5 = get_region(data, 366, 471, 50);

result6Aux = get_region(data, 366, 471, 50);
result6AAux = get_region(result6Aux, 713, 768, 20);
result6 = get_region(result6AAux, 1847, 1918, 50);

result7 = result5 + 0.1;
%result7 = data + 3.5;
result7(1452) = result7(1452) + 0.3;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:1557) - 0.5;

result11 = resultAux1;

[result22, result33] = get_create(resultAux1);

result44Aux = get_region(resultAux1, 366, 471, 50);
result44 = get_region(result44Aux, 713, 768, 28);

result55 = get_region(resultAux1, 1264, 1363, 50);

result66Aux = get_region(resultAux1, 371, 471, 50);
result66 = get_region(result66Aux, 1264, 1363, 60);

result77 = result66 + 0.2;
result77(455) = result77(455) + 0.4;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result111 = data(1:876) + 0.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 361, 471, 25);

result555 = get_region(result111, 713, 776, 25);

result667Aux = get_region(result111, 361, 471, 35); 
result667 = get_region(result667Aux, 713, 776, 20);

result777 = result333;
result777(640) = result777(640) + 0.4;

%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:549) + 1;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 361, 471, 30);

result5555 = result2222 + 0.2;
result5555(90) = result5555(90) + 0.5;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22+0.1 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end