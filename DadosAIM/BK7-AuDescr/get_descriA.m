function [result] = get_descriA(data)
 %  result = [];
 
 %for i = 1:1:length(data(:,1:1))
     %if (i <= 188)
     
        %complex(mean(data(1:i)), length(data(1:i)))
        result1 = [complex(mean(data(1:187,1:1)), length(data(1:187,1:1)));
                   complex(mean(data(188:212,1:1)), length(data(188:212,1:1)));
                   complex(mean(data(213:365,1:1)), length(data(213:365,1:1)));
                   complex(mean(data(366:426,1:1)), length(data(366:426,1:1)));
                   complex(mean(data(427:548,1:1)), length(data(427:548,1:1)));
                   complex(mean(data(549:575,1:1)), length(data(549:575,1:1)));
                   complex(mean(data(576:712,1:1)), length(data(576:712,1:1)));
                   complex(mean(data(713:742,1:1)), length(data(713:742,1:1)));
                   complex(mean(data(743:875,1:1)), length(data(743:875,1:1)));
                   complex(mean(data(876:986,1:1)), length(data(876:986,1:1)));
                   complex(mean(data(987:1214,1:1)), length(data(987:1214,1:1)));
                   complex(mean(data(1215:1305,1:1)), length(data(1215:1305,1:1)));
                   complex(mean(data(1306:1556,1:1)), length(data(1306:1556,1:1)));
                   complex(mean(data(1557:1580,1:1)), length(data(1557:1580,1:1)));
                   complex(mean(data(1581:1850,1:1)), length(data(1581:1850,1:1)));
                   complex(mean(data(1851:1917,1:1)), length(data(1851:1917,1:1)));
                   complex(mean(data(1918:2069,1:1)), length(data(1918:2069,1:1)))];
               
        result2 = [complex(mean(data(1:187,2:2)), length(data(1:187,2:2)));
                   complex(mean(data(188:212,2:2)), length(data(188:212,2:2)));
                   complex(mean(data(213:365,2:2)), length(data(213:365,2:2)));
                   complex(mean(data(366:426,2:2)), length(data(366:426,2:2)));
                   complex(mean(data(427:548,2:2)), length(data(427:548,2:2)));
                   complex(mean(data(549:575,2:2)), length(data(549:575,2:2)));
                   complex(mean(data(576:712,2:2)), length(data(576:712,2:2)));
                   complex(mean(data(713:742,2:2)), length(data(713:742,2:2)));
                   complex(mean(data(743:875,2:2)), length(data(743:875,2:2)));
                   complex(mean(data(876:1051,2:2)), length(data(876:1051,2:2)));
                   complex(mean(data(1052:1263,2:2)), length(data(1052:1263,2:2)));
                   complex(mean(data(1264:1362,2:2)), length(data(1264:1362,2:2)));
                   complex(mean(data(1363:1556,2:2)), length(data(1363:1556,2:2)));
                   complex(mean(data(1557:1580,2:2)), length(data(1557:1580,2:2)));
                   complex(mean(data(1581:1850,2:2)), length(data(1581:1850,2:2)));
                   complex(mean(data(1851:1902,2:2)), length(data(1851:1902,2:2)));
                   complex(mean(data(1903:2069,2:2)), length(data(1903:2069,2:2)))];
               
         result3 = [complex(mean(data(1:187,3:3)), length(data(1:187,3:3)));
                   complex(mean(data(188:212,3:3)), length(data(188:212,3:3)));
                   complex(mean(data(213:365,3:3)), length(data(213:365,3:3)));
                   complex(mean(data(366:426,3:3)), length(data(366:426,3:3)));
                   complex(mean(data(427:548,3:3)), length(data(427:548,3:3)));
                   complex(mean(data(549:575,3:3)), length(data(549:575,3:3)));
                   complex(mean(data(576:712,3:3)), length(data(576:712,3:3)));
                   complex(mean(data(713:742,3:3)), length(data(713:742,3:3)));
                   complex(mean(data(743:875,3:3)), length(data(743:875,3:3)));
                   complex(mean(data(876:1051,3:3)), length(data(876:1051,3:3)));
                   complex(mean(data(1052:1263,3:3)), length(data(1052:1263,3:3)));
                   complex(mean(data(1264:1362,3:3)), length(data(1264:1362,3:3)));
                   complex(mean(data(1363:1556,3:3)), length(data(1363:1556,3:3)));
                   complex(mean(data(1557:1580,3:3)), length(data(1557:1580,3:3)));
                   complex(mean(data(1581:1850,3:3)), length(data(1581:1850,3:3)));
                   complex(mean(data(1851:1902,3:3)), length(data(1851:1902,3:3)));
                   complex(mean(data(1903:2069,3:3)), length(data(1903:2069,3:3)))];

         result4 = [complex(mean(data(1:187,4:4)), length(data(1:187,4:4)));
                   complex(mean(data(188:212,4:4)), length(data(188:212,4:4)));
                   complex(mean(data(213:365,4:4)), length(data(213:365,4:4)));
                   complex(mean(data(366:426,4:4)), length(data(366:426,4:4)));
                   complex(mean(data(427:548,4:4)), length(data(427:548,4:4)));
                   complex(mean(data(549:575,4:4)), length(data(549:575,4:4)));
                   complex(mean(data(576:712,4:4)), length(data(576:712,4:4)));
                   complex(mean(data(713:742,4:4)), length(data(713:742,4:4)));
                   complex(mean(data(743:875,4:4)), length(data(743:875,4:4)));
                   complex(mean(data(876:1051,4:4)), length(data(876:1051,4:4)));
                   complex(mean(data(1052:1214,4:4)), length(data(1052:1214,4:4)));
                   complex(mean(data(1215:1280,4:4)), length(data(1215:1280,4:4)));
                   complex(mean(data(1281:1556,4:4)), length(data(1281:1556,4:4)));
                   complex(mean(data(1557:1580,4:4)), length(data(1557:1580,4:4)));
                   complex(mean(data(1581:1800,4:4)), length(data(1581:1800,4:4)));
                   complex(mean(data(1801:1853,4:4)), length(data(1801:1853,4:4)));
                   complex(mean(data(1854:2069,4:4)), length(data(1854:2069,4:4)))];
                           
          result5 = [complex(mean(data(1:187,5:5)), length(data(1:187,5:5)));
                   complex(mean(data(188:212,5:5)), length(data(188:212,5:5)));
                   complex(mean(data(213:315,5:5)), length(data(213:315,5:5)));
                   complex(mean(data(316:377,5:5)), length(data(316:377,5:5)));
                   complex(mean(data(378:548,5:5)), length(data(378:548,5:5)));
                   complex(mean(data(549:575,5:5)), length(data(549:575,5:5)));
                   complex(mean(data(576:712,5:5)), length(data(576:712,5:5)));
                   complex(mean(data(713:742,5:5)), length(data(713:742,5:5)));
                   complex(mean(data(743:875,5:5)), length(data(743:875,5:5)));
                   complex(mean(data(876:1051,5:5)), length(data(876:1051,5:5)));
                   complex(mean(data(1052:1263,5:5)), length(data(1052:1263,5:5)));
                   complex(mean(data(1264:1362,5:5)), length(data(1264:1362,5:5)));
                   complex(mean(data(1363:1556,5:5)), length(data(1363:1556,5:5)));
                   complex(mean(data(1557:1580,5:5)), length(data(1557:1580,5:5)));
                   complex(mean(data(1581:1850,5:5)), length(data(1581:1850,5:5)));
                   complex(mean(data(1851:1902,5:5)), length(data(1851:1902,5:5)));
                   complex(mean(data(1903:2069,5:5)), length(data(1903:2069,5:5)))];
               
               
               result = [result1 result2 result3 result4 result5];
     %end
end