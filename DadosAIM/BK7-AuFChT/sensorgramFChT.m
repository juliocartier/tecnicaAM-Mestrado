close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataBK7Au_A);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3] = get_D(dataBK7Au_D);

resultFChT = fct2(resultA1(:,1:1), 1);

inverseFChT = ifct2(resultFChT(1:9,1:1), 2069);
inverseFChT100 = ifct2(resultFChT(1:100,1:1), 2069);
inverseFChT200 = ifct2(resultFChT(1:200,1:1), 2069);
inverseFChT300 = ifct2(resultFChT(1:300,1:1), 2069);

% inverseFChTToT = ifct2(resultFChT(:,1:1), 2069);

names = {'9'; '100'; '200'; '300'};

err9 = mse(inverseFChT(2069:-1:1), resultA1(:,1:1));
err100 = mse(inverseFChT100(2069:-1:1), resultA1(:,1:1));
err200 = mse(inverseFChT200(2069:-1:1), resultA1(:,1:1));
err300 = mse(inverseFChT300(2069:-1:1), resultA1(:,1:1));

errors = [err9; err100; err200; err300];

figure(2645)
plot(errors,'.r');
set(gca,'xtick',[1:4],'xticklabel',names)

figure(1234547)
plot(inverseFChT(end:-1:1) + 21.4577);
hold on;
plot(inverseFChT100(end:-1:1) + 10.9581);
hold on;
plot(inverseFChT200(end:-1:1) + 4.7579);
hold on;
plot(inverseFChT300(end:-1:1));



% accuracyC = [];
% res = [];
% class = [];

% for i = 1:1:length(resultDCT(:,1:1))
%     a = resultDCT(i:i,1:end);
%     result = setdiff(resultDCT, a, 'stable', 'rows');
%     
%     mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors',  3, 'Distance' , 'euclidean');
%     res = predict(mdl, a(:,1:end-1));
%     
%     class = a(:,end:end);
%    
%     accuracyC = [accuracyC; sum(class == res) / numel(class)];
%     
% end
%  
% accuracy = sum(accuracyC)./length(resultDCT(:,1:1))