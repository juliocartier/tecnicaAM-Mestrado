close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataBK7Au_A);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3, resultD4] = get_D(dataBK7Au_D);

resultCHTA1 = [];
resultCHTA2 = [];
resultCHTA3 = [];
resultCHTA4 = [];

resultCHTB1 = [];
resultCHTB2 = [];
resultCHTB3 = [];
resultCHTB4 = [];

resultCHTC1 = [];
resultCHTC2 = [];
resultCHTC3 = [];

resultCHTD1 = [];
resultCHTD2 = [];
resultCHTD3 = [];
resultCHTD4 = [];

for i = 1:1:7
    resultCHTA1 = [resultCHTA1; fct2(resultA1(:,i:i),1)'];
    resultCHTA2 = [resultCHTA2; fct2(resultA2(:,i:i),1)'];
    resultCHTA3 = [resultCHTA3; fct2(resultA3(:,i:i),1)'];
    
    resultCHTB1 = [resultCHTB1; fct2(resultB1(:,i:i),1)'];
    resultCHTB2 = [resultCHTB2; fct2(resultB2(:,i:i),1)'];
    resultCHTB3 = [resultCHTB3; fct2(resultB3(:,i:i),1)'];
    
    resultCHTC1 = [resultCHTC1; fct2(resultC1(:,i:i),1)'];
    resultCHTC2 = [resultCHTC2; fct2(resultC2(:,i:i),1)'];
    resultCHTC3 = [resultCHTC3; fct2(resultC3(:,i:i),1)'];
    
    resultCHTD1 = [resultCHTD1; fct2(resultD1(:,i:i),1)'];
    resultCHTD2 = [resultCHTD2; fct2(resultD2(:,i:i),1)'];
    resultCHTD3 = [resultCHTD3; fct2(resultD3(:,i:i),1)'];
end

for j = 1:1:5
    resultCHTA4 = [resultCHTA4; fct2(resultA4(:,j:j),1)'];
    resultCHTB4 = [resultCHTB4; fct2(resultB4(:,j:j),1)'];
    resultCHTD4 = [resultCHTD4; fct2(resultD4(:,j:j),1)'];
end

tamanho = 4;

resultCHT = [abs(resultCHTA1(:,2:tamanho)) repelem(1,7)'; abs(resultCHTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultCHTA3(:,2:tamanho)) repelem(3,7)'; abs(resultCHTA4(:,2:tamanho)) repelem(4,5)';
             abs(resultCHTB1(:,2:tamanho)) repelem(5,7)'; abs(resultCHTB2(:,2:tamanho)) repelem(6,7)';
             abs(resultCHTB3(:,2:tamanho)) repelem(7,7)'; abs(resultCHTB4(:,2:tamanho)) repelem(8,5)';
             abs(resultCHTC1(:,2:tamanho)) repelem(9,7)'; abs(resultCHTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultCHTC3(:,2:tamanho)) repelem(11,7)'; abs(resultCHTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultCHTD2(:,2:tamanho)) repelem(13,7)'; abs(resultCHTD3(:,2:tamanho)) repelem(14,7)';
             abs(resultCHTD4(:,2:tamanho)) repelem(15,5)'];
figure(4532)
plot3(resultCHT(1:7,1:1), resultCHT(1:7,2:2), resultCHT(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultCHT(27:33,1:1), resultCHT(27:33,2:2), resultCHT(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
hold on;
plot3(resultCHT(53:59,1:1), resultCHT(53:59,2:2), resultCHT(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
hold on;
plot3(resultCHT(74:80,1:1), resultCHT(74:80,2:2), resultCHT(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
hold on;
plot3(resultCHT(8:14,1:1), resultCHT(8:14,2:2), resultCHT(8:14,3:3), 'ob', 'MarkerSize', 8);
hold on;
plot3(resultCHT(34:40,1:1), resultCHT(34:40,2:2), resultCHT(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
hold on;
plot3(resultCHT(60:66,1:1), resultCHT(60:66,2:2), resultCHT(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
hold on;
plot3(resultCHT(81:87,1:1), resultCHT(81:87,2:2), resultCHT(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
hold on;
plot3(resultCHT(15:21,1:1), resultCHT(15:21,2:2), resultCHT(15:21,3:3), '+g', 'MarkerSize', 8);
hold on;
plot3(resultCHT(41:47,1:1), resultCHT(41:47,2:2), resultCHT(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
hold on;
plot3(resultCHT(67:73,1:1), resultCHT(67:73,2:2), resultCHT(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
hold on;
plot3(resultCHT(88:94,1:1), resultCHT(88:94,2:2), resultCHT(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
hold on;
plot3(resultCHT(22:26,1:1), resultCHT(22:26,2:2), resultCHT(22:26,3:3), '*k', 'MarkerSize', 8);
hold on;
plot3(resultCHT(48:52,1:1), resultCHT(48:52,2:2), resultCHT(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
hold on;
plot3(resultCHT(95:99,1:1), resultCHT(95:99,2:2), resultCHT(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
legend('Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5', 'Group 6', 'Group 7', 'Group 8', ...
    'Group 9', 'Group 10', 'Group 11', 'Group 12', 'Group 13', 'Group 14', 'Group 15');
       

result = [];
accuracyC = [];
acc = [];

c = cvpartition(resultCHT(:,1:1),'LeaveOut');

for i = 1:1:c.NumTestSets
    a = resultCHT(i:i,1:end);
    result = setdiff(resultCHT, a, 'stable', 'rows');
    
    mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 5, 'Distance' , 'cosine');
    res = predict(mdl, a(:,1:end-1));
    
    acc = [acc; res];
    
    class = a(:,end:end);
  
    accuracyC = [accuracyC; sum(class == res) / numel(class)];

end

accuracy = sum(accuracyC)./length(resultCHT(:,1:1))

B = 2000;

ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student'); 

mediaFChT = mean(ci);
errorFChT = ci(1) - mediaFChT;
         
figure(6887)
errorbar(mediaFChT, errorFChT, 'b');
ylabel('Confidence Interval');