function [resultNewData, resultData] = get_create(data)

resultNewData = [];
resultData = [];

for i=1:length(data)
    if (isempty(resultNewData))
        resultNewData = data - 0.8;
    else
        resultData = awgn(data + 0.8, 70);
    end
end