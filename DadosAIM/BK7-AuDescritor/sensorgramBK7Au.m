close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[group_result_A1, group_result_A2, group_result_A3] = get_groupA(dataBK7Au_A);

[group_result_B1, group_result_B2, group_result_B3] = get_groupB(dataBK7Au_B);

[group_result_C1, group_result_C2, group_result_C3] = get_groupC(dataBK7Au_C);

[group_result_D1, group_result_D2, group_result_D3] = get_groupD(dataBK7Au_D);

data_valor = get_data_descrit(dataBK7Au_A);

resultDescritA = get_descriA(group_result_A1);

resultInverA = get_inverDescrit(resultDescritA(:,1:1));
 
% resultDescritB = get_descriB(group_result_B1(:,1:5));

% resultDescritC = get_descriC(group_result_C1(:,1:5));

% resultDescritD = get_descriD(group_result_D1(:,1:5));
 
% resultDescr = [resultDescritA(1:2:17,:)' real(repelem(1,5))'; 
%                resultDescritB(1:2:17,:)' real(repelem(2,5))';
%                resultDescritC(1:2:17,:)' real(repelem(3,5))'
%                resultDescritD(1:2:17,:)' real(repelem(4,5))'];
%            
           
          
% mdl = fitcknn(real(resultDescr(1:19,1:9)), real(resultDescr(1:19,10:10)), 'NumNeighbors', 3);
% res = predict(mdl, real(resultDescr(20:20,1:9)))

% resultDescr = [real(resultDescrit(1:2:17,:))' real(resultDescrit(17:17,:))' repelem(1,5)'; 
%                real(resultDescritB(1:2:17,:))' real(resultDescritB(17:17,:))' repelem(2,5)';
%                real(resultDescritC(1:2:17,:))' real(resultDescritC(17:17,:))' repelem(3,5)'
%                real(resultDescritD(1:2:17,:))' real(resultDescritD(17:17,:))' repelem(4,5)'];

% accuracyC = get_knn(resultDescr, 3, 8, 9);

% figure(33333333);
% hold on;
% for j=1:2:length(resultDescrit(:, 1:1))
%     plot(real(resultDescrit(j, 1:1)), imag(resultDescrit(j, 1:1)), '.', 'MarkerSize', 18);
% end
% accuracyC = [];
% xx = 9;
% y = 10;

% for j = 1:1:10
%     
%     aux = repelem(randperm(20,14),1)';
%     x = sort(aux);
%     train = [];
%     test = [];
%     
%   
%     for i=1:1:length(resultDescr)
%        if find(i==x)
%         train = [train; resultDescr(i:i,1:y)];
%        else
%         test = [test; resultDescr(i:i,1:y)];
%        end
%     end
% 
%   mdl = fitcknn(train(:,1:xx), train(:,y:y), 'NumNeighbors', 5);
%   res = predict(mdl, test(:,1:xx))
%   class = test(:,y:y);
%   accuracyC = [accuracyC; sum(class == res) / numel(class)];
% end
% 
% accuracyCAux = sum(accuracyC)./10


% resultDesKNN = get_inverDescrit(resultDescrit(:,1:1));

% idx = resultDesKNN > 0;
% value_yIDX = resultDesKNN.*idx;

figure(222)
plot(group_result_A1(:,1:1));
hold on;
plot(resultInverA, '.');
% hold on;
% plot(resultDesKNN, '.r');
% hold on;
% plot(find(idx~=0),value_yIDX(idx~=0),'r');

