function [result] = get_knn(data, k, x, y)

aux = repelem(randperm(20,14),1)';
x = sort(aux);
train = [];
test = [];
result = [];
for j = 1:1:10
   for i=1:1:length(data)
       if find(i==x)
        train = [train; data(i:i,1:y)];
       else
        test = [test; data(i:i,1:y)];
       end
    end

  mdl = fitcknn(train(:,1:x), train(:,y:y), 'NumNeighbors', k);
  res = predict(mdl, test(:,1:x))
     
  class = test(:,y:y)
  
  result = [result; sum(class == res) / numel(class)];
end