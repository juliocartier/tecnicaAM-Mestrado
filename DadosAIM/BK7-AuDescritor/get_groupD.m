function [resultData, resultData2, resultData3] = get_groupD(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = get_region(data, 937, 1036, 50) + 0.6;

[result2, result3] = get_create(data - 0.2);

result4Aux = get_region(data, 366, 449, 50) + 0.4;
result4 = get_region(result4Aux, 1524, 1591, 50);

result5 = get_region(data, 1907, 1937, 50) - 0.2;

result6Aux = get_region(data, 371, 471, 50) - 0.4;
result6AAux = get_region(result6Aux, 1521, 1591, 50);
result6 = get_region(result6AAux, 1907, 1937, 50);
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = get_region_time(data, 1520, 1591, 188);

[result22, result33] = get_create(result11 - 0.2);

result44Aux = get_region(result11, 937, 1036, 70) + 0.4;
result44 = get_region(result44Aux, 1332, 1404, 20);

result55 = get_region(result11, 366, 449, 60) - 0.3;

result66Aux = get_region(result11, 366, 449, 50) + 0.45;
result66 = get_region(result66Aux, 937, 1036, 50);
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111Aux = data(1:1230);
resultAux111 = get_region_time(result111Aux, 366, 499, 100);
resultAux1111 = get_region_time(resultAux111, 836, 936, 50);
result111 = resultAux1111(1:1028);

[result222, result333] = get_create(result111 - 0.65);

result444 = get_region(result111, 266, 350, 15) - 0.3;

result555 = get_region(result111, 788, 887, 25) - 0.35;

result667Aux = get_region(result111, 266, 350, 10) + 0.27;
result667 = get_region(result667Aux, 788, 887, 15);

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6];

resultData2 = [result11 result22-0.4 result33+0.3 result44 result55 result66];

resultData3 = [result111 result222 result333 result444 result555 result667];