function [resultData, resultData1, resultData2] = get_groupC(data)

%Inicio dos Sensorgramas com todo o tempo
result1 = get_region(data, 693, 776, 50) + 0.5;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 352, 382, 50) + 0.2;
result4 = get_region(result4Aux, 1264, 1363, 50);

result5 = get_region(data, 1851, 1918, 50) + 0.3;

result6Aux = get_region(data, 693, 798, 50) - 0.2;
result6AAux = get_region(result6Aux, 1264, 1363, 50);
result6 = get_region(result6AAux, 1851, 1918, 50);
%Fim dos Sensorgramas com todo o tempo

% result7 = result6 - 0.1;
% result7(593) = 69.2;

%Inicio dos Sensorgramas com o tempo 1557

result11 = data(1:1557);

[result22, result33] = get_create(result11 - 0.33);

result44Aux = get_region(result11, 352, 382, 30) + 0.38;
result44 = get_region(result44Aux, 699, 776, 50);
 
result55 = get_region(result11, 1264, 1363, 70) - 0.2;
 
result66Aux = get_region(result11, 699, 776, 60) - 0.1;
result66 = get_region(result66Aux, 1264, 1363, 80);
%Fim dos Sensorgramas com o tempo 1557

result111Aux = data(1:876);
result1Aux = repelem(result111Aux(1), 52)';
result11Aux = repelem(result111Aux(875), 100)';

result111 = cat(1, result1Aux, result111Aux, result11Aux);

[result222, result333] = get_create(result111 - 0.45);

result444 = get_region(result111, 745, 806, 70) + 0.22;
 
result555 = get_region(result111, 404, 434, 30) - 0.42;
 
result667Aux = get_region(result111, 404, 434, 20) - 0.2;
result667 = get_region(result667Aux, 745, 806, 80);

resultData = [result1 result2 result3 result4 result5 result6];

resultData1 = [result11 result22 result33 result44 result55 result66];

resultData2 = [result111 result222 result333 result444 result555 result667];