close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[result_group_A1, result_group_A2, result_group_A3] = get_groupA(dataBK7Au_A);

[result_group_B1, result_group_B2, result_group_B3]= get_groupB(dataBK7Au_B);

[result_group_C1, result_group_C2, result_group_C3] = get_groupC(dataBK7Au_C);

[result_group_D1, result_group_D2, result_group_D3] = get_groupD(dataBK7Au_D);

resultDCTA1 = get_dct(result_group_A1)';
resultDCTA2 = get_dct(result_group_A2)';
resultDCTA3 = get_dct(result_group_A3)';

resultDCTB1 = get_dct(result_group_B1)';
resultDCTB2 = get_dct(result_group_B2)';
resultDCTB3 = get_dct(result_group_B3)';

resultDCTC1 = get_dct(result_group_C1)';
resultDCTC2 = get_dct(result_group_C2)';
resultDCTC3 = get_dct(result_group_C3)';

resultDCTD1 = get_dct(result_group_D1)';
resultDCTD2 = get_dct(result_group_D2)';
resultDCTD3 = get_dct(result_group_D3)';

resultDCT = [abs(resultDCTA1(:,2:4)) repelem(1,6)'; abs(resultDCTA2(:,2:4)) repelem(2,6)';
    abs(resultDCTA3(:,2:4)) repelem(3,6)'; abs(resultDCTB1(:,2:4)) repelem(4,6)';
    abs(resultDCTB2(:,2:4)) repelem(5,6)'; abs(resultDCTB3(:,2:4)) repelem(6,6)';
    abs(resultDCTC1(:,2:4)) repelem(7,6)'; abs(resultDCTC2(:,2:4)) repelem(8,6)'; 
    abs(resultDCTC3(:,2:4)) repelem(9,6)'; abs(resultDCTD1(:,2:4)) repelem(10,6)';
    abs(resultDCTD2(:,2:4)) repelem(11,6)'; abs(resultDCTD3(:,2:4)) repelem(12,6)']; 

result = [];
accuracyC = [];

for i = 1:1:72
    a = resultDCT(i:i,1:4);
    result = setdiff(resultDCT, a, 'stable', 'rows');
    
    mdl = fitcknn(result(:,1:3), result(:,4:4), 'NumNeighbors', 5);
    res = predict(mdl, a(:,1:3))
    
    class = a(:,4:4);
    
%      [Train, Test] = crossvalind('LeaveMOut', res, 1);
  
    accuracyC = [accuracyC; sum(class == res) / numel(class)];
    
%    value(:,1:4) = setdiff(resultDCT(:,1:4), a(1:1,1:4));
%     if i == cont
%         aux(i:i,1:4) = resultDCT(i:i,1:4);
%         dataVetor(:,1:4) = resultDCT(2:end,1:4);
%     end
end

accuracyCAux = sum(accuracyC)./72