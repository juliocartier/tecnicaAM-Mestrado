close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

result_A = get_groupA(dataBK7Au_A);

result_B = get_groupB(dataBK7Au_B);

result_C = get_groupC(dataBK7Au_C);

result_D = get_groupD(dataBK7Au_D);

% figure(123)
% plot(result_A(:,2:2), '');
% 
% figure(1234)
% plot(result_B(:,2:2));
% 
% figure(12345)
% plot(result_C(:,2:2));
% 
% figure(123456)
% plot(result_D(:,2:2));

resultDCTA = get_dct(result_A)';
resultDCTB = get_dct(result_B)';
resultDCTC = get_dct(result_C)';
resultDCTD = get_dct(result_D)';

resultDCT = [abs(resultDCTA(:,2:4)) repelem(1,6)'; abs(resultDCTB(:,2:4)) repelem(2,6)'; 
    abs(resultDCTC(:,2:4)) repelem(3,6)'; abs(resultDCTD(:,2:4)) repelem(4,6)'];

% figure(2);
% plot(result_A(:,:), 'r');
% hold on;
% plot(result_B(:,:), 'b');
% hold on;
% plot(result_C(:,:), 'g');
% hold on;
% plot(result_D(:,:), 'k');

% figure(3);
% plot3(resultDCT(1:7,1:1), resultDCT(1:7,2:2), resultDCT(1:7,3:3), '.r', 'MarkerSize', 20);
% hold on;
% plot3(resultDCT(8:14,1:1), resultDCT(8:14,2:2), resultDCT(8:14,3:3), '.b', 'MarkerSize', 20);
% hold on;
% plot3(resultDCT(15:21,1:1), resultDCT(15:21,2:2), resultDCT(15:21,3:3), '.g', 'MarkerSize', 20);
% hold on;
% plot3(resultDCT(22:28,1:1), resultDCT(22:28,2:2), resultDCT(22:28,3:3), '.k', 'MarkerSize', 20);

%accuracyC = [];
% res = [];
% class = [];
% 
% for j = 1:1:10

%      aux = repelem(randperm(28,20),1)';
%      x = sort(aux);
%      train = [];
%      test = [];

%    for i=1:1:length(resultDCT)
%        if find(i==x)
%         train = [train; resultDCT(i:i,1:4)];
%        else
%         test = [test; resultDCT(i:i,1:4)];
%        end
%    end
    
%c = cvpartition(resultDCT(:,4:4), 'leaveout', 1)  

%   mdl = fitcknn(train(:,1:3), train(:,4:4), 'NumNeighbors', 5);
%     
%   j
%   res = predict(mdl, test(:,1:3))
%      
%   class = test(:,4:4)
% 
%   
%accuracyC = [accuracyC; sum(class == res) / numel(class)];
% 
% 
% end
%  accuracyCAux = sum(accuracyC)./10

result = [];
accuracyC = [];
%class = [];

resp_c = [];

for i = 1:1:24
    a = resultDCT(i:i,1:4);
    result = setdiff(resultDCT, a, 'stable', 'rows');
    
    mdl = fitcknn(result(:,1:3), result(:,4:4), 'NumNeighbors', 3);
    
    class = a(:,4:4);
    
    res = predict(mdl, a(:,1:3));
    
    %resp_c = [resp_c; a(:,4:4)];
    
    %mree = (a(:,4:4) - res)/res
    
    accuracyC = [accuracyC; sum(class == res) / numel(class)];
%    accuracyC = [accuracyC; res];
    
%      [Train, Test] = crossvalind('LeaveMOut', res, 1);
  
    %accuracyC = [accuracyC; res];
    
%    value(:,1:4) = setdiff(resultDCT(:,1:4), a(1:1,1:4));
%     if i == cont
%         aux(i:i,1:4) = resultDCT(i:i,1:4);
%         dataVetor(:,1:4) = resultDCT(2:end,1:4);
%     end
end
% 
%mree = (sum(resp_c) - sum(accuracyC))/sum(accuracyC)
accuracyCAux = sum(accuracyC)./length(accuracyC)

%ac = (sum(resp_c) - sum(accuracyC))/ length(resp_c)