function [resultData, resultData1, resultData2] = get_groupA(data)

%Inicio dos Sensorgramas do grupo 1 com todo o tempo
result1 = get_region(data, 1264, 1363, 50);

[result2, result3] = get_create(data);

result4Aux = get_region(data, 1263, 1355, 50) + 0.2;
result4 = get_region(result4Aux, 1851, 1918, 50);

result5 = get_region(data, 366, 427, 50) - 0.2;

result6Aux = get_region(data, 371, 471, 50) - 0.4;
result6AAux = get_region(result6Aux, 713, 743, 50);
result6 = get_region(result6AAux, 1851, 1918, 50);

%Fim dos Sensorgramas do grupo 1 com todo o tempo

% result7 = result6 - 0.3;
% result7(625) = 67.6;

%Inicio dos Sensorgramas do grupo 1 com o tempo 1557

resultAux1 = result2(1:1557);

result11 = resultAux1 - 0.09;

[result22, result33] = get_create(resultAux1);

result44Aux = get_region(resultAux1, 366, 449, 50) - 0.3;
result44 = get_region(result44Aux, 713, 743, 50);

result55 = get_region(resultAux1, 1264, 1363, 50) + 0.5;

result66Aux = get_region(resultAux1, 371, 471, 50) - 0.9;
result66 = get_region(result66Aux, 1264, 1363, 60);

%Fim dos Sensorgramas do grupo 1 com o tempo 1557

%Inicio dos Sensorgramas do grupo 1 com o tempo 1028
result1Aux2 = data(1:876);
result111Aux = repelem(result1Aux2(1), 52)';
result1111Aux = repelem(result1Aux2(875), 100)';

result111 = cat(1, result111Aux, result1Aux2, result1111Aux) - 0.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 418, 479, 60) - 0.6;

result555 = get_region(result111, 765, 795, 40) + 0.3;

result667Aux = get_region(result111, 418, 479, 40) - 0.9; 
result667 = get_region(result667Aux, 765, 795, 30);

%Fim dos Sensorgramas do grupo 1 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6];

resultData1 = [result11 result22-0.1 result33+0.1 result44 result55 result66];

resultData2 = [result111 result222+0.2 result333-0.3 result444 result555 result667];
