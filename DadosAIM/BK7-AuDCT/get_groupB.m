function [resultData, resultData1, resultData2] = get_groupB(data)

%Inicio dos Sensorgramas com todo o tempo
result1 = get_region(data,352, 382, 50) - 0.5;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 903, 1002, 50) - 0.2;
result4 = get_region(result4Aux, 1847, 1918, 50);

result5 = get_region(data, 1847, 1918, 50) - 0.3;

result6Aux = get_region(data, 352, 382, 50) + 0.3;
result6AAux = get_region(result6Aux, 903, 1002, 50);
result6 = get_region(result6AAux, 1851, 1918, 50);

%Fim dos Sensorgramas com todo o tempo

% result7 = result6 - 0.1;
% result7(796) = 68.7;
 
%Inicio dos Sensorgramas com o tempo 1557
result11 = data(1:1556);
result11(1557:1557, 1:1) = repelem(result11(1556:1556,1:1),1)';

[result22, result33] = get_create(result11 - 0.7);

result44Aux = get_region(result11, 352, 382, 40) - 0.6;
result44 = get_region(result44Aux, 1374, 1457, 60);
 
result55 = get_region(result11, 1374, 1457, 50) - 0.55;

result66Aux = get_region(result11, 352, 382, 50) + 0.35;
result66 = get_region(result66Aux, 1374, 1457, 50);

%Fim dos Sensorgramas com o tempo 1557

%Inicio dos Sensorgramas com o tempo 1028

resultAuxData111 = data(1:1198);
resultAux1111 = get_region_time(resultAuxData111, 903, 1002, 100);
resultAux111 = get_region_time(resultAux1111, 352, 475, 70);
result111 = resultAux111;

[result222, result333] = get_create(result111 - 0.58);

result444 = get_region(result111, 734, 833, 20) - 0.45;
 
result555 = get_region(result111, 282, 313, 15) + 0.55;

result667Aux = get_region(result111, 282, 313, 10) + 0.35;
result667 = get_region(result667Aux, 734, 833, 15);

%Fim dos Sensorgramas com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6];

resultData1 = [result11 result22 result33 result44 result55 result66];

resultData2 = [result111 result222 result333 result444 result555 result667];
