function [groupB, groupC, groupD] = get_BK7AuGroup(data)

groupB = [data(1:187) - 0.0599; data(549:767); data(768:875); data(876:1354); data(1355:1556);data(188:470); data(471:548); data(1557:1917); data(1918:2069)]; 

groupC = [data(1:187) - 0.054; data(549:767); data(768:875); data(188:470); data(471:548); data(876:1354); data(1355:1556); data(1557:1917); data(1918:2069)];

groupD = [data(1:187); data(188:470); data(471:548); data(876:1354); data(1355:1556); data(1557:1917); data(1918:2069); data(549:767); data(768:875)];

groupB(1195:1198) = 68.15;

groupD(1742:1746) = 68.08;
