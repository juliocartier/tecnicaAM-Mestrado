function [resultData] = get_groupA(data)

result1 = get_region(data,1264, 1363);

[result2, result3] = get_create(data);

result4 = get_region(data, 1263, 1355) + 0.2;
result44 = get_region(result4, 1851, 1918);

result5 = get_region(data, 366, 427) - 0.2;

result6Aux = get_region(data, 371, 471) - 0.4;
result6AAux = get_region(result6Aux, 713, 743);
result6 = get_region(result6AAux, 1851, 1918);

result7 = result6 - 0.3;
result7(625) = 67.6;

resultData = [result1 result2 result3 result44 result5 result6 result7];
