function [resultData] = get_groupC(data)

result1 = get_region(data, 693, 776) + 0.5;

[result2, result3] = get_create(data);

result4 = get_region(data, 352, 382) + 0.2;
result44 = get_region(result4, 1264, 1363);

result5 = get_region(data, 1851, 1918) + 0.3;

result6Aux = get_region(data, 693, 798) - 0.2;
result6AAux = get_region(result6Aux, 1264, 1363);
result6 = get_region(result6AAux, 1851, 1918);

result7 = result6 - 0.1;
result7(593) = 69.2;

resultData = [result1 result2 result3 result44 result5 result6 result7];