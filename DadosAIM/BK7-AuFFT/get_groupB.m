function [resultData] = get_groupB(data)

result1 = get_region(data,352, 382) - 0.5;

[result2, result3] = get_create(data);

result4 = get_region(data, 903, 1002) - 0.2;
result44 = get_region(result4, 1847, 1918);

result5 = get_region(data, 1847, 1918) - 0.3;

result6Aux = get_region(data, 352, 382) + 0.3;
result6AAux = get_region(result6Aux, 903, 1002);
result6 = get_region(result6AAux, 1851, 1918);

result7 = result6 - 0.1;
result7(796) = 68.7; 

resultData = [result1 result2 result3 result44 result5 result6 result7];
