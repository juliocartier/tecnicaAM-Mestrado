function [resultData] = get_groupD(data)

result1 = get_region(data, 937, 1036) + 0.6;

[result2, result3] = get_create(data - 0.2);

result4 = get_region(data, 366, 449) + 0.4;
result44 = get_region(result4, 1524, 1591);

result5 = get_region(data, 1907, 1937) - 0.2;

result6Aux = get_region(data, 371, 471) - 0.4;
result6AAux = get_region(result6Aux, 1521, 1591);
result6 = get_region(result6AAux, 1907, 1937);

result7 = result6 + 0.1;
result7(1819) = 68.3;

resultData = [result1 result2 result3 result44 result5 result6 result7];