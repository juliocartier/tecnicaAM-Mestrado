function[result] = get_region(data, inicio, fim)

receitaInicio = data(1:inicio-50);
varDissociacao = data(inicio:fim);

valorRepetido = repelem(data(fim), (inicio - (inicio - 50)))';

result = cat(1, receitaInicio, varDissociacao, valorRepetido, data(fim + 2:end));