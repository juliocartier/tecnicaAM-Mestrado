close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

result_A = get_groupA(dataBK7Au_A);

result_B = get_groupB(dataBK7Au_B);

result_C = get_groupC(dataBK7Au_C);

result_D = get_groupD(dataBK7Au_D);

resultFFTA = get_fft(result_A)';
resultFFTB = get_fft(result_B)';
resultFFTC = get_fft(result_C)';
resultFFTD = get_fft(result_D)';

figure(123)
plot(result_A(:,1:1));

figure(333);
plot(abs(resultFFTA(1:1,2:30)), '.');

resultFFT = [abs(resultFFTA(:,2:4)) repelem(1,7)'; abs(resultFFTB(:,2:4)) repelem(2,7)'; 
    abs(resultFFTC(:,2:4)) repelem(3,7)'; abs(resultFFTD(:,2:4)) repelem(4,7)'];

% figure(2);
% plot(result_A(:,:), 'r');
% hold on;
% plot(result_B(:,:), 'b');
% hold on;
% plot(result_C(:,:), 'g');
% hold on;
% plot(result_D(:,:), 'k');

figure(3);
plot3(resultFFT(1:7,1:1), resultFFT(1:7,2:2), resultFFT(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultFFT(8:14,1:1), resultFFT(8:14,2:2), resultFFT(8:14,3:3), '.b', 'MarkerSize', 20);
hold on;
plot3(resultFFT(15:21,1:1), resultFFT(15:21,2:2), resultFFT(15:21,3:3), '.g', 'MarkerSize', 20);
hold on;
plot3(resultFFT(22:28,1:1), resultFFT(22:28,2:2), resultFFT(22:28,3:3), '.k', 'MarkerSize', 20);

accuracyC = [];
% res = [];
% class = [];

for j = 1:1:10

     aux = repelem(randperm(28,20),1)';
     x = sort(aux);
     train = [];
     test = [];

   for i=1:1:length(resultFFT)
       if find(i==x)
        train = [train; resultFFT(i:i,1:4)];
       else
        test = [test; resultFFT(i:i,1:4)];
       end
    end

  mdl = fitcknn(train(:,1:3), train(:,4:4), 'NumNeighbors', 5);
    
  j
  res = predict(mdl, test(:,1:3))
     
  class = test(:,4:4)

  
 accuracyC = [accuracyC; sum(class == res) / numel(class)];


end
 accuracyCAux = sum(accuracyC)./10
 