clear all
close all
clc

load dataBKAu.txt

input_date = dataBKAu(:,1:1);

dctDate = dct(input_date);
% fchtDate = fwht(input_date, 2048, 'hadamard');

figure(123)
plot(abs(dctDate(1:10)), '.r', 'MarkerSize', 20);
xlabel('Coeficientes');
ylabel('Amplitude');
