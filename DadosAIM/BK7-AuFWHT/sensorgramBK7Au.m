close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataBK7Au_A);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3] = get_D(dataBK7Au_D);

resultWHTA1 = fwht(resultA1(:,1:1), 4096);

resultIWHT = ifwht(resultWHTA1(1:9,1:1), 4096);
resultIWHT100 = ifwht(resultWHTA1(1:100,1:1), 4096);
resultIWHT200 = ifwht(resultWHTA1(1:200,1:1), 4096);
resultIWHT300 = ifwht(resultWHTA1(1:300,1:1), 4096);

resultIWHT2069 = ifwht(resultWHTA1(1:4096,1:1), 4096);

err9 = mse(resultIWHT(1:2048,1:1), resultA1(1:2048,1:1));
err100 = mse(resultIWHT100(1:2048,1:1), resultA1(1:2048,1:1));
err200 = mse(resultIWHT200(1:2048,1:1), resultA1(1:2048,1:1));
err300 = mse(resultIWHT300(1:2048,1:1), resultA1(1:2048,1:1));

errors = [err9; err100; err200; err300];

names = {'9'; '100'; '200'; '300'};

figure(2645)
plot(errors,'.r');
set(gca,'xtick',[1:4],'xticklabel',names)
xlabel('Qtd. Coeficientes');
ylabel('ERM $(\hat{\Theta})$','interpreter','latex')


figure(123456)
plot(resultIWHT(1:2048))
hold on;
plot(resultIWHT100(1:2048))
hold on;
plot(resultIWHT200(1:2048))
hold on;
plot(resultIWHT300(1:2048))
xlabel('Tempo (s)');
ylabel('\theta_r (�)');

% resultWHTA2 = fwht(result_group_A2, 1024, 'hadamard')';
% resultWHTA3 = fwht(result_group_A3, 512, 'hadamard')';
% resultWHTA4 = fwht(result_group_A4, 256, 'hadamard')';
% 
% resultWHTB1 = fwht(result_group_B1, 1024, 'hadamard')';
% resultWHTB2 = fwht(result_group_B2, 1024, 'hadamard')';
% resultWHTB3 = fwht(result_group_B3, 512, 'hadamard')';
% resultWHTB4 = fwht(result_group_B4, 256, 'hadamard')';
% 
% resultWHTC1 = fwht(result_group_C1, 1024, 'hadamard')';
% resultWHTC2 = fwht(result_group_C2, 1024, 'hadamard')';
% resultWHTC3 = fwht(result_group_C3, 512, 'hadamard')';
% 
% resultWHTD1 = fwht(result_group_D1, 1024, 'hadamard')';
% resultWHTD2 = fwht(result_group_D2, 1024, 'hadamard')';
% resultWHTD3 = fwht(result_group_D3, 512, 'hadamard')';


% accuracyC = [];
% res = [];
% class = [];

% for i = 1:1:length(resultDCT(:,1:1))
%     a = resultDCT(i:i,1:end);
%     result = setdiff(resultDCT, a, 'stable', 'rows');
%     
%     mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors',  3, 'Distance' , 'euclidean');
%     res = predict(mdl, a(:,1:end-1));
%     
%     class = a(:,end:end);
%    
%     accuracyC = [accuracyC; sum(class == res) / numel(class)];
%     
% end
%  
% accuracy = sum(accuracyC)./length(resultDCT(:,1:1))