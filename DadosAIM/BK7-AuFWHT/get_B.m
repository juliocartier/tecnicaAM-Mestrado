function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2

result1 = data - 1.5;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 903, 1002, 50) - 0.2;
result4 = get_region(result4Aux, 1847, 1918, 50);

result5 = get_region(data, 1847, 1918, 50) - 0.3;

result6Aux = get_region(data, 352, 415, 30) + 0.3;
result6AAux = get_region(result6Aux, 903, 1002, 50);
result6 = get_region(result6AAux, 1851, 1918, 50);

result7 = result5 - 0.6;
result7(1500) = result7(1500) + 0.4;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:1550);

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 352, 415, 20) - 0.6;
result44 = get_region(result44Aux, 1374, 1457, 60);
 
result55 = get_region(result11, 1374, 1457, 50) - 0.55;

result66Aux = get_region(result11, 352, 415, 25) + 0.35;
result66 = get_region(result66Aux, 1374, 1457, 50);


result77 = result44 - 0.8;
result77(1447) = result77(1447) + 0.4;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:1198);

[result222, result333] = get_create(result111);

result444 = get_region(result111, 903, 1062, 60) - 0.45;
 
result555 = get_region(result111, 352, 415, 15) + 0.55;

result667Aux = get_region(result111, 352, 415, 10) + 0.35;
result667 = get_region(result667Aux, 903, 1062, 75);

result777 = result555 + 0.3;
% result777(165) = result777(155) + 10;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:512);

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 352, 415, 40) - 0.25;

result5555 = result1111 + 0.9;
result5555(281) = result5555(281) + 0.5;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
