function [resultData, resultData2, resultData3, resultData4] = get_D(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = data + 0.6;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 366, 471, 50) + 0.4;
result4 = get_region(result4Aux, 1520, 1591, 50);

result5 = get_region(data, 1907, 1970, 35) - 0.2;

result6Aux = get_region(data, 366, 471, 50) - 0.4;
result6AAux = get_region(result6Aux, 1520, 1591, 50);
result6 = get_region(result6AAux, 1907, 1970, 35);

result7 = result3 + 0.2;
result7(790) = result7(790) + 0.5;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:1741);

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 937, 1081, 70) + 0.4;
result44 = get_region(result44Aux, 1520, 1591, 30);

result55 = get_region(result11, 361, 471, 60) - 0.3;

result66Aux = get_region(result11, 361, 471, 50) + 0.45;
result66 = get_region(result66Aux, 937, 1081, 70);

result77 = result66 + 1.3;
result77(200) = result77(200) + 0.2;
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:1230);

[result222, result333] = get_create(result111);

result444 = get_region(result111, 361, 471, 50) - 0.3;

result555 = get_region(result111, 937, 1081, 45) - 0.35;

result667Aux = get_region(result111, 361, 471, 40) + 0.27;
result667 = get_region(result667Aux, 937, 1081, 35);

result777 = result111 - 0.6;
result777(864) = result777(864) + 0.4;

% Sensorgramas com 3 Regimes

sensorgram3R = [data(1:188) - 0.0560; data(552:1230)];

result1111 = sensorgram3R;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 574, 712, 40) - 0.2;

result5555 = result2222 + 0.1;

result5555(776) = result5555(776) + 0.3;

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22-0.4 result33+0.3 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];