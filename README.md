# Dados da Dissertação do Mestrado e Algoritmos Utilizados
# SMART SPR: Identificação e Análise das Resposas Fornecidas por Sensores Baseados em Ressonância de Plasmons de Superfície

### Discente: Julio Cartier Maia Gomes

### Orientadores: Dr. Leiva Casemiro Oliveira && Dr. Leandro Carlos de Souza

## Dados, Técnicas de AM e Estatísticos
1. Foram obtidos sensorgramas com as estruturas multicamadas AIM (Mode Interrogation Angular - Modo de interrogação Angular) e WIM (Mode Interrogation Wavelength) em um sensor SPR, esses sensorgramas foram adquiridos na Tese "Construção e Caracterização de Sensores SPR: Influências da camada metálica e do substrato dieletrico" do autor Dr. Leiva Casemiro Oliveira.
2. Foram obtidos sensorgramas da Leishmaniose e Dengue com o usdo do sensor SPR. 
3. Regressão Linear
4. *k*-NN (*k*-nearest neighbor - *k*- Vizinho mais proximo)
5. Teste de Hipotese *t-student* (Uma e Duas Amostras)
6. Teste de Hipotese Bootstrap (Uma e Duas Amostras)
7. Intervalo de Confiança 
8. Transforma Discreta do Cosseno (DCT - Discrete Cosine Transform)
9. Transforma Discreta de Chebyshev (DCTh - Discrete Chebyshev Transform)
10. Transforma Rápida de Walsh-Hadamard (FWHT - Fast Walsh-Hadamard Transform)
11. Descritor Temporal de Sensorgrama (TSD - Temporal Sensorgram Descriptor)
