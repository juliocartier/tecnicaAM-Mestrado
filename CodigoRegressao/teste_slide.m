clear all;
close all;
clc

y = [9; 6; 4; 3; 3; 5; 8; 2; 7; 4];
x1 = [6; 5; 3; 1; 4; 3; 6; 2; 4; 2];
x2 = [3; 2; 2; 1; 1; 3; 3; 1; 2; 2];

X = [ones(1, length(x1))' x1 x2];

X_trans = inv(X'*X);
Y_trans = (X' * y);
XY = X_trans * Y_trans;

Cjj = X_trans;

SSE = y'*y - XY' * X'*y;
S2 = SSE/(length(x1) - 3);
S = sqrt(S2); 
% test_t = (XY(2:2,1:1) - 0)/(sqrt(S2 * Cjj(2:2,2:2)))

% valor_coef = regress(y, X);