%%
%format shortG
clear all
close all
clc

%ler dados - sensorgramas com diferentes t�cnicas de detec��o do m�nimo
load leishPositivo.csv
%load receitaPMMAAg.txt
%load dataBKAu.txt
%load sensor_min_centro.spr

%input_date = sensor_min_centro(:,1);
input_date = leishPositivo(:,1:1);

%input_date = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

figure(2);
plot (input_date)
xlabel('Tempo (s)');
ylabel('\lambda_R');


%A variavel winSize � o tamanho da janela
%winSize = 18;
%winSize = 20;
winSize = 13;

%Variavel para a primeira regress�o
lambdaRegress = [];
low_i = [];
high_i = [];
y_ie = [];
IC_A = [];
IC_B = [];
teste_h = [];
valor_estimado = [];

BICI = [];
BICS = [];

%Percorre todos os dados com o tamanho da janela
for i=1:1:length(input_date)
%for i=winSize+1:1:length(aa)
    ante = i-winSize;
    pos = i+winSize;
    if(ante <= 1)
        ante = 1;
    end
    
    if(pos > length(input_date))
       pos = length(input_date);
    end
    
    %date = input_date(ante:i); %so anterior
    %date = input_date(i:pos); %so posterior
    date = input_date(ante:pos); %anterior e posterior

    st = max(1,i - winSize);
    %[lb_A, lb_B,ll_A, hh_A, y_i, ic_coefA, ic_coefB] = get_regress(date, 0.001);
    %[lb_A, lb_B, valor_estim, y_i, ic_coefA, ic_coefB, teste_hip, p_value_ll, p_value_hh, ICIB, ICSB] = get_regress(date, 0.01);
    [lb_A, lb_B, valor_estim, y_i, ic_coefA, ic_coefB, teste_hip, p_value_ll, p_value_hh] = get_regress(date, 0.001);
    %[lb_A, lb_B,ll_A, hh_A, estim] = get_regress(date, 0.01);
    
    lambdaRegress = [lambdaRegress; lb_A];
    valor_estimado = [valor_estimado; valor_estim];
    y_ie = [y_ie; y_i];
    IC_A = [IC_A; ic_coefA];
    IC_B = [IC_B; ic_coefB];
    low_i = [low_i; p_value_ll];
    high_i = [high_i; p_value_hh];
    teste_h = [teste_h; teste_hip];
    
%     BICI = [BICI; ICIB];
%     BICS = [BICS; ICSB];
end

idx = IC_A <= 0 & IC_B >=0;
input_date_regress = input_date.*idx;

idx(747:757) = 0;

idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
input_date_regress2 = input_date.*idx2;

% idx2(90:95) = 0;
% idx2(272:273) = 0;
% idx2(505:507) = 0;
% idx2(679:685) = 0;
% 
% hold on;
% plot(lambdaRegress+665, 'k');
%savefig('Imagem.fig');
% z = ones(length(lambdaRegress),1)*660;
% plot(low_i + 660, 'b');
% plot(high_i+ 660, 'r');
% plot(find(idx==1),input_date_regress(find(idx==1)),'ko');
% plot([1:length(lambdaRegress)]', z, 'g');

% figure(123456)
% plot(estimado, '.');

% value_yiP = get_value_regress(y_ie(255:265), 9)';
% value_yiT = get_value_regress(y_ie(548:558), 9)';

% y_i = input_date(1:10,1:1) - (value_yiP+660);

% figure(123412)
% histfit(y_ie(1:10), 3);
% 
% figure(1234129)
% histfit(y_ie(255:265), 3);

% figure(1234)
% plot(y_ie, '.');
% plot(input_date);
% hold on;
% plot(value_yi, '.');

new_valeu_stim = valor_estimado(1:12:end);

%figure(9031)
%plot(new_valeu_stim, '.');

figure(214)
plot(input_date)
hold on;
plot((1:125), new_valeu_stim(1:125), '.k')
hold on;
plot((148:167), new_valeu_stim(295:4:374), '.m')

% figure(2123)
%histfit(y_ie(734:774, 1:1), 3);
% histfit(y_ie(219:239, 1:1), 3);

z = ones(length(lambdaRegress),1)*665;

% figure(21345)
% % plot(input_date);
% plot(lambdaRegress, ':k');
% hold on;
% plot(IC_B, 'b');
% hold on;
% plot(IC_A, 'r');
% hold on;
% plot([1:length(lambdaRegress)]', z, 'g');


% figure(24512)
% plot(input_date);
% hold on;
% plot((1:1:41),valor_estimado(1:13:525), '.', 'MarkerSize', 10);
% hold on;
% plot((42:1:54),valor_estimado(565:1:577), '.', 'MarkerSize', 10);
% plot(idx + 660, '.b')

% figure(123456733)
% plot(input_date)
% hold on;
% plot(low_i+660, 'r');
% hold on;
% plot(high_i+660, 'b');
% hold on;
% plot(teste_h+660, '.k');

%input_date_regress_test_h = input_date.*teste_h;
 
% figure(245123)
% plot(input_date)
% hold on;
% plot(find(idx==1),input_date_regress(find(idx==1)),'ro');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)');
% plot(teste_h + 660, '.k')

% figure(2452123)
% % plot(input_date)
% % hold on;
% plot(find(idx==0), input_date_regress(find(idx==0)),'ko');

%figure(2452123)
% plot(input_date);
% hold on;
% plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)');


