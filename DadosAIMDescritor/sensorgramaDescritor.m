close all;
clear all;
clc;
format short;

%load receitaBK7Ag.txt;
%load dados_BKAg.txt
% load dataBKAu.txt
%load PCAgSensorgram.txt
% load PMMAAgSensorCorret.txt
%load receitaTOPASAg.txt

load leishPositivo.csv

input_date = leishPositivo(:,1:1);

idx = [];
error = 0.00015;
%soma = [];

%aux;
for i=1:1:length(input_date)

    pos = i+3;
    
    if(pos > length(input_date))
      pos = length(input_date);
    end
    
    dateMaior = input_date(pos) + error;
    dateMenor = input_date(pos) - error;
  
    if input_date(i) <= dateMaior && input_date(i) >= dateMenor
    %if input_date(i) >= dateMenor
        idx = [idx; input_date(i)];
        %idx{i} = input_date(i);
    else 
        %idx{i} = 0;
        idx = [idx; 0];
    end
    
end

idx(131:154) = 0;
idx(393:610) = 0;
idx(810:905) = 0;
idx(1176:1436) = 0;
idx(1813:1905) = 0;
idx(2023:2082) = 0;
idx(1556:1562) = input_date(1556:1562);


cont = 0;
aux = {};
vetor = [];
  
j = 1;
i = 1;

idx(end:end+3) = 0;

%for j=1:1:8
 for k = 1:1:length(idx)
        if idx(k) == 0
            cont = cont + 1;
            
            if cont > 3 
                if j ~= 1
                    aux{i} = vetor;
                    vetor = [];
                    i = i + 1;
                end
                
                j = 1;
                             
                continue;
            end
        else
           cont = 0;
           
           vetor(j) = idx(k);
           j = j + 1;
        end

      
 end
%end

%regimPe = [];
vetorAux = [];

for m = 1:1:i-1
   
    vetorAux = aux{1, m};
    
    if length(vetorAux) > 1
        regimPe(m) = complex(mean(aux{m}), length(aux{m})); 
    else
        regimPe(m) = 0;
    end
    
end

regimPe = regimPe(find(regimPe~=0));

posicoes = [75; 280; 686; 1044; 1650; 1942; 2223];

resultTSD = [real(regimPe(1:1,:)); imag(regimPe(1:1,:))]';

resultTSDNota = [0.000769; 0.00058; 0.001478; 0.01062; 0.00401; 0.00480; 0.00409];

regiPerm = {'RP1'; 'RP2'; 'RP3'; 'RP4'; 'RP5'; 'RP6'; 'RP7'};

T = table(resultTSD(:,1:1), resultTSD(:,2:2), 'RowNames', regiPerm);

figure(9043)
uitable('Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
    'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);

figure(8473)
plot((1:length(input_date(:,1:1))), input_date(:,1:1),'LineWidth', 1.5)
hold on;
plot(posicoes(:,1:1), resultTSD(:,1:1), '.k', 'MarkerSize', 20);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');

% inverDescri = get_inverDescrit(regimPe);
% 
% figure(21)
% plot(input_date, 'b');
% hold on;
% plot(find(idx~=0), idx(find(idx~=0)), '.k');
%plot(inverDescri);


% plot(find(idx==1),valor(find(idx==1)),'ro');


% figure(1)
% plot(input_date, 'b');
% hold on;
% plot(find(idx~=0), idx(find(idx~=0)), 'r');
% plot(find(idx==1),valor(find(idx==1)),'ro');
