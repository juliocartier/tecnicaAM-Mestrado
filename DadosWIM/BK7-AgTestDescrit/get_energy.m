function [energy, coef] = get_energy(data)
    
    %dctData = dct(data);-
    
    energy = [];
    
    coef = (1:9);
%     coef2 = (10:10:length(data));
%     coef3 = [coef coef2]';
    
%     energy = [energy; sum(abs(data(2:2)))];
%     energy = [energy; sum(abs(data(2:3)))];   
%     energy = [energy; sum(abs(data(2:4)))];   
%     energy = [energy; sum(abs(data(2:5)))];   
%     energy = [energy; sum(abs(data(2:6)))];

    i = 1;
    while i <= length(data)
        energy = [energy; sum(real(data(1:i))  + imag(data(1:i)) )];
        i = i + 1;
    end
end