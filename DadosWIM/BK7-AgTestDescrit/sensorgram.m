close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A, result_group_AA, result_group_AAA, result_group_AAAA] = get_A(receitaBK7Ag);

[result_group_B, result_group_BB, result_group_BBB, result_group_BBBB] = get_B(data_group_b);

[result_group_C, result_group_CC, result_group_CCC] = get_C(data_group_c);

[result_group_D, result_group_DD, result_group_DDD, result_group_DDDD] = get_D(data_group_d);

%Descritor

[result_descrit_A, result_descrit_AA, result_descrit_AAA, result_descrit_AAAA] = get_descriA(result_group_A(:,1:7), result_group_AA(:,1:7), result_group_AAA(:,1:7), result_group_AAAA(:,1:5));

[result_descrit_B, result_descrit_BB, result_descrit_BBB, result_descrit_BBBB] = get_descriB(result_group_B(:,1:7), result_group_BB(:,1:7), result_group_BBB(:,1:7), result_group_BBBB(:,1:5));

[result_descrit_C, result_descrit_CC, result_descrit_CCC] = get_descriC(result_group_C(:,1:7), result_group_CC(:,1:7), result_group_CCC(:,1:7));

[result_descrit_D, result_descrit_DD, result_descrit_DDD, result_descrit_DDDD] = get_descriD(result_group_D(:,1:7), result_group_DD(:,1:7), result_group_DDD(:,1:7), result_group_DDDD(:,1:5));

result_inv = get_descrit_inver(result_descrit_A(:,2:2));

idx = result_inv > 0;
result_inv_A = idx.*result_inv;

[EnergiaReceitaA, conf] = (get_energy(result_descrit_A(1:2:end,2:2)));
 
EnergiaA = (EnergiaReceitaA(end)./EnergiaReceitaA);

EnergiaAcumuladaA = cumsum(EnergiaA(1:end)); 

figure(231)
subplot(2,1,1)
plot(conf, EnergiaA, '.r', 'MarkerSize', 15);
subplot(2,1,2)
plot(conf, EnergiaAcumuladaA, '.r', 'MarkerSize', 15);

winSize = 3;

if (winSize <= 3)

    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    resultA3 = verificaTamanho(result_descrit_AAA, winSize);
    resultA4 = verificaTamanho(result_descrit_AAAA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    resultB3 = verificaTamanho(result_descrit_BBB, winSize);
    resultB4 = verificaTamanho(result_descrit_BBBB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    resultC3 = verificaTamanho(result_descrit_CCC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    resultD3 = verificaTamanho(result_descrit_DDD, winSize);
    resultD4 = verificaTamanho(result_descrit_DDDD, winSize);

  resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultA4 repelem(4,length(resultA4(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))';
                     resultB3 repelem(7,length(resultB3(:,1:1)))'; resultB4 repelem(8,length(resultB4(:,1:1)))';
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))'; 
                     resultC3 repelem(11,length(resultC3(:,1:1)))';
                     resultD1 repelem(12,length(resultD1(:,1:1)))'; resultD2 repelem(13,length(resultD2(:,1:1)))';
                     resultD3 repelem(14,length(resultD3(:,1:1)))'; resultD4 repelem(15,length(resultD4(:,1:1)))'];

  
elseif(winSize > 3 && winSize <= 5)
     
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    resultA3 = verificaTamanho(result_descrit_AAA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    resultB3 = verificaTamanho(result_descrit_BBB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    resultC3 = verificaTamanho(result_descrit_CCC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    resultD3 = verificaTamanho(result_descrit_DDD, winSize);
    
resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))';
                     resultB2 repelem(6,length(resultB2(:,1:1)))'; resultB3 repelem(7,length(resultB3(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultC3 repelem(11,length(resultC3(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))';
                     resultD2 repelem(13,length(resultD2(:,1:1)))'; resultD3 repelem(14,length(resultD3(:,1:1)))'];

elseif(winSize > 5 && winSize <= 7)
    
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    
resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultD1 repelem(12,length(resultD1(:,1:1)))'; resultD2 repelem(13,length(resultD2(:,1:1)))'];
                 
else
    
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    
 resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))'];
    
end

figure(4532)
plot3(resultTotDescrt(1:7,1:1), resultTotDescrt(1:7,2:2), resultTotDescrt(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultTotDescrt(8:14,1:1), resultTotDescrt(8:14,2:2), resultTotDescrt(8:14,3:3), 'ob', 'MarkerSize', 8);
hold on;
plot3(resultTotDescrt(15:21,1:1), resultTotDescrt(15:21,2:2), resultTotDescrt(15:21,3:3), '+g', 'MarkerSize', 8);
hold on;
plot3(resultTotDescrt(22:26,1:1), resultTotDescrt(22:26,2:2), resultTotDescrt(22:26,3:3), '*k', 'MarkerSize', 8);
hold on;
plot3(resultTotDescrt(27:33,1:1), resultTotDescrt(27:33,2:2), resultTotDescrt(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
hold on;
plot3(resultTotDescrt(34:40,1:1), resultTotDescrt(34:40,2:2), resultTotDescrt(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
hold on;
plot3(resultTotDescrt(41:47,1:1), resultTotDescrt(41:47,2:2), resultTotDescrt(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
hold on;
plot3(resultTotDescrt(48:52,1:1), resultTotDescrt(48:52,2:2), resultTotDescrt(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
hold on;
plot3(resultTotDescrt(53:59,1:1), resultTotDescrt(53:59,2:2), resultTotDescrt(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
hold on;
plot3(resultTotDescrt(60:66,1:1), resultTotDescrt(60:66,2:2), resultTotDescrt(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
hold on;
plot3(resultTotDescrt(67:73,1:1), resultTotDescrt(67:73,2:2), resultTotDescrt(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
hold on;
plot3(resultTotDescrt(74:80,1:1), resultTotDescrt(74:80,2:2), resultTotDescrt(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
hold on;
plot3(resultTotDescrt(81:87,1:1), resultTotDescrt(81:87,2:2), resultTotDescrt(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
hold on;
plot3(resultTotDescrt(88:94,1:1), resultTotDescrt(88:94,2:2), resultTotDescrt(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
hold on;
plot3(resultTotDescrt(95:99,1:1), resultTotDescrt(95:99,2:2), resultTotDescrt(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
legend('Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5', 'Group 6', 'Group 7', 'Group 8', ...
    'Group 9', 'Group 10', 'Group 11', 'Group 12', 'Group 13', 'Group 14', 'Group 15');



% for j=1:2:length(result_descrit_A(:, 1:1))
%     hold on;
%     plot(real(result_descrit_A(j, :)), imag(result_descrit_A(j, :)), '.r', 'MarkerSize', 12);
%     hold on;
%     plot(real(result_descrit_B(j, :)), imag(result_descrit_B(j, :)), '.b', 'MarkerSize', 12);
% end

% figure(2354)
% plot(real(result_descrit_A(1:2:6, 1:end)), imag(result_descrit_A(1:2:6, 1:end)), '.r', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_AA(1:2:6, 1:end)), imag(result_descrit_AA(1:2:6, 1:end)), '.y', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_B(1:2:6, 1:end)), imag(result_descrit_B(1:2:6, 1:end)), '.g', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_BB(1:2:6, 1:end)), imag(result_descrit_BB(1:2:6, 1:end)), '.m', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_C(1:2:6, 1:end)), imag(result_descrit_C(1:2:6, 1:end)), '.k', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_D(1:2:6, 1:end)), imag(result_descrit_D(1:2:6, 1:end)), '.b', 'MarkerSize', 12);
% 

figure(213454)
% plot(result_group_A(:,1:7), 'r');
% hold on;
% plot(result_group_AA(:,1:7), 'y');
% hold on;
%plot(result_group_AAA(:,1:end), 'Color', [0.6350 0.0780 0.1840]);
% hold on;
% plot(result_group_AAAA(:,1:end), 'k');
% % hold on;
plot(find(idx~=0), result_inv(idx~=0), '.r')
% hold on;
%plot(result_group_B(:,1:7), 'g');
% hold on;
%plot(result_group_BB(:,1:7), 'm');
% hold on;
% plot(result_group_BBB(:,1:7), 'Color', [0.4660 0.6740 0.1880]);
% hold on;
% plot(result_group_BBBB(:,1:5), 'g');
% hold on;
% plot(result_group_C(:,1:7), 'k');
% hold on;
% plot(result_group_CC(:,1:7), 'c');
% hold on;
% plot(result_group_CCC(:,1:7), 'Color', [0.3010 0.7450 0.9330]);
% hold on;
% plot(result_group_D(:,1:7), 'b');
% hold on;
% plot(result_group_DD(:,1:7), 'Color', [0.4940 0.1840 0.5560]);
% hold on;
% plot(result_group_DDD(:,1:7), 'Color', [0 0.4470 0.7410]);
% hold on;
%plot(result_group_DDDD(:,1:5), 'Color', [0.9290 0.6940 0.1250]);
%ylabel('$\bar{\lambda_r}$ (nm)','interpreter','latex')
ylabel('$\bar{\lambda_r}$ (nm)','interpreter','latex')

resultTestAlgoritm = get_descritCorreto(receitaBK7Ag + 5);

result = [];
accuracyC = [];
accuracyCAux = [];
acc = [];

c = cvpartition(resultTotDescrt(:,1:1),'LeaveOut');

    for i = 1:1:c.NumTestSets
        a = resultTotDescrt(i:i,1:end);
        result = setdiff(resultTotDescrt, a, 'stable', 'rows');

        mdl = fitcknn(resultTotDescrt(:,1:end-1), resultTotDescrt(:,end:end), 'NumNeighbors', 3, 'Distance' , 'chebychev');
        res = predict(mdl, a(:,1:end-1))
        %res = predict(mdl, real(resultTestAlgoritm(1:1,1:9)))
        
        acc = [acc; res];

        class = a(:,end:end)

        accuracyC = [accuracyC; sum(class == res) / numel(class)];
        %accuracyC = sum(class == res) / numel(class);

    end

    accuracyCAux = sum(accuracyC)./length(resultTotDescrt(:,1:1))
    
B = 2000;
% thetahat = var(lambda);
 
%[bootreps, bootsam] = bootstrp(B,'mean', acc);

%ci = bootci(B,@mean, acc, 'alpha', 0.01)
%ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');

