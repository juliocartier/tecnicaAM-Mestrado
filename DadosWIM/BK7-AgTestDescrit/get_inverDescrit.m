function [resultDes] = get_inverDescrit(data)

% resultDes = [repelem(real(data(1:1, 1:1)), imag(data(1:1, 1:1)))'; 
%     repelem(0, imag(data(2:2, 1:1)))'; repelem(real(data(3:3, 1:1)), imag(data(3:3, 1:1)))';
%     repelem(0, imag(data(4:4, 1:1)))'; repelem(real(data(5:5, 1:1)), imag(data(5:5, 1:1)))';
%     repelem(0, imag(data(6:6, 1:1)))'; repelem(real(data(7:7, 1:1)), imag(data(7:7, 1:1)))';
%     repelem(0, imag(data(8:8, 1:1)))'; repelem(real(data(9:9, 1:1)), imag(data(9:9, 1:1)))';
%     repelem(0, imag(data(10:10, 1:1)))'; repelem(real(data(11:11, 1:1)), imag(data(11:11, 1:1)))';
%     repelem(0, imag(data(12:12, 1:1)))'; repelem(real(data(13:13, 1:1)), imag(data(13:13, 1:1)))';
%     repelem(0, imag(data(14:14, 1:1)))'; repelem(real(data(15:15, 1:1)), imag(data(15:15, 1:1)))';
%     repelem(0, imag(data(16:16, 1:1)))'; repelem(real(data(17:17, 1:1)), imag(data(17:17, 1:1)))'];

% resultDes = [repelem(real(data(1:1, 1:1)), imag(data(1:1, 1:1)))'; 
%             repelem(real(data(2:2,1:1)), 1)'; repelem(real(data(3:3, 1:1)), imag((data(2:2,1:1) + data(3:3, 1:1)) - 1))';
%             repelem(real(data(4:4,1:1)), 1)'; repelem(real(data(5:5, 1:1)), imag((data(4:4,1:1) + data(5:5, 1:1)) - 1))';
%             repelem(real(data(6:6,1:1)), 1)'; repelem(real(data(7:7, 1:1)), imag((data(6:6,1:1) + data(7:7, 1:1)) - 1))';
%             repelem(real(data(8:8,1:1)), 1)'; repelem(real(data(9:9, 1:1)), imag((data(8:8,1:1) + data(9:9, 1:1)) - 1))';
%             repelem(real(data(10:10,1:1)), 1)'; repelem(real(data(11:11, 1:1)), imag((data(10:10,1:1) + data(11:11, 1:1)) - 1))';
%             repelem(real(data(12:12,1:1)), 1)'; repelem(real(data(13:13, 1:1)), imag((data(12:12,1:1) + data(13:13, 1:1)) - 1))';
%             repelem(real(data(14:14,1:1)), 1)'; repelem(real(data(15:15, 1:1)), imag((data(14:14,1:1) + data(15:15, 1:1)) - 1))';
%             repelem(real(data(16:16,1:1)), 1)'; repelem(real(data(17:17, 1:1)), imag((data(16:16,1:1) + data(17:17, 1:1)) - 1))'];

% resultDes = [repelem(real(data(1:1, 1:1)), imag(data(1:1, 1:1)))'; 
%     repelem(real(data(2:2,1:1)), 1)'; repelem(real(data(3:3, 1:1)), abs(imag(data(2:2,1:1) + data(3:3, 1:1))) - 1)';
%     repelem(real(data(4:4,1:1)), 1)'; repelem(real(data(5:5, 1:1)), abs(imag(data(4:4,1:1) + data(5:5, 1:1))) - 1)';
%     repelem(real(data(6:6,1:1)), 1)'; repelem(real(data(7:7, 1:1)), abs(imag(data(6:6,1:1) + data(7:7, 1:1))) - 1)';
%     repelem(real(data(8:8,1:1)), 1)'; repelem(real(data(9:9, 1:1)), abs(imag(data(8:8,1:1) + data(9:9, 1:1))) - 1)';
%     repelem(real(data(10:10,1:1)), 1)'; repelem(real(data(11:11, 1:1)), abs(imag(data(10:10,1:1) + data(11:11, 1:1))) - 1)';
%     repelem(real(data(12:12,1:1)), 1)'; repelem(real(data(13:13, 1:1)), abs(imag(data(12:12,1:1) + data(13:13, 1:1))) - 1)';
%     repelem(real(data(14:14,1:1)), 1)'; repelem(real(data(15:15, 1:1)), abs(imag(data(14:14,1:1) + data(15:15, 1:1))) - 1)';
%     repelem(real(data(16:16,1:1)), 1)'; repelem(real(data(17:17, 1:1)), abs(imag(data(16:16,1:1) + data(17:17, 1:1))) - 1)'];

resultDes = [];
   
 for i = 1:1:length(data)
    if(mod(i, 2))
        resultDes = [resultDes; repelem(real(data(i:i, 1:1)), imag(data(i:i, 1:1)))'];
    else
        resultDes = [resultDes; repelem(0, imag(data(i:i,1:1)))'];
    end
 end

% resultDes = [repelem(real(data(1:1, 1:1)), imag(data(1:1, 1:1)))'; 
%     repelem(real(data(2:2,1:1)), imag(data(2:2,1:1)))'; repelem(real(data(3:3, 1:1)), abs(imag(data(3:3, 1:1))))';
%     repelem(real(data(4:4,1:1)), imag(data(4:4,1:1)))'; repelem(real(data(5:5, 1:1)), abs(imag(data(5:5, 1:1))))';
%     repelem(real(data(6:6,1:1)), imag(data(6:6,1:1)))'; repelem(real(data(7:7, 1:1)), abs(imag(data(7:7, 1:1))))';
%     repelem(real(data(8:8,1:1)), imag(data(8:8,1:1)))'; repelem(real(data(9:9, 1:1)), abs(imag(data(9:9, 1:1))))';
%     repelem(real(data(10:10,1:1)), imag(data(10:10,1:1)))'; repelem(real(data(11:11, 1:1)), abs(imag(data(11:11, 1:1))))';
%     repelem(real(data(12:12,1:1)), imag(data(12:12,1:1)))'; repelem(real(data(13:13, 1:1)), abs(imag(data(13:13, 1:1))))';
%     repelem(real(data(14:14,1:1)), imag(data(14:14,1:1)))'; repelem(real(data(15:15, 1:1)), abs(imag(data(15:15, 1:1))))';
%     repelem(real(data(16:16,1:1)), imag(data(16:16,1:1)))'; repelem(real(data(17:17, 1:1)), abs(imag(data(17:17, 1:1))))'];