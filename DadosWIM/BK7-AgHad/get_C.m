function [resultData, resultData1, resultData2] = get_C(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
data = data - 60;
result1 = get_region(data, 532, 570, 15);

[result2, result3] = get_create(data);

result4Aux = get_region(data, 134, 163, 20);
result4 = get_region(result4Aux, 742, 779, 11);

result5 = get_region(data, 351, 365, 7) - 5;

result6Aux = get_region(data, 134, 163, 12);
result6AAux = get_region(result6Aux, 351, 365, 15);
result6 = get_region(result6AAux, 532, 570, 5);

result7 = result1 - 3;
result7(600) = 610;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:651) + 45;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 351, 365, 25);
result44 = get_region(result44Aux, 532, 570, 14);
 
result55 = get_region(result11, 134, 163, 16);
 
result66Aux = get_region(result11, 134, 163, 16);
result66 = get_region(result66Aux, 532, 570, 18);

result77 = result22 + 2;
result77(340) = 750;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:448) + 10;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 134, 163, 22);
 
result555 = get_region(result111, 351, 365, 13);
 
result667Aux = get_region(result111, 134, 163, 20);
result667 = get_region(result667Aux, 351, 365, 8);

result777 = result667 + 5;
result777(200) = 610;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];