close all;
clear all;
clc;

load receitaBK7Ag.txt;

resultBK7AgFWHT = fwht(receitaBK7Ag, 1024);

resultIFWHT = ifwht(resultBK7AgFWHT(1:9,1:1), 1024);
resultIFWHT100 = ifwht(resultBK7AgFWHT(1:100,1:1), 1024);
resultIFWHT200 = ifwht(resultBK7AgFWHT(1:200,1:1), 1024);
resultIFWHT300 = ifwht(resultBK7AgFWHT(1:300,1:1), 1024);

figure(994753)
plot(resultIFWHT(1:840,1:1), 'b');
hold on;
plot(resultIFWHT100(1:840,1:1), 'r');
hold on;
plot(resultIFWHT200(1:840,1:1), 'k');
hold on;
plot(resultIFWHT300(1:840,1:1), 'g');

names = {'9'; '100'; '200'; '300'};

err9 = mse(resultIFWHT(1:845,1:1), receitaBK7Ag(:,1:1));
err100 = mse(resultIFWHT100(1:845,1:1), receitaBK7Ag(:,1:1));
err200 = mse(resultIFWHT200(1:845,1:1), receitaBK7Ag(:,1:1));
err300 = mse(resultIFWHT300(1:845,1:1), receitaBK7Ag(:,1:1));

errors = [err9; err100; err200; err300];

figure(264115)
plot(errors,'.r', 'MarkerSize', 20);
set(gca,'xtick',[1:4],'xticklabel',names)
xlabel('Qtd. Coeficientes');
ylabel('$ERM (\hat{\Theta})$','interpreter','latex')