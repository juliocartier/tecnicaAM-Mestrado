function [resultData, resultData1, resultData2] = get_groupC(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
result1 = get_region(data, 532, 570, 15) - 22;

[result2, result3] = get_create(data - 20);

result4Aux = get_region(data, 134, 163, 20) - 24;
result4 = get_region(result4Aux, 742, 779, 11);

result5 = get_region(data, 351, 365, 7) - 23;

result6Aux = get_region(data, 134, 163, 12) - 25;
result6AAux = get_region(result6Aux, 351, 365, 15);
result6 = get_region(result6AAux, 532, 570, 5);

result7 = result1 - 11;
result7(600) = 665;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:651) + 21;

[result22, result33] = get_create(result11 + 23);

result44Aux = get_region(result11, 351, 365, 25) + 20;
result44 = get_region(result44Aux, 532, 570, 14);
 
result55 = get_region(result11, 134, 163, 16) + 22;
 
result66Aux = get_region(result11, 134, 163, 16) + 24;
result66 = get_region(result66Aux, 532, 570, 18);

result77 = result22 + 25;
result77(340) = 750;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:448) - 30;

[result222, result333] = get_create(result111 - 26);

result444 = get_region(result111, 134, 163, 22) - 27;
 
result555 = get_region(result111, 351, 365, 13) - 29;
 
result667Aux = get_region(result111, 134, 163, 20) - 29;
result667 = get_region(result667Aux, 351, 365, 8);

result777 = result667 - 5;
result777(200) = 610;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];