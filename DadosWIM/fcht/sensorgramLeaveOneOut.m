close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A1, result_group_A2, result_group_A3, result_group_A4] = get_A(receitaBK7Ag);

[result_group_B1, result_group_B2, result_group_B3, result_group_B4] = get_B(data_group_b);

[result_group_C1, result_group_C2, result_group_C3] = get_C(data_group_c);

[result_group_D1, result_group_D2, result_group_D3, result_group_D4] = get_D(data_group_d);

resultCHTA1 = [];
resultCHTA2 = [];
resultCHTA3 = [];
resultCHTA4 = [];

resultCHTB1 = [];
resultCHTB2 = [];
resultCHTB3 = [];
resultCHTB4 = [];

resultCHTC1 = [];
resultCHTC2 = [];
resultCHTC3 = [];

resultCHTD1 = [];
resultCHTD2 = [];
resultCHTD3 = [];
resultCHTD4 = [];

for i = 1:1:7
    resultCHTA1 = [resultCHTA1; fcht(result_group_A1(:,i:i))'];
    resultCHTA2 = [resultCHTA2; fcht(result_group_A2(:,i:i))'];
    resultCHTA3 = [resultCHTA3; fcht(result_group_A3(:,i:i))'];
    
    resultCHTB1 = [resultCHTB1; fcht(result_group_B1(:,i:i))'];
    resultCHTB2 = [resultCHTB2; fcht(result_group_B2(:,i:i))'];
    resultCHTB3 = [resultCHTB3; fcht(result_group_B3(:,i:i))'];
    
    resultCHTC1 = [resultCHTC1; fcht(result_group_C1(:,i:i))'];
    resultCHTC2 = [resultCHTC2; fcht(result_group_C2(:,i:i))'];
    resultCHTC3 = [resultCHTC3; fcht(result_group_C3(:,i:i))'];
    
    resultCHTD1 = [resultCHTD1; fcht(result_group_D1(:,i:i))'];
    resultCHTD2 = [resultCHTD2; fcht(result_group_D2(:,i:i))'];
    resultCHTD3 = [resultCHTD3; fcht(result_group_D3(:,i:i))'];
end

for j = 1:1:5
    resultCHTA4 = [resultCHTA4; fcht(result_group_A4(:,j:j))'];
    
    resultCHTB4 = [resultCHTB4; fcht(result_group_B4(:,j:j))'];
    
    resultCHTD4 = [resultCHTD4; fcht(result_group_D4(:,j:j))'];
end

fchtTestDescr = fcht(result_group_A1(:,2:2));

[EnergiaReceitaA, conf] = (get_energy(fchtTestDescr));

EnergiaA = EnergiaReceitaA(89)./EnergiaReceitaA;

EnergiaAcumuladaA = cumsum(EnergiaA(2:end)); 

figure(231)
subplot(2,1,1)
plot(conf(2:20), EnergiaA(2:20), '.r', 'MarkerSize', 15);
subplot(2,1,2)
plot(conf(1:20), EnergiaAcumuladaA(1:20), '.r', 'MarkerSize', 15);

figure(3421132)
plot(abs(fchtTestDescr(1:20,:)), '.r', 'MarkerSize', 12);

% iresultCHTA1 = [];
% iresultCHTA2 = [];
% 
% for j = 1:1:7
%     iresultCHTA1 = [iresultCHTA1; ifcht(resultCHTA1(j:j,:))'];
%     iresultCHTA2 = [iresultCHTA2; ifcht(resultCHTA2(j:j,:))'];
% end
% 
% iresultCHTA1 = iresultCHTA1';
% iresultCHTA2 = iresultCHTA2';
% 
% % figure(2134)
% % plot(result_group_A1(:,1:1), 'b');
% % hold on;
% % plot(iresultCHTA1(:,1:1), 'r');

tamanho = 4;

if tamanho <= 241 

resultCHT = [abs(resultCHTA1(:,2:tamanho)) repelem(1,7)'; abs(resultCHTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultCHTA3(:,2:tamanho)) repelem(3,7)'; abs(resultCHTA4(:,2:tamanho)) repelem(4,5)';
             abs(resultCHTB1(:,2:tamanho)) repelem(5,7)'; abs(resultCHTB2(:,2:tamanho)) repelem(6,7)';
             abs(resultCHTB3(:,2:tamanho)) repelem(7,7)'; abs(resultCHTB4(:,2:tamanho)) repelem(8,5)';
             abs(resultCHTC1(:,2:tamanho)) repelem(9,7)'; abs(resultCHTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultCHTC3(:,2:tamanho)) repelem(11,7)'; abs(resultCHTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultCHTD2(:,2:tamanho)) repelem(13,7)'; abs(resultCHTD3(:,2:tamanho)) repelem(14,7)';
             abs(resultCHTD4(:,2:tamanho)) repelem(15,5)'];

elseif tamanho > 241 && tamanho <= 449
    
resultCHT = [abs(resultCHTA1(:,2:tamanho)) repelem(1,7)'; abs(resultCHTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultCHTA3(:,2:tamanho)) repelem(3,7)'; abs(resultCHTB1(:,2:tamanho)) repelem(5,7)'; 
             abs(resultCHTB2(:,2:tamanho)) repelem(6,7)'; abs(resultCHTB3(:,2:tamanho)) repelem(7,7)'; 
             abs(resultCHTC1(:,2:tamanho)) repelem(9,7)'; abs(resultCHTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultCHTC3(:,2:tamanho)) repelem(11,7)'; abs(resultCHTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultCHTD2(:,2:tamanho)) repelem(13,7)'; abs(resultCHTD3(:,2:tamanho)) repelem(14,7)'];

elseif tamanho > 449 && tamanho <= 650
    
resultCHT = [abs(resultCHTA1(:,2:tamanho)) repelem(1,7)'; abs(resultCHTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultCHTB1(:,2:tamanho)) repelem(5,7)'; abs(resultCHTB2(:,2:tamanho)) repelem(6,7)'; 
             abs(resultCHTC1(:,2:tamanho)) repelem(9,7)'; abs(resultCHTC2(:,2:tamanho)) repelem(10,7)'; 
             abs(resultCHTD1(:,2:tamanho)) repelem(12,7)'; abs(resultCHTD2(:,2:tamanho)) repelem(13,7)'];

else
    
resultCHT = [abs(resultCHTA1(:,2:tamanho)) repelem(1,7)'; abs(resultCHTB1(:,2:tamanho)) repelem(5,7)'; 
              abs(resultCHTC1(:,2:tamanho)) repelem(9,7)'; abs(resultCHTD1(:,2:tamanho)) repelem(12,7)'];

    
end

% resultDCT = [abs(resultDCTA1(:,2:4)) repelem(1,7)';  abs(resultDCTA2(:,2:4)) repelem(2,7)';
%              abs(resultDCTB1(:,2:4)) repelem(5,7)';  abs(resultDCTB2(:,2:4)) repelem(6,7)';
%              abs(resultDCTC1(:,2:4)) repelem(9,7)'; 
%              abs(resultDCTD1(:,2:4)) repelem(12,7)'];


figure(4532)
plot3(resultCHT(1:7,1:1), resultCHT(1:7,2:2), resultCHT(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultCHT(27:33,1:1), resultCHT(27:33,2:2), resultCHT(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
hold on;
plot3(resultCHT(53:59,1:1), resultCHT(53:59,2:2), resultCHT(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
hold on;
plot3(resultCHT(74:80,1:1), resultCHT(74:80,2:2), resultCHT(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
hold on;
plot3(resultCHT(8:14,1:1), resultCHT(8:14,2:2), resultCHT(8:14,3:3), 'ob', 'MarkerSize', 8);
hold on;
plot3(resultCHT(34:40,1:1), resultCHT(34:40,2:2), resultCHT(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
hold on;
plot3(resultCHT(60:66,1:1), resultCHT(60:66,2:2), resultCHT(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
hold on;
plot3(resultCHT(81:87,1:1), resultCHT(81:87,2:2), resultCHT(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
hold on;
plot3(resultCHT(15:21,1:1), resultCHT(15:21,2:2), resultCHT(15:21,3:3), '+g', 'MarkerSize', 8);
hold on;
plot3(resultCHT(41:47,1:1), resultCHT(41:47,2:2), resultCHT(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
hold on;
plot3(resultCHT(67:73,1:1), resultCHT(67:73,2:2), resultCHT(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
hold on;
plot3(resultCHT(88:94,1:1), resultCHT(88:94,2:2), resultCHT(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
hold on;
plot3(resultCHT(22:26,1:1), resultCHT(22:26,2:2), resultCHT(22:26,3:3), '*k', 'MarkerSize', 8);
hold on;
plot3(resultCHT(48:52,1:1), resultCHT(48:52,2:2), resultCHT(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
hold on;
plot3(resultCHT(95:99,1:1), resultCHT(95:99,2:2), resultCHT(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
legend('Grupo 1', 'Grupo 2', 'Grupo 3', 'Grupo 4', 'Grupo 5', 'Grupo 6', 'Grupo 7', 'Grupo 8', ...
    'Grupo 9', 'Grupo 10', 'Grupo 11', 'Grupo 12', 'Grupo 13', 'Grupo 14', 'Grupo 15');



result = [];
accuracyC = [];
acc = [];

c = cvpartition(resultCHT(:,1:1),'LeaveOut');

for i = 1:1:c.NumTestSets
    a = resultCHT(i:i,1:end);
    result = setdiff(resultCHT, a, 'stable', 'rows');
    
    mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 9, 'Distance' , 'cosine');
    res = predict(mdl, a(:,1:end-1))
    
    acc = [acc; res];
    
    class = a(:,end:end)
  
    accuracyC = [accuracyC; sum(class == res) / numel(class)];

end

% accuracy = sum(accuracyC)./length(resultCHT(:,1:1))
% 
% B = 2000;
% 
% ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');
 
% [bootreps, bootsam] = bootstrp(B,'mean', accuracyC);
% bvals = bootstrp(B,'mean', accuracyC);
