close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A1, result_group_A2, result_group_A3, result_group_A4] = get_A(receitaBK7Ag);

[result_group_B1, result_group_B2, result_group_B3, result_group_B4] = get_B(data_group_b);

[result_group_C1, result_group_C2, result_group_C3] = get_C(data_group_c);

[result_group_D1, result_group_D2, result_group_D3, result_group_D4] = get_D(data_group_d);

% resultCHTA1 = [];
% resultCHTA2 = [];
% resultCHTA3 = [];
% resultCHTA4 = [];
% 
% resultCHTB1 = [];
% resultCHTB2 = [];
% resultCHTB3 = [];
% resultCHTB4 = [];
% 
% resultCHTC1 = [];
% resultCHTC2 = [];
% resultCHTC3 = [];
% 
% resultCHTD1 = [];
% resultCHTD2 = [];
% resultCHTD3 = [];
% resultCHTD4 = [];
% 
% for i = 1:1:7
%     resultCHTA1 = [resultCHTA1; fcht(result_group_A1(:,i:i))'];
%     resultCHTA2 = [resultCHTA2; fcht(result_group_A2(:,i:i))'];
%     resultCHTA3 = [resultCHTA3; fcht(result_group_A3(:,i:i))'];
%     
%     resultCHTB1 = [resultCHTB1; fcht(result_group_B1(:,i:i))'];
%     resultCHTB2 = [resultCHTB2; fcht(result_group_B2(:,i:i))'];
%     resultCHTB3 = [resultCHTB3; fcht(result_group_B3(:,i:i))'];
%     
%     resultCHTC1 = [resultCHTC1; fcht(result_group_C1(:,i:i))'];
%     resultCHTC2 = [resultCHTC2; fcht(result_group_C2(:,i:i))'];
%     resultCHTC3 = [resultCHTC3; fcht(result_group_C3(:,i:i))'];
%     
%     resultCHTD1 = [resultCHTD1; fcht(result_group_D1(:,i:i))'];
%     resultCHTD2 = [resultCHTD2; fcht(result_group_D2(:,i:i))'];
%     resultCHTD3 = [resultCHTD3; fcht(result_group_D3(:,i:i))'];
% end
% 
% for j = 1:1:5
%     resultCHTA4 = [resultCHTA4; fcht(result_group_A4(:,j:j))'];
%     
%     resultCHTB4 = [resultCHTB4; fcht(result_group_B4(:,j:j))'];
%     
%     resultCHTD4 = [resultCHTD4; fcht(result_group_D4(:,j:j))'];
% end

resultCHTD3 = fcht(receitaBK7Ag);

energy(1,1:1) = sum(abs(resultCHTD3(2:2)));
energy(2,1:1) = sum(abs(resultCHTD3(2:3)));   
energy(3,1:1) = sum(abs(resultCHTD3(2:4)));   
energy(4,1:1) = sum(abs(resultCHTD3(2:5)));   
energy(5,1:1) = sum(abs(resultCHTD3(2:6)));

i = 10;
    while i <= length(resultCHTD3)
        energy = [energy; sum(abs(resultCHTD3(2:i)))];
        i = i + 10;
    end
    
%EnergiaA = (1./length(resultCHTD3)) * energy;
EnergiaA = energy(end)./energy;

figure(45342)
plot(EnergiaA, '.r', 'MarkerSize', 15);

% iresultCHTA1 = [];
% iresultCHTA2 = [];
% 
% for j = 1:1:7
%     iresultCHTA1 = [iresultCHTA1; ifcht(resultCHTA1(j:j,:))'];
%     iresultCHTA2 = [iresultCHTA2; ifcht(resultCHTA2(j:j,:))'];
% end
% 
% iresultCHTA1 = iresultCHTA1';
% iresultCHTA2 = iresultCHTA2';
% 
% % figure(2134)
% % plot(result_group_A1(:,1:1), 'b');
% % hold on;
% % plot(iresultCHTA1(:,1:1), 'r');
% 
% resultCHT = [abs(resultCHTA1(:,2:4)) repelem(1,7)'; abs(resultCHTA2(:,2:4)) repelem(2,7)';
%              abs(resultCHTA3(:,2:4)) repelem(3,7)'; abs(resultCHTA4(:,2:4)) repelem(4,5)';
%              abs(resultCHTB1(:,2:4)) repelem(5,7)'; abs(resultCHTB2(:,2:4)) repelem(6,7)';
%              abs(resultCHTB3(:,2:4)) repelem(7,7)'; abs(resultCHTB4(:,2:4)) repelem(8,5)';
%              abs(resultCHTC1(:,2:4)) repelem(9,7)'; abs(resultCHTC2(:,2:4)) repelem(10,7)';
%              abs(resultCHTC3(:,2:4)) repelem(11,7)'; abs(resultCHTD1(:,2:4)) repelem(12,7)';
%              abs(resultCHTD2(:,2:4)) repelem(13,7)'; abs(resultCHTD3(:,2:4)) repelem(14,7)';
%              abs(resultCHTD4(:,2:4)) repelem(15,5)']; 
% 
% accuracyC = [];
% res = [];
% class = [];
% 
% for j = 1:1:10
% 
%      aux = repelem(randperm(99,71),1)';
%      x = sort(aux);
%      train = [];
%      test = [];
% 
%    for i=1:1:length(resultCHT)
%        if find(i==x)
%         train = [train; resultCHT(i:i,1:4)];
%        else
%         test = [test; resultCHT(i:i,1:4)];
%        end
%     end
% 
%   mdl = fitcknn(train(:,1:3), train(:,4:4), 'NumNeighbors', 5);
%     
%   j
%   res = predict(mdl, test(:,1:3))
%      
%   class = test(:,4:4)
% 
%   
%  accuracyC = [accuracyC; sum(class == res) / numel(class)];
% 
% 
% end
%  accuracyCAux = sum(accuracyC)./10