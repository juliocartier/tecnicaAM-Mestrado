function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2

data = data - 25;

result1 = get_region(data, 344, 381, 22);

[result2, result3] = get_create(data);

result4Aux = get_region(data, 134, 162, 20);
result4 = get_region(result4Aux, 728, 760, 15);

result5 = get_region(data, 547, 564, 22) - 4;

result6Aux = get_region(data, 134, 162, 16);
result6AAux = get_region(result6Aux, 547, 564, 15);
result6 = get_region(result6AAux, 728, 760, 22);

result7 = result5 - 6;
result7(700) = 660;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:661) - 13;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 134, 162, 7);
result44 = get_region(result44Aux, 547, 564, 15);
 
result55 = get_region(result11, 134, 162, 18);

result66Aux = get_region(result11, 344, 381, 15);
result66 = get_region(result66Aux, 547, 564, 20);

result77 = result44 - 2.8;
result77(400) = 630;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:446) + 20;
result111(447:449) = data(445) + 20;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 344, 381, 25);
 
result555 = get_region(result111, 134, 162, 11);

result667Aux = get_region(result111, 134, 162, 10);
result667 = get_region(result667Aux, 344, 381, 15);

result777 = result555 + 4.6;
result777(165) = result777(155) + 10;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:252) + 30;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 132, 164, 25);

result5555 = result1111 + 11;
result5555(200) = 730.2;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
