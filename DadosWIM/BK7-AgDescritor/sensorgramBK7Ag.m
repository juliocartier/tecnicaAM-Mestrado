close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A, result_group_AA, result_group_AAA] = get_groupA(receitaBK7Ag);

[result_group_B, result_group_BB, result_group_BBB] = get_groupB(data_group_b);

[result_group_C, result_group_CC, result_group_CCC] = get_groupC(data_group_c);

[result_group_D, result_group_DD, result_group_DDD] = get_groupD(data_group_d);

%Descritor

[result_descrit_A, result_descrit_AA, result_descrit_AAA] = get_descriA(result_group_A(:,1:6), result_group_AA(:,1:6), result_group_AAA(:,1:6));

[result_descrit_B, result_descrit_BB, result_descrit_BBB] = get_descriB(result_group_B(:,1:6), result_group_BB(:,1:6), result_group_BBB(:,1:6));

[result_descrit_C, result_descrit_CC, result_descrit_CCC] = get_descriC(result_group_C(:,1:6), result_group_CC(:,1:6), result_group_CCC(:,1:6));

[result_descrit_D, result_descrit_DD, result_descrit_DDD] = get_descriD(result_group_D(:,1:6), result_group_DD(:,1:6), result_group_DDD(:,1:6));

% result_inv_descrit_A = get_descrit_inver(result_descrit_AAA(:,6:6));
% 
% result_inv_descrit_B = get_descrit_inver(result_descrit_BBB(:,6:6));
% 
% result_inv_descrit_C = get_descrit_inver(result_descrit_CCC(:,6:6));

result_inv_descrit_D = get_descrit_inver(result_descrit_DDD(:,6:6));

%result_inv_AA = get_descrit_inver(result_descrit_AA);

% idx = result_inv_descrit_A > 0;
% result_inv_A = idx.*result_inv_descrit_A;
% 
% idx3 = result_inv_descrit_B > 0;
% result_inv_B = idx3.*result_inv_descrit_B;
% 
% idx4 = result_inv_descrit_C > 0;
% result_inv_C = idx4.*result_inv_descrit_C;

idx5 = result_inv_descrit_D > 0;
result_inv_D = idx5.*result_inv_descrit_D;

figure(213454)
plot(result_group_A(:,6:6), 'r');
hold on;
plot(result_group_AA(:,6:6), 'b');
hold on;
plot(result_group_DDD(:,6:6), 'k');


% figure(234543)
% plot(result_group_DDD(:,6:6), 'k');
% hold on;
% plot(find(idx5~=0), result_inv_D(idx5~=0), '.r')

winSize = 6;
%resultDescr = zeros(6,5);
resultDescr = [];

%for j = 1:1:6
%     for i = 1:2:5
%       %for j = 1:1:winSize
%         
%         if(winSize <= length(result_descrit_A) && winSize <= length(result_descrit_AA) && winSize <= length(result_descrit_AAA))
%             %resultDescr = [resultDescr; real(result_descrit_A(i,1:1)) imag(result_descrit_A(i,1:1)) repelem(1,1); 
%             %    real(result_descrit_AA(i,1:1)) imag(result_descrit_AA(i,1:1)) repelem(2,1);
%             %    real(result_descrit_AAA(i,1:1)) imag(result_descrit_AAA(i,1:1)) repelem(3,1)];
%             
%             %resultDescr = [real(result_descrit_A(i,j)) imag(result_descrit_A(i,j))]; 
%             
%             resultDescr(j,i) = real(result_descrit_A(i,j));
%             
%             %resultDescr{j,i} = [real(result_descrit_A(i,j)), imag(result_descrit_A(i,j))];
%             
%            %resultDescr(resultDescr == 0) = imag(result_descrit_A(i,j))
%             
%         end
%        %end
%     end
% end
% 
% resultDescr(resultDescr == 0) = imag(result_descrit_A(1:2,1:6))';




   