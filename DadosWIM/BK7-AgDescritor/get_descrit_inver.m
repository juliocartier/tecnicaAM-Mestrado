function [result] = get_descrit_inver(data)

result = [];
   
 for i = 1:1:length(data)
    if(mod(i, 2))
        result = [result; repelem(real(data(i:i, 1:1)), imag(data(i:i, 1:1)))'];
    else
        %result = [result; repelem(0, imag(data(i:i,1:1)))'];
        result = [result; repelem(real(data(i:i, 1:1)), imag(data(i:i,1:1)))'];
    end
 end

end