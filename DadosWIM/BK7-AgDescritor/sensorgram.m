close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A, result_group_AA, result_group_AAA, result_group_AAAA] = get_A(receitaBK7Ag);

[result_group_B, result_group_BB, result_group_BBB, result_group_BBBB] = get_B(data_group_b);

[result_group_C, result_group_CC, result_group_CCC] = get_C(data_group_c);

[result_group_D, result_group_DD, result_group_DDD, result_group_DDDD] = get_D(data_group_d);

%Descritor

[result_descrit_A, result_descrit_AA, result_descrit_AAA, result_descrit_AAAA] = get_descriA(result_group_A(:,1:7), result_group_AA(:,1:7), result_group_AAA(:,1:7), result_group_AAAA(:,1:5));

[result_descrit_B, result_descrit_BB, result_descrit_BBB, result_descrit_BBBB] = get_descriB(result_group_B(:,1:7), result_group_BB(:,1:7), result_group_BBB(:,1:7), result_group_BBBB(:,1:5));

[result_descrit_C, result_descrit_CC, result_descrit_CCC] = get_descriC(result_group_C(:,1:7), result_group_CC(:,1:7), result_group_CCC(:,1:7));

[result_descrit_D, result_descrit_DD, result_descrit_DDD, result_descrit_DDDD] = get_descriD(result_group_D(:,1:7), result_group_DD(:,1:7), result_group_DDD(:,1:7), result_group_DDDD(:,1:5));

posicoes = [22; 99; 198; 292; 400; 508; 607; 702; 800];

resultTSD = [real(result_descrit_AAAA(1:2:end,2:2)) imag(result_descrit_AAAA(1:2:end,2:2))]

%regiPerm = {'RP1'; 'RP2'; 'RP3'; 'RP4'; 'RP5'; 'RP6'; 'RP7'; 'RP8'; 'RP9'};
regiPerm = {'RP1'; 'RP2'; 'RP3'};

T = table(resultTSD(:,1:1), resultTSD(:,2:2), 'RowNames', regiPerm);

figure(9043)
uitable('Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
    'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);

% figure(8473)
% plot((1:length(result_group_A(:,2:2))), result_group_A(:,2:2),'r','LineWidth', 1.5)
% hold on;
% plot(posicoes(:,1:1), resultTSD(:,1:1), '.b', 'MarkerSize', 20);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');




result_inv = get_descrit_inver(result_descrit_A(:,1:1));

idx = result_inv > 0;
result_inv_A = idx.*result_inv;

winSize = 5;

if (winSize <= 3)

    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    resultA3 = verificaTamanho(result_descrit_AAA, winSize);
    resultA4 = verificaTamanho(result_descrit_AAAA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    resultB3 = verificaTamanho(result_descrit_BBB, winSize);
    resultB4 = verificaTamanho(result_descrit_BBBB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    resultC3 = verificaTamanho(result_descrit_CCC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    resultD3 = verificaTamanho(result_descrit_DDD, winSize);
    resultD4 = verificaTamanho(result_descrit_DDDD, winSize);

  resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultA4 repelem(4,length(resultA4(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))';
                     resultB3 repelem(7,length(resultB3(:,1:1)))'; resultB4 repelem(8,length(resultB4(:,1:1)))';
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))'; 
                     resultC3 repelem(11,length(resultC3(:,1:1)))';
                     resultD1 repelem(12,length(resultD1(:,1:1)))'; resultD2 repelem(13,length(resultD2(:,1:1)))';
                     resultD3 repelem(14,length(resultD3(:,1:1)))'; resultD4 repelem(15,length(resultD4(:,1:1)))'];

  
elseif(winSize > 3 && winSize <= 5)
     
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    resultA3 = verificaTamanho(result_descrit_AAA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    resultB3 = verificaTamanho(result_descrit_BBB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    resultC3 = verificaTamanho(result_descrit_CCC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    resultD3 = verificaTamanho(result_descrit_DDD, winSize);
    
resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))';
                     resultB2 repelem(6,length(resultB2(:,1:1)))'; resultB3 repelem(7,length(resultB3(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultC3 repelem(11,length(resultC3(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))';
                     resultD2 repelem(13,length(resultD2(:,1:1)))'; resultD3 repelem(14,length(resultD3(:,1:1)))'];

elseif(winSize > 5 && winSize <= 7)
    
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    
resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultD1 repelem(12,length(resultD1(:,1:1)))'; resultD2 repelem(13,length(resultD2(:,1:1)))'];
                 
else
    
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    
 resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))'];
    
end

% for j=1:2:length(result_descrit_A(:, 1:1))
%     hold on;
%     plot(real(result_descrit_A(j, :)), imag(result_descrit_A(j, :)), '.r', 'MarkerSize', 12);
%     hold on;
%     plot(real(result_descrit_B(j, :)), imag(result_descrit_B(j, :)), '.b', 'MarkerSize', 12);
% end

% figure(2354)
% plot(real(result_descrit_A(1:2:6, 1:end)), imag(result_descrit_A(1:2:6, 1:end)), '.r', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_AA(1:2:6, 1:end)), imag(result_descrit_AA(1:2:6, 1:end)), '.y', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_B(1:2:6, 1:end)), imag(result_descrit_B(1:2:6, 1:end)), '.g', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_BB(1:2:6, 1:end)), imag(result_descrit_BB(1:2:6, 1:end)), '.m', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_C(1:2:6, 1:end)), imag(result_descrit_C(1:2:6, 1:end)), '.k', 'MarkerSize', 12);
% hold on;
% plot(real(result_descrit_D(1:2:6, 1:end)), imag(result_descrit_D(1:2:6, 1:end)), '.b', 'MarkerSize', 12);


 figure(1111)
 plot(real(result_descrit_AAAA(1:1,:)), imag(result_descrit_AAAA(1:1,:)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_BBBB(1:1,:)), imag(result_descrit_BBBB(1:1,:)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_DDDD(1:1,1:4)), imag(result_descrit_DDDD(1:1,1:4)) , '.g', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_DDDD(1:1,5:5)), imag(result_descrit_DDDD(1:1,5:5)) , '.k', 'MarkerSize', 20);
 
 figure(2222)
 plot(real(result_descrit_AAAA(3:3,:)), imag(result_descrit_AAAA(3:3,:)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_BBBB(3:3,:)), imag(result_descrit_BBBB(3:3,:)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_DDDD(3:3,1:4)), imag(result_descrit_DDDD(3:3,1:4)), '.g', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_DDDD(3:3,5:5)), imag(result_descrit_DDDD(3:3,5:5)), '.k', 'MarkerSize', 20);
 
 figure(3333)
 plot(real(result_descrit_AAAA(5:5,:)), imag(result_descrit_AAAA(5:5,:)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_BBBB(5:5,:)), imag(result_descrit_BBBB(5:5,:)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_DDDD(5:5,1:4)), imag(result_descrit_DDDD(5:5,1:4)), '.g', 'MarkerSize', 20);
 hold on;
 plot(real(result_descrit_DDDD(5:5,5:5)), imag(result_descrit_DDDD(5:5,5:5)), '.k', 'MarkerSize', 20);

result = [];
accuracyC = [];
accuracyCAux = [];
acc = [];

c = cvpartition(resultTotDescrt(:,1:1),'LeaveOut');

    for i = 1:1:c.NumTestSets
        a = resultTotDescrt(i:i,1:end);
        result = setdiff(resultTotDescrt, a, 'stable', 'rows');

        mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 5, 'Distance' , 'chebychev');
        res = predict(mdl, a(:,1:end-1))
        
        acc = [acc; res];

        class = a(:,end:end)

        accuracyC = [accuracyC; sum(class == res) / numel(class)];
        %accuracyC = sum(class == res) / numel(class);

    end

    accuracyCAux = sum(accuracyC)./length(resultTotDescrt(:,1:1))
%     
% B = 2000;
% % thetahat = var(lambda);
%  
% %[bootreps, bootsam] = bootstrp(B,'mean', acc);
% 
% %ci = bootci(B,@mean, acc, 'alpha', 0.01)
% ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');

% figure(2296)
% plot3(resultA1(1:7,1:1), resultA1(1:7,2:2), resultA1(1:7,3:3), '.r', 'MarkerSize', 12);
% hold on;
% plot3(resultA2(1:7,1:1), resultA2(1:7,2:2), resultA2(1:7,3:3), '*b', 'MarkerSize', 12);
% hold on;
% plot3(resultA3(1:7,1:1), resultA3(1:7,2:2), resultA3(1:7,3:3), 'xk', 'MarkerSize', 12);
% hold on;
% % plot3(resultA4(1:5,1:1), resultA4(1:5,2:2), resultA4(1:5,3:3), 'og', 'MarkerSize', 12);
% % hold on;
% plot3(resultB1(1:7,1:1), resultB1(1:7,2:2), resultB1(1:7,3:3), '>m', 'MarkerSize', 12);
% hold on;
% plot3(resultB2(1:7,1:1), resultB2(1:7,2:2), resultB2(1:7,3:3), '.m', 'MarkerSize', 12);
% hold on;
% plot3(resultC1(1:7,1:1), resultC1(1:7,2:2), resultC1(1:7,3:3), '<k', 'MarkerSize', 12);
% hold on;
% plot3(resultC2(1:7,1:1), resultC2(1:7,2:2), resultC2(1:7,3:3), 'pentagram', 'MarkerSize', 12);
% hold on;
% plot3(resultD1(1:7,1:1), resultD1(1:7,2:2), resultD1(1:7,3:3), 'xb', 'MarkerSize', 12);
% hold on;
% plot3(resultD2(1:7,1:1), resultD2(1:7,2:2), resultD2(1:7,3:3), 'square', 'MarkerSize', 12, 'Color', 'g');