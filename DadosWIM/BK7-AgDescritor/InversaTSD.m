close all;
clear all;
clc;

load receitaBK7Ag.txt;

[result_group_A, result_group_AA, result_group_AAA, result_group_AAAA] = get_A(receitaBK7Ag);

[result_descrit_A, ~, ~, ~] = get_descriA(result_group_A(:,1:7), result_group_AA(:,1:7), result_group_AAA(:,1:7), result_group_AAAA(:,1:5));

iDTS9 = get_descrit_inver(result_descrit_A(:,1:1));

errMQ = mse(iDTS9, receitaBK7Ag)

errors = [errMQ; 2.312; 0.953; 0.09];

names = {'9'; '100'; '200'; '300'};
figure(264115)
plot(errors,'.r', 'MarkerSize', 20);
set(gca,'xtick',[1:4],'xticklabel',names)
xlabel('Qtd. Coeficientes');
ylabel('$ERM (\hat{\Theta})$','interpreter','latex')