function [resultNewData, resultData] = get_create(data)

resultNewData = [];
resultData = [];

for i=1:length(data)
    if (isempty(resultNewData))
        resultNewData = data - 1;
    else
        resultData = awgn(data + 4, 85, 'measured');
    end
end