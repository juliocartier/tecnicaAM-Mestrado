function a = fct2(f,l)
%x=-cos(pi/N*((0:N-1)'+1/2));

f=f(end:-1:1,:);
A=size(f); N=A(1); 
if exist('A(3)','var') && A(3)~=1
    for i=1:A(3)
        
        a(:,:,i)=sqrt(2/N)*dct(f(:,:,i));
        a(1,:,i)=a(1,:,i)/sqrt(2);
      
    end
else

   a=sqrt(2/N)*dct(f(:,:,l));
   a(1,:)=a(1,:)/sqrt(2);

end