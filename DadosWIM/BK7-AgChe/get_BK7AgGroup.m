function [groupB, groupC, groupD] = get_BK7AgGroup(data)

groupB = [data(1:46); data(243:356); data(357:448); data(47:173); data(174:242); data(449:564); data(565:661); data(662:764); data(765:845)]; 

groupC = [data(1:46); data(243:356); data(357:448); data(449:564); data(565:661); data(662:764); data(765:845); data(47:173); data(174:242)];

groupD = [data(1:46); data(449:564); data(565:661); data(662:764); data(765:845); data(47:173); data(174:242); data(243:356); data(357:448)];
    
groupC(649:651) = 663.1600;
groupD(444:445) = 663.1600;

end

