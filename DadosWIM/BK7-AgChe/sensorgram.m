close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A1, result_group_A2, result_group_A3, result_group_A4] = get_A(receitaBK7Ag);

[result_group_B1, result_group_B2, result_group_B3, result_group_B4] = get_B(data_group_b);

[result_group_C1, result_group_C2, result_group_C3] = get_C(data_group_c);

[result_group_D1, result_group_D2, result_group_D3, result_group_D4] = get_D(data_group_d);

% resultDCTA1 = get_dct(result_group_A1)';

% resultDCT = [abs(resultDCTA1(:,2:4)) repelem(1,7)'; abs(resultDCTA2(:,2:4)) repelem(2,7)';
%              abs(resultDCTA3(:,2:4)) repelem(3,7)'; abs(resultDCTA4(:,2:4)) repelem(4,5)';
%              abs(resultDCTB1(:,2:4)) repelem(5,7)'; abs(resultDCTB2(:,2:4)) repelem(6,7)';
%              abs(resultDCTB3(:,2:4)) repelem(7,7)'; abs(resultDCTB4(:,2:4)) repelem(8,5)';
%              abs(resultDCTC1(:,2:4)) repelem(9,7)'; abs(resultDCTC2(:,2:4)) repelem(10,7)';
%              abs(resultDCTC3(:,2:4)) repelem(11,7)'; abs(resultDCTD1(:,2:4)) repelem(12,7)';
%              abs(resultDCTD2(:,2:4)) repelem(13,7)'; abs(resultDCTD3(:,2:4)) repelem(14,7)';
%              abs(resultDCTD4(:,2:4)) repelem(15,5)']; 

% resultDCT = [abs(resultDCTA1(:,2:4)) repelem(1,7)';  abs(resultDCTA2(:,2:4)) repelem(2,7)';
%              abs(resultDCTB1(:,2:4)) repelem(5,7)';  abs(resultDCTB2(:,2:4)) repelem(6,7)';
%              abs(resultDCTC1(:,2:4)) repelem(9,7)'; 
%              abs(resultDCTD1(:,2:4)) repelem(12,7)'];



% result = [];
% accuracyC = [];
% 
% for i = 1:1:length(resultDCT(:,1:1))
%     a = resultDCT(i:i,1:4);
%     result = setdiff(resultDCT, a, 'stable', 'rows');
%     
%     mdl = fitcknn(result(:,1:3), result(:,4:4), 'NumNeighbors', 7, 'Distance' , 'euclidean');
%     res = predict(mdl, a(:,1:3))
%     
%     class = a(:,4:4)
%     
% %      [Train, Test] = crossvalind('LeaveMOut', res, 1);
%   
%     accuracyC = [accuracyC; sum(class == res) / numel(class)];
%     
% %    value(:,1:4) = setdiff(resultDCT(:,1:4), a(1:1,1:4));
% %     if i == cont
% %         aux(i:i,1:4) = resultDCT(i:i,1:4);
% %         dataVetor(:,1:4) = resultDCT(2:end,1:4);
% %     end
% end
% 
% accuracy = sum(accuracyC)./length(resultDCT(:,1:1))