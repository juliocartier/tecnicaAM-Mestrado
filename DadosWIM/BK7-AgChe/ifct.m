function u = ifct(uh)
% Inverse Fast Chebyshev Transform 
%
%    uh : Discrete Chebyshev Transform Coefficients.
%    u  : Function values evaluted at Chebyshev Gauss Lobatto nodes
%         with nodes ordered increasingly x_i=[-1,...,1}
%         for i=1,2...,N
%
% By Allan P. Engsig-Karup, apek@imm.dtu.dk.

N = length(uh);
u=fft([uh(1); [uh(2:N-1); uh(N)*2; uh(N-1:-1:2)]*0.5]); 
u=(u(N:-1:1)); % reverse ordering due to Matlab's fft.
return

