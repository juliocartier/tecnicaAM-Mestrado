close all;
clear all;
clc;

load receitaBK7Ag.txt;

result = get_descritT(receitaBK7Ag);

result_inv = get_descrit_inver(result);

idx = result_inv > 0;
result_inv_A = idx.*result_inv;

figure(1231489)
% plot(receitaBK7Ag);
% hold on;
plot(find(idx~=0), result_inv(idx~=0), 'r');

% energy(1,1:1) = sum(real(result(1:2:1)));
% energy(2,1:1) = sum(real(result(1:2:3)));   
% energy(3,1:1) = sum(real(result(1:2:5)));   
% energy(4,1:1) = sum(real(result(1:2:7)));   
% energy(5,1:1) = sum(real(result(1:2:9)));
% energy(6,1:1) = sum(real(result(1:2:11)));
% energy(7,1:1) = sum(real(result(1:2:13)));
% energy(8,1:1) = sum(real(result(1:2:15)));
% energy(9,1:1) = sum(real(result(1:2:17)));
% 
% EnergiaA = energy(end)./energy;
% 
% figure(324)
% plot(EnergiaA, '.r', 'MarkerSize', 15);