function [resultData, resultData2, resultData3, resultData4] = get_groupD(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = get_region(data, 326, 359, 15) + 29;

[result2, result3] = get_create(data + 28);

result4Aux = get_region(data, 144, 159, 22) + 27;
result4 = get_region(result4Aux, 726, 755, 15);

result5 = get_region(data, 535, 573, 13) + 30;

result6Aux = get_region(data, 144, 159, 15) + 26;
result6AAux = get_region(result6Aux, 535, 573, 5);
result6 = get_region(result6AAux, 726, 755, 17);

result7 = result3 + 5;
result7(750) = 740;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:639) + 35;

[result22, result33] = get_create(result11 + 30);

result44Aux = get_region(result11, 144, 159, 17) + 31;
result44 = get_region(result44Aux, 326, 359, 20);

result55 = get_region(result11, 535, 573, 26) + 32;

result66Aux = get_region(result11, 144, 159, 25) + 34;
result66 = get_region(result66Aux, 535, 573, 13);

result77 = result66 + 14.8;
result77(200) = 760;
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:445) - 37;

[result222, result333] = get_create(result111 - 35);

result444 = get_region(result111, 144, 159, 12) - 36;

result555 = get_region(result111, 326, 359, 15) - 38;

result667Aux = get_region(result111, 144, 159, 16) - 39;
result667 = get_region(result667Aux, 326, 359, 14);

result777 = result111 - 16;
result777(370) = 620;

result1111 = data(1:241) + 36;

[result2222, result3333] = get_create(result1111 + 6);

result4444 = get_region(result1111, 144, 159, 15) + 7;

result5555 = result3333 + 20;
result5555(175) = 745; 

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22-0.4 result33+0.3 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];