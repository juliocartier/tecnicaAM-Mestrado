close all;
clear all;
clc;

load receitaBK7Ag.txt;

resultBK7AgDCT = dct(receitaBK7Ag);

resultIDCT = idct(resultBK7AgDCT(1:9,1:1), 845);
resultIDCT100 = idct(resultBK7AgDCT(1:100,1:1), 845);
resultIDCT200 = idct(resultBK7AgDCT(1:200,1:1), 845);
resultIDCT300 = idct(resultBK7AgDCT(1:300,1:1), 845);

figure(994753)
plot(resultIDCT, 'b');
hold on;
plot(resultIDCT100, 'r');
hold on;
plot(resultIDCT200, 'k');
hold on;
plot(resultIDCT300, 'g');

names = {'9'; '100'; '200'; '300'};

err9 = mse(resultIDCT, receitaBK7Ag(:,1:1));
err100 = mse(resultIDCT100, receitaBK7Ag(:,1:1));
err200 = mse(resultIDCT200, receitaBK7Ag(:,1:1));
err300 = mse(resultIDCT300, receitaBK7Ag(:,1:1));

errors = [err9; err100; err200; err300];

figure(264115)
plot(errors,'.r', 'MarkerSize', 20);
set(gca,'xtick',[1:4],'xticklabel',names)
xlabel('Qtd. Coeficientes');
ylabel('$ERM (\hat{\Theta})$','interpreter','latex')

