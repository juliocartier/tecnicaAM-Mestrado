function [resultData, resultData1, resultData2, resultData3] = get_groupB(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2
result1 = get_region(data, 344, 381, 22) + 11;

[result2, result3] = get_create(data + 12);

result4Aux = get_region(data, 134, 162, 20) + 15;
result4 = get_region(result4Aux, 728, 760, 15);

result5 = get_region(data, 547, 564, 22) + 13;

result6Aux = get_region(data, 134, 162, 16) + 14;
result6AAux = get_region(result6Aux, 547, 564, 15);
result6 = get_region(result6AAux, 728, 760, 22);

result7 = result5 + 15.7;
result7(700) = 780;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:661) - 13;

[result22, result33] = get_create(result11 - 10);

result44Aux = get_region(result11, 134, 162, 7) - 11;
result44 = get_region(result44Aux, 547, 564, 15);
 
result55 = get_region(result11, 134, 162, 18) - 12;

result66Aux = get_region(result11, 344, 381, 15) - 14;
result66 = get_region(result66Aux, 547, 564, 20);

result77 = result44 - 14.8;
result77(400) = 650;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:446) - 16;
result111(447:449) = data(445) - 16;

[result222, result333] = get_create(result111 -15);

result444 = get_region(result111, 344, 381, 25) - 17;
 
result555 = get_region(result111, 134, 162, 11) - 19;

result667Aux = get_region(result111, 134, 162, 10) - 18;
result667 = get_region(result667Aux, 344, 381, 15);

result777 = result555 + 18.6;
result777(155) = 660;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:241) + 17;

[result2222, result3333] = get_create(result1111 + 18);

result4444 = get_region(result1111, 132, 164, 25) + 15;

result5555 = result1111 + 11;
result5555(190) = 710.2;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
