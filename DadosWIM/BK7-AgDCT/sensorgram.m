close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A1, result_group_A2, result_group_A3, result_group_A4] = get_A(receitaBK7Ag);

[result_group_B1, result_group_B2, result_group_B3, result_group_B4] = get_B(data_group_b);

[result_group_C1, result_group_C2, result_group_C3] = get_C(data_group_c);

[result_group_D1, result_group_D2, result_group_D3, result_group_D4] = get_D(data_group_d);

% figure(34532)
% subplot(2,2,1)
% plot(receitaBK7Ag, 'r', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,2)
% plot(data_group_b, '--b', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,3)
% plot(data_group_c, '-.g', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,4)
% plot(data_group_d, ':k', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');

% figure(345322)
% subplot(2,2,1)
% plot(result_group_A2(:,1:1) - 16, 'r', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,2)
% plot(result_group_B2(:,1:1) + 38, '--b', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,3)
% plot(result_group_C2(:,1:1) + 15, '-.g', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,4)
% plot(result_group_D2(:,1:1) - 40, ':k', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');

% figure(345322)
% subplot(2,2,1)
% plot(result_group_A3(:,1:1) + 25, 'r', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,2)
% plot(result_group_B3(:,1:1) + 5, '--b', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,3)
% plot(result_group_C3(:,1:1) + 50, '-.g', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,4)
% plot(result_group_D3(:,1:1) + 47, ':k', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');

% figure(3453112)
% subplot(2,2,1)
% plot(result_group_A4(:,1:1) - 20, 'r', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,2)
% plot(result_group_B4(:,1:1) - 5, '--b', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,3)
% plot(result_group_D4(:,1:1) - 50, ':k', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');


% figure(3452)
% subplot(2,2,1)
% plot(result_group_A1(:,3:3) - 6, 'r', 'LineWidth', 1.8);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,2)
% plot(result_group_A1(:,2:2), '--r', 'LineWidth', 1.8);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,3)
% plot(result_group_A1(:,4:4), '-.r', 'LineWidth', 1.8);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,4)
% plot(result_group_A1(:,7:7) - 3.5, ':r', 'LineWidth', 1.8);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');

% figure(3453211)
% subplot(2,2,1)
% plot(receitaBK7Ag, 'r', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,2)
% plot(receitaBK7Ag, '--b', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,3)
% plot(data_group_c, '-.g', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');
% subplot(2,2,4)
% plot(data_group_d, ':k', 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('\lambda_r (nm)');


resultData = [];
cont = 0;
MSEResult = [];

for i = 40:10:70
        cont = cont + 1;
        resultData = [resultData awgn(receitaBK7Ag, i, 'measured')];
        figure(i)
        plot((1:845), resultData(:,cont:cont))
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
        
        MSEResult = [MSEResult; mse(resultData(:,cont:cont), receitaBK7Ag(:,1:1))];
end

% figure(98741)
% plot(MSEResult, '.', 'MarkerSize', 15)
% xlabel('Qtd. de Sensorgramas');
% ylabel('$ERM (\hat{\Theta})$','interpreter','latex')


resultDCTA1 = get_dct(result_group_A1)';
resultDCTA2 = get_dct(result_group_A2)';
resultDCTA3 = get_dct(result_group_A3)';
resultDCTA4 = get_dct(result_group_A4)';

% resultData343 = resultDCTA1(1:1,:)';
% 
% [EnergiaReceitaA, conf] = get_energy(resultData343);
% 
% EnergiaA = EnergiaReceitaA(89)./EnergiaReceitaA;
% 
% %power = 1/length(resultData343) * EnergiaReceitaA;
% 
% EnergiaAcumuladaA = cumsum(EnergiaA(2:end)); 
% 
% figure(231)
% subplot(2,1,1)
% plot(conf(2:20), EnergiaA(2:20), '.r', 'MarkerSize', 15);
% subplot(2,1,2)
% plot(conf(1:20), EnergiaAcumuladaA(1:20), '.r', 'MarkerSize', 15);
% 
% resultDCTTest = dct(result_group_A1(:,2:2));
% 
% figure(3421132)
% plot(abs(resultDCTTest(2:end,:)), '.r', 'MarkerSize', 12);



resultDCTB1 = get_dct(result_group_B1)';
resultDCTB2 = get_dct(result_group_B2)';
resultDCTB3 = get_dct(result_group_B3)';
resultDCTB4 = get_dct(result_group_B4)';

resultDCTC1 = get_dct(result_group_C1)';
resultDCTC2 = get_dct(result_group_C2)';
resultDCTC3 = get_dct(result_group_C3)';

resultDCTD1 = get_dct(result_group_D1)';
resultDCTD2 = get_dct(result_group_D2)';
resultDCTD3 = get_dct(result_group_D3)';
resultDCTD4 = get_dct(result_group_D4)';

tamanho = 4;

if tamanho <= 241

resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultDCTA3(:,2:tamanho)) repelem(3,7)'; abs(resultDCTA4(:,2:tamanho)) repelem(4,5)';
             abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; abs(resultDCTB2(:,2:tamanho)) repelem(6,7)';
             abs(resultDCTB3(:,2:tamanho)) repelem(7,7)'; abs(resultDCTB4(:,2:tamanho)) repelem(8,5)';
             abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultDCTC3(:,2:tamanho)) repelem(11,7)'; abs(resultDCTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultDCTD2(:,2:tamanho)) repelem(13,7)'; abs(resultDCTD3(:,2:tamanho)) repelem(14,7)';
             abs(resultDCTD4(:,2:tamanho)) repelem(15,5)'];
         
elseif tamanho > 241 && tamanho <= 449
    
resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultDCTA3(:,2:tamanho)) repelem(3,7)'; abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; 
             abs(resultDCTB2(:,2:tamanho)) repelem(6,7)'; abs(resultDCTB3(:,2:tamanho)) repelem(7,7)'; 
             abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultDCTC3(:,2:tamanho)) repelem(11,7)'; abs(resultDCTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultDCTD2(:,2:tamanho)) repelem(13,7)'; abs(resultDCTD3(:,2:tamanho)) repelem(14,7)'];
         
elseif tamanho > 449 && tamanho <= 661
    
resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; abs(resultDCTB2(:,2:tamanho)) repelem(6,7)';  
             abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultDCTD1(:,2:tamanho)) repelem(12,7)'; abs(resultDCTD2(:,2:tamanho)) repelem(13,7)'];
        
else
   
    resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; 
                 abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTD1(:,2:tamanho)) repelem(12,7)'];
         
end

% resultDCT = [abs(resultDCTA1(:,2:4)) repelem(1,7)';  abs(resultDCTA2(:,2:4)) repelem(2,7)';
%              abs(resultDCTB1(:,2:4)) repelem(5,7)';  abs(resultDCTB2(:,2:4)) repelem(6,7)';
%              abs(resultDCTC1(:,2:4)) repelem(9,7)'; 
%              abs(resultDCTD1(:,2:4)) repelem(12,7)'];

figure(4532)
plot3(resultDCT(1:7,1:1), resultDCT(1:7,2:2), resultDCT(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultDCT(27:33,1:1), resultDCT(27:33,2:2), resultDCT(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
hold on;
plot3(resultDCT(53:59,1:1), resultDCT(53:59,2:2), resultDCT(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
hold on;
plot3(resultDCT(74:80,1:1), resultDCT(74:80,2:2), resultDCT(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
hold on;
plot3(resultDCT(8:14,1:1), resultDCT(8:14,2:2), resultDCT(8:14,3:3), 'ob', 'MarkerSize', 8);
hold on;
plot3(resultDCT(34:40,1:1), resultDCT(34:40,2:2), resultDCT(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
hold on;
plot3(resultDCT(60:66,1:1), resultDCT(60:66,2:2), resultDCT(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
hold on;
plot3(resultDCT(81:87,1:1), resultDCT(81:87,2:2), resultDCT(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
hold on;
plot3(resultDCT(15:21,1:1), resultDCT(15:21,2:2), resultDCT(15:21,3:3), '+g', 'MarkerSize', 8);
hold on;
plot3(resultDCT(41:47,1:1), resultDCT(41:47,2:2), resultDCT(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
hold on;
plot3(resultDCT(67:73,1:1), resultDCT(67:73,2:2), resultDCT(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
hold on;
plot3(resultDCT(88:94,1:1), resultDCT(88:94,2:2), resultDCT(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
hold on;
plot3(resultDCT(22:26,1:1), resultDCT(22:26,2:2), resultDCT(22:26,3:3), '*k', 'MarkerSize', 8);
hold on;
plot3(resultDCT(48:52,1:1), resultDCT(48:52,2:2), resultDCT(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
hold on;
plot3(resultDCT(95:99,1:1), resultDCT(95:99,2:2), resultDCT(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
legend('Grupo 1', 'Grupo 2', 'Grupo 3', 'Grupo 4', 'Grupo 5', 'Grupo 6', 'Grupo 7', 'Grupo 8', ...
    'Grupo 9', 'Grupo 10', 'Grupo 11', 'Grupo 12', 'Grupo 13', 'Grupo 14', 'Grupo 15');


% figure(4532)
% plot3(resultDCT(1:7,1:1), resultDCT(1:7,2:2), resultDCT(1:7,3:3), '.r', 'MarkerSize', 20);
% hold on;
% plot3(resultDCT(8:14,1:1), resultDCT(8:14,2:2), resultDCT(8:14,3:3), 'ob', 'MarkerSize', 8);
% hold on;
% plot3(resultDCT(15:21,1:1), resultDCT(15:21,2:2), resultDCT(15:21,3:3), '+g', 'MarkerSize', 8);
% hold on;
% plot3(resultDCT(22:26,1:1), resultDCT(22:26,2:2), resultDCT(22:26,3:3), '*k', 'MarkerSize', 8);
% hold on;
% plot3(resultDCT(27:33,1:1), resultDCT(27:33,2:2), resultDCT(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
% hold on;
% plot3(resultDCT(34:40,1:1), resultDCT(34:40,2:2), resultDCT(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
% hold on;
% plot3(resultDCT(41:47,1:1), resultDCT(41:47,2:2), resultDCT(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
% hold on;
% plot3(resultDCT(48:52,1:1), resultDCT(48:52,2:2), resultDCT(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
% hold on;
% plot3(resultDCT(53:59,1:1), resultDCT(53:59,2:2), resultDCT(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
% hold on;
% plot3(resultDCT(60:66,1:1), resultDCT(60:66,2:2), resultDCT(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
% hold on;
% plot3(resultDCT(67:73,1:1), resultDCT(67:73,2:2), resultDCT(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
% hold on;
% plot3(resultDCT(74:80,1:1), resultDCT(74:80,2:2), resultDCT(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
% hold on;
% plot3(resultDCT(81:87,1:1), resultDCT(81:87,2:2), resultDCT(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
% hold on;
% plot3(resultDCT(88:94,1:1), resultDCT(88:94,2:2), resultDCT(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
% hold on;
% plot3(resultDCT(95:99,1:1), resultDCT(95:99,2:2), resultDCT(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
% legend('Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5', 'Group 6', 'Group 7', 'Group 8', ...
%     'Group 9', 'Group 10', 'Group 11', 'Group 12', 'Group 13', 'Group 14', 'Group 15');

% result = [];
% accuracyC = [];
% 
% for i = 1:1:length(resultDCT(:,1:1))
%     a = resultDCT(i:i,1:end);
%     result = setdiff(resultDCT, a, 'stable', 'rows');
%     
%     mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors',  9, 'Distance' , 'cosine');
%     res = predict(mdl, a(:,1:end-1));
%     
%     class = a(:,end:end);
%     
% %      [Train, Test] = crossvalind('LeaveMOut', res, 1);
%   
%     accuracyC = [accuracyC; sum(class == res) / numel(class)];
%     
% %    value(:,1:4) = setdiff(resultDCT(:,1:4), a(1:1,1:4));
% %     if i == cont
% %         aux(i:i,1:4) = resultDCT(i:i,1:4);
% %         dataVetor(:,1:4) = resultDCT(2:end,1:4);
% %     end
% end

% accuracy = sum(accuracyC)./length(resultDCT(:,1:1))
% 
% B = 2000;
% 
% ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');
% thetahat = var(lambda);
 
% [bootreps, bootsam] = bootstrp(B,'mean', accuracyC);
% bvals = bootstrp(B,'mean', accuracyC);