function [resultNewData, resultData] = get_create(data)

resultNewData = [];
resultData = [];

for i=1:length(data)
    if (isempty(resultNewData))
        resultNewData = data + 8;
    else
        resultData = awgn(data + 6, 65, 'measured');
    end
end