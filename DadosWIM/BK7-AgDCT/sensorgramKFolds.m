close all;
clear all;
clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A1, result_group_A2, result_group_A3, result_group_A4] = get_A(receitaBK7Ag);

[result_group_B1, result_group_B2, result_group_B3, result_group_B4] = get_B(data_group_b);

[result_group_C1, result_group_C2, result_group_C3] = get_C(data_group_c);

[result_group_D1, result_group_D2, result_group_D3, result_group_D4] = get_D(data_group_d);

figure(2341)
plot(result_group_A4, 'r');
hold on;
plot(result_group_B4, 'b');
hold on;
plot(result_group_D4, 'k');

size = 242;

if size <= 241

resultDCTA1 = get_dct(result_group_A1)';
resultDCTA2 = get_dct(result_group_A2)';
resultDCTA3 = get_dct(result_group_A3)';
resultDCTA4 = get_dct(result_group_A4)';

resultDCTB1 = get_dct(result_group_B1)';
resultDCTB2 = get_dct(result_group_B2)';
resultDCTB3 = get_dct(result_group_B3)';
resultDCTB4 = get_dct(result_group_B4)';

resultDCTC1 = get_dct(result_group_C1)';
resultDCTC2 = get_dct(result_group_C2)';
resultDCTC3 = get_dct(result_group_C3)';

resultDCTD1 = get_dct(result_group_D1)';
resultDCTD2 = get_dct(result_group_D2)';
resultDCTD3 = get_dct(result_group_D3)';
resultDCTD4 = get_dct(result_group_D4)';

resultDCT = [abs(resultDCTA1(:,2:size)) repelem(1,7)'; abs(resultDCTA2(:,2:size)) repelem(2,7)';
             abs(resultDCTA3(:,2:size)) repelem(3,7)'; abs(resultDCTA4(:,2:size)) repelem(4,5)';
             abs(resultDCTB1(:,2:size)) repelem(5,7)'; abs(resultDCTB2(:,2:size)) repelem(6,7)';
             abs(resultDCTB3(:,2:size)) repelem(7,7)'; abs(resultDCTB4(:,2:size)) repelem(8,5)';
             abs(resultDCTC1(:,2:size)) repelem(9,7)'; abs(resultDCTC2(:,2:size)) repelem(10,7)';
             abs(resultDCTC3(:,2:size)) repelem(11,7)'; abs(resultDCTD1(:,2:size)) repelem(12,7)';
             abs(resultDCTD2(:,2:size)) repelem(13,7)'; abs(resultDCTD3(:,2:size)) repelem(14,7)';
             abs(resultDCTD4(:,2:size)) repelem(15,5)']; 
         
elseif size > 241 && size <= 449
    
resultDCTA1 = get_dct(result_group_A1)';
resultDCTA2 = get_dct(result_group_A2)';
resultDCTA3 = get_dct(result_group_A3)';

resultDCTB1 = get_dct(result_group_B1)';
resultDCTB2 = get_dct(result_group_B2)';
resultDCTB3 = get_dct(result_group_B3)';

resultDCTC1 = get_dct(result_group_C1)';
resultDCTC2 = get_dct(result_group_C2)';
resultDCTC3 = get_dct(result_group_C3)';

resultDCTD1 = get_dct(result_group_D1)';
resultDCTD2 = get_dct(result_group_D2)';
resultDCTD3 = get_dct(result_group_D3)';

resultDCT = [abs(resultDCTA1(:,2:size)) repelem(1,7)'; abs(resultDCTA2(:,2:size)) repelem(2,7)';
             abs(resultDCTA3(:,2:size)) repelem(3,7)'; abs(resultDCTB1(:,2:size)) repelem(5,7)'; 
             abs(resultDCTB2(:,2:size)) repelem(6,7)'; abs(resultDCTB3(:,2:size)) repelem(7,7)'; 
             abs(resultDCTC1(:,2:size)) repelem(9,7)'; abs(resultDCTC2(:,2:size)) repelem(10,7)';
             abs(resultDCTC3(:,2:size)) repelem(11,7)'; abs(resultDCTD1(:,2:size)) repelem(12,7)';
             abs(resultDCTD2(:,2:size)) repelem(13,7)'; abs(resultDCTD3(:,2:size)) repelem(14,7)'];
  
end
         
accuracyC = [];
res = [];
class = [];
ci = [];

c = cvpartition(resultDCT(:,1:1), 'KFold', 6);
%c = cvpartition(resultDCT(:,1:1), 'LeaveOut');

for j = 1:1:c.NumTestSets

     aux = randperm(c.NumObservations, c.TrainSize(j))';
     x = sort(aux);
     train = [];
     test = [];

   for i=1:1:length(resultDCT(:,1:1))
       if find(i==x)
        train = [train; resultDCT(i:i,1:size)];
       else
        test = [test; resultDCT(i:i,1:size)];
       end
    end

  mdl = fitcknn(train(:,1:size-1), train(:,size:size), 'NumNeighbors', 5, 'Distance', 'euclidean');
    
  j
  res = predict(mdl, test(:,1:size-1))
     
  class = test(:,size:size)

  %ci = [ci; bootci(1000, {@mean, res}, 'alpha', 0.05, 'type','student')];
  
 accuracyC = [accuracyC; sum(class == res) / numel(class)];

end

figure(2314)
plot(ci(1:2:end),'.r', 'MarkerSize', 12);
hold on;
plot(ci(2:2:end),'.b', 'MarkerSize', 12);

 accuracyCAux = sum(accuracyC)./c.NumTestSets