function [resultData, resultData1, resultData2, resultData3] = get_groupA(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = get_region(data, 139, 173, 20);
%result1 = awgn(result1Aux, 75, 'measured') + 2;

[result2, result3] = get_create(data + 2);

result4Aux = get_region(data, 331, 356, 20) + 1;
result4 = get_region(result4Aux, 728, 764, 30);

result5 = get_region(data, 547, 564, 20) + 4;

result6Aux = get_region(data, 139, 173, 25) + 5;
result6AAux = get_region(result6Aux, 331, 356, 20);
result6 = get_region(result6AAux, 728, 764, 25);

result7 = result5 + 3.5;
result7(790) = 700;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:661);

result11 = resultAux1 - 5;

[result22, result33] = get_create(resultAux1 - 2);

result44Aux = get_region(resultAux1, 139, 173, 15) - 4;
result44 = get_region(result44Aux, 547, 564, 20);

result55 = get_region(resultAux1, 331, 356, 18) - 3;

result66Aux = get_region(resultAux1, 331, 356, 25) - 6;
result66 = get_region(result66Aux, 547, 564, 10);

result77 = result66 - 6.5;
result77(500) = 680;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result1Aux2 = data(1:449) + 8;

[result222, result333] = get_create(result1Aux2 + 6);

result444 = get_region(result1Aux2, 139, 173, 13) + 9;

result555 = get_region(result1Aux2, 331, 356, 18) + 7;

result667Aux = get_region(result1Aux2, 139, 173, 22) + 10; 
result667 = get_region(result667Aux, 331, 356, 7);

result777 = result333 + 8.6;
result777(200) = 720;

%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:241) - 6;

[result2222, result3333] = get_create(result1111 - 7);

result4444 = get_region(result1111, 138, 173, 30) - 8;

result5555 = result2222 - 9;
result5555(90) = 730.2;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result1Aux2 result222 result333+5 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end