%Dados com o k igual a 9 vizinhos

close all;
clear all;
clc;

monoSPR = [0.9865; 0.8631];
hadamard = [0.7748; 0.5878];
chebyshev = [0.8828; 0.7116];
DCTCos = [0.8576; 0.6878];

acc = [0.6768; 0.7879; 0.7677; 0.9091];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk9.fig');

figure(3422)
plot(acc, '.g', 'MarkerSize', 12);
ylabel('Accuracy');
title('Chebychev');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk9.fig');

