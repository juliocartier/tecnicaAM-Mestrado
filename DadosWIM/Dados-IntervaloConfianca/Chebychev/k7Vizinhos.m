%Dados com o k igual a 7 vizinhos

close all;
clear all;
clc;

monoSPR = [0.9891; 0.8607];
hadamard = [0.8708; 0.6866];
chebyshev = [0.9052; 0.7343];
DCTCos = [0.9274; 0.7729];

acc = [0.7677; 0.8081; 0.8384; 0.9091];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk7.fig');

figure(3422)
plot(acc, '.g', 'MarkerSize', 12);
ylabel('Accuracy');
title('Chebychev');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk7.fig');
