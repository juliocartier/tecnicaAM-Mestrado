%Dados com o k igual a 3 vizinhos

close all;
clear all;
clc;

monoSPR = [0.9960; 0.9590];
hadamard = [0.8619; 0.6909];
chebyshev = [0.9899; 0.8585];
DCTCos = [0.9814; 0.8638];

acc = [0.7677; 0.9091; 0.9191; 0.9798];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk3.fig');

figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk3.fig');

