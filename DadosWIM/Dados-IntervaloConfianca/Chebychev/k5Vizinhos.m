%Dados com o k igual a 5 vizinhos

close all;
clear all;
clc;

monoSPR = [1.0215; 0.9292];
hadamard = [0.9024; 0.7469];
chebyshev = [0.9986; 0.8717];
DCTCos = [1.0012; 0.8722];

acc = [0.8182; 0.9092; 0.9192; 0.9596];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk5.fig');

figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk5.fig');

