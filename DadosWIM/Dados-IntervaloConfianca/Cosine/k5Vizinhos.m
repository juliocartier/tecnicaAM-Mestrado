%Dados com o k igual a 5 vizinhos

close all;
clear all;
clc;

monoSPR = [1.0096; 0.8998];
hadamard = [0.6457; 0.4485];
chebyshev = [0.9136; 0.7611];
DCTCos = [0.8434; 0.6670];

acc = [0.5455; 0.8283; 0.7475; 0.9394];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk5.fig');

figure(3422)
plot(acc, '.b', 'MarkerSize', 20);
ylabel('Accuracy');
title('Cosine');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk5.fig');


