%Dados com o k igual a 3 vizinhos

close all;
clear all;
clc;

monoSPR = [1.0052; 0.9475];
hadamard = [0.6327; 0.4232];
chebyshev = [0.9253; 0.7702];
DCTCos = [0.9377; 0.7962];

acc = [0.5253; 0.8384; 0.8586; 0.9697];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

figure(6887)
errorbar(medias, errorr, 'b');
ylabel('Confidence Interval');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('ICk3.fig');

figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk3.fig');

