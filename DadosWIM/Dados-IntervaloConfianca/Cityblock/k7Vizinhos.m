%Dados com o k igual a 7 vizinhos

close all;
clear all;
clc;

monoSPR = [0.9710; 0.8350];
hadamard = [0.8375; 0.6673];
chebyshev = [0.8124; 0.6346];
DCTCos = [0.9374; 0.7801];

acc = [0.7475; 0.7172; 0.8485; 0.8889];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk7.fig');

figure(3422)
plot(acc, '.k', 'MarkerSize', 12);
ylabel('Accuracy');
title('Cityblock');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk7.fig');

