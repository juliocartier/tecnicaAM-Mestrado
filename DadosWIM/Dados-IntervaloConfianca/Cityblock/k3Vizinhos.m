%Dados com o k igual a 3 vizinhos

close all;
clear all;
clc;

monoSPR = [1.0228; 0.9287];
hadamard = [0.9042; 0.7463];
chebyshev = [0.9966; 0.8752];
DCTCos = [0.9724; 0.8486];

acc = [0.8182; 0.9192; 0.8990; 0.9596];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('ICk3.fig');

figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('Accuracyk3.fig');

