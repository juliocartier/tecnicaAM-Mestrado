%Dados com o k igual a 5 vizinhos
close all;
clear all;
clc;

monoSPR = [1.0125; 0.8994];
hadamard = [0.8394; 0.6560];
chebyshev = [0.9867; 0.8599];
DCTCos = [0.9831; 0.8638];

acc = [ 0.7374;0.9091; 0.9091; 0.9394];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

figure(6881)
errorbar(medias, errorr, 'b');
ylabel('Confidence Interval');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('k5Vizinhos.fig');

figure(3422)
plot(acc, '.b', 'MarkerSize', 12);
ylabel('Accuracy');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('AcurracyK5.fig');


