%Dados com o k igual a 7 vizinhos

close all;
clear all;
clc;

monoSPR = [0.9749; 0.8466];
hadamard = [0.8731; 0.6893];
chebyshev = [0.8680; 0.6909];
DCTCos = [0.8960; 0.7222];

acc = [0.7677; 0.7677; 0.7980; 0.8990];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('k7Vizinhos.fig');

figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
title('Euclidean');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('AcurracyK7.fig');

