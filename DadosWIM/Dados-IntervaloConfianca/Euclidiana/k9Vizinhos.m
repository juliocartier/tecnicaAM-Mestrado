%Dados com o k igual a 9 vizinhos

close all;
clear all;
clc;

monoSPR = [0.9972; 0.8753];
hadamard = [0.7841; 0.6030];
chebyshev = [0.8746; 0.6955];
DCTCos = [0.8820; 0.7107];

acc = [0.6869; 0.7778; 0.7879; 0.9192];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

% figure(6887)
% errorbar(medias, errorr, 'b');
% ylabel('Confidence Interval');
% set(gca,'xtick',[1:4],'xticklabel',names)
% savefig('k9Vizinhos.fig');

figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
title('Euclidean');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('AcurracyK9.fig');

