%Dados com o k igual a 3 vizinhos

close all;
clear all;
clc;

monoSPR = [1.0080; 0.9432];
hadamard = [0.8999; 0.7494];
chebyshev = [0.9854; 0.8621];
DCTCos = [1.0013; 0.8719];

acc = [0.8182; 0.9091; 0.9192; 0.9697];

mediaMonolayerSPR = mean(monoSPR);
errorMonolayerSPR = monoSPR(1) - mediaMonolayerSPR;

mediaHadard = mean(hadamard);
errorHadard = hadamard(1) - mediaHadard;

mediaChebyshev = mean(chebyshev);
errorChebyshev = chebyshev(1) - mediaChebyshev;

mediaDCTCos = mean(DCTCos);
errorDCTCos = DCTCos(1) - mediaDCTCos;

medias = [mediaHadard mediaChebyshev mediaDCTCos mediaMonolayerSPR];
errorr = [errorHadard errorChebyshev errorDCTCos errorMonolayerSPR];

names = {'FWHT'; 'FCHT'; 'DCT'; 'MonoSPR'};

figure(6887)
errorbar(medias, errorr, 'b');
ylabel('Confidence Interval');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('k3Vizinhos.fig');


figure(3422)
plot(acc, '.r', 'MarkerSize', 12);
ylabel('Accuracy');
set(gca,'xtick',[1:4],'xticklabel',names)
savefig('AcurracyK3.fig');
