close all;
clear all;
clc;

load receitaBK7Ag.txt;

resultFChT = fct2(receitaBK7Ag, 1);

inverseFChT = ifct2(resultFChT(1:9,1:1), 845);
inverseFChT100 = ifct2(resultFChT(1:100,1:1), 845);
inverseFChT200 = ifct2(resultFChT(1:200,1:1), 845);
inverseFChT300 = ifct2(resultFChT(1:300,1:1), 845);


figure(3424)
plot(inverseFChT(845:-1:1) + 329.0729, 'b');
hold on;
plot(inverseFChT100(845:-1:1) + 166.6745, 'r');
hold on;
plot(inverseFChT200(845:-1:1) + 72.5623, 'k');
hold on;
plot(inverseFChT300(845:-1:1), 'g');

names = {'9'; '100'; '200'; '300'};

err9 = mse(inverseFChT(845:-1:1), receitaBK7Ag(:,1:1));
err100 = mse(inverseFChT100(845:-1:1), receitaBK7Ag(:,1:1));
err200 = mse(inverseFChT200(845:-1:1), receitaBK7Ag(:,1:1));
err300 = mse(inverseFChT300(845:-1:1), receitaBK7Ag(:,1:1));

errors = [err9; err100; err200; err300];

figure(264115)
plot(errors,'.r', 'MarkerSize', 20);
set(gca,'xtick',[1:4],'xticklabel',names)
xlabel('Qtd. Coeficientes');
ylabel('$ERM (\hat{\Theta})$','interpreter','latex')