function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2

result1 = data - 25;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 200, 229, 20);
result4 = get_region(result4Aux, 494, 519, 15);

result5 = get_region(result1, 906, 983, 22) - 4;

result6Aux = get_region(result1, 494, 519, 16);
result6AAux = get_region(result6Aux, 906, 983, 15);
result6 = get_region(result6AAux, 1388, 1425, 28);

result7 = result5 - 6;
result7(786) = result7(786) + 6;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:1193) - 13;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 200, 229, 17);
result44 = get_region(result44Aux, 494, 519, 15);
 
result55 = get_region(result11, 200, 229, 18);

result66Aux = get_region(result11, 494, 519, 15);
result66 = get_region(result66Aux, 906, 983, 10);

result77 = result44 - 2.8;
result77(400) = result77(400) + 6;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:614) + 20;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 200, 229, 25);
 
%result555 = get_region(result111, 494, 533, 21);

result555 = result111;

result667Aux = get_region(result111, 200, 229, 10);
result667 = get_region(result667Aux, 494, 533, 25);

result777 = result555 + 4.6;
result777(165) = result777(155) + 10;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:354) + 30;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 200, 229, 25);

result5555 = result1111 + 11;
result5555(25) = result5555(25) + 6;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
