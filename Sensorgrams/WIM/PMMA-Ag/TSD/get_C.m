function [resultData, resultData1, resultData2] = get_C(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
result1 = data - 60;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 640, 767, 20);
result4 = get_region(result4Aux, 1073, 1425, 21);

result5 = get_region(result1, 1073, 1098, 17);

result6Aux = get_region(result1, 640, 767, 22);
result6AAux = get_region(result6Aux, 1073, 1098, 25);
result6 = get_region(result6AAux, 1384, 1425, 15);

result7 = result1 - 3;
result7(514) = result7(514) + 5;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:1191) + 45;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 200, 229, 25);
result44 = get_region(result44Aux, 640, 767, 24);
 
result55 = get_region(result11, 1073, 1098, 26);
 
result66Aux = get_region(result11, 200, 229, 16);
result66 = get_region(result66Aux, 1073, 1098, 28);

result77 = result22 + 2;
result77(288) = result77(288) + 6;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:934) + 10;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 200, 229, 22);
 
result555 = get_region(result111, 640, 767, 23);
 
result667Aux = get_region(result111, 200, 229, 20);
result667 = get_region(result667Aux, 640, 767, 18);

result777 = result667 + 5;
result777(291) = result777(291) + 5;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];