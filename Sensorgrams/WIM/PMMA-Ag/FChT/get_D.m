function [resultData, resultData2, resultData3, resultData4] = get_D(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = data + 29;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 818, 859, 22);
result4 = get_region(result4Aux, 1113, 1138, 25);

result5 = get_region(result1, 1113, 1138, 23);

result6Aux = get_region(result1, 818, 859, 15);
result6AAux = get_region(result6Aux, 1113, 1138, 15);
result6 = get_region(result6AAux, 1385, 1404, 27);

result7 = result3 + 5;
result7(790) = result7(790) + 3;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:1233) + 40;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 340, 467, 17);
result44 = get_region(result44Aux, 1113, 1138, 20);

result55 = get_region(result11, 818, 859, 26);

result66 = get_region(result11, 340, 467, 25);
%result66 = get_region(result66Aux, 1113, 1404, 13);

result77 = result66 + 4.8;
result77(200) = result77(200) + 4;
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:975) - 47;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 340, 467, 22);

result555 = get_region(result111, 818, 859, 25);

result667Aux = get_region(result111, 340, 467, 16);
result667 = get_region(result667Aux, 818, 859, 34);

result777 = result111 - 6;
result777(483) = result777(483) + 3.5;

result1111 = data(1:626) + 50;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 340, 467, 15);

result5555 = result3333 + 5;
result5555(175) = result5555(175) + 2.5; 

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22 result33 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];