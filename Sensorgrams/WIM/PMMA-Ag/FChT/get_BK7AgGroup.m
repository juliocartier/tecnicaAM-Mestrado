function [groupB, groupC, groupD] = get_BK7AgGroup(data)

groupB = [data(1:47); data(314:484); data(485:613); data(48:218); data(219:313); data(614:1032); data(1033:1192); data(1193:1424); data(1425:1532)]; 

groupC = [data(1:47); data(314:484); data(485:613); data(614:1032); data(1033:1192); data(48:218); data(219:313); data(1193:1424); data(1425:1532)];

groupD = [data(1:47); data(614:1032); data(1033:1192); data(1193:1424); data(1425:1532); data(48:218); data(219:313); data(314:484); data(485:613)];

groupC(926:935) = 660.9000;

groupD(966:975) = 661.1100;

end

