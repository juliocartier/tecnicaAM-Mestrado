function [resultNewData, resultData] = get_create(data)

resultNewData = [];
resultData = [];

for i=1:length(data)
    if (isempty(resultNewData))
        resultNewData = data + 5;
    else
        resultData = awgn(data + 6, 75, 'measured');
    end
end