function [resultData, resultData1, resultData2, resultData3] = get_A(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = data;
%result1 = awgn(result1Aux, 75, 'measured') + 2;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 246, 305, 40);
result4 = get_region(result4Aux, 804, 827, 30);

result5 = get_region(data, 1108, 1135, 40);

result6Aux = get_region(data, 246, 305, 45);
result6AAux = get_region(result6Aux, 804, 827, 40);
result6 = get_region(result6AAux, 1108, 1135, 25);

result7 = result5 + 3.5;
result7(790) = result7(790) + 2;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:938) + 8;

result11 = resultAux1;

[result22, result33] = get_create(resultAux1);

result44Aux = get_region(resultAux1, 246, 305, 25);
result44 = get_region(result44Aux, 508, 544, 20);

result55 = get_region(resultAux1, 804, 827, 28);

result66Aux = get_region(resultAux1, 246, 305, 25);
result66 = get_region(result66Aux, 508, 544, 30);

result77 = result66 - 2.5;
result77(584) = result77(584) + 7;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result1Aux2 = data(1:646) - 25;

[result222, result333] = get_create(result1Aux2);

result444 = get_region(result1Aux2, 246, 305, 33);

result555 = get_region(result1Aux2, 508, 544, 18);

result667Aux = get_region(result1Aux2, 246, 305, 22); 
result667 = get_region(result667Aux, 508, 544, 17);

result777 = result333 + 5;
result777(200) = result777(200) + 2;

%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:389) + 20;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 138, 173, 30);

result5555 = result2222 + 2;
result5555(90) = result5555(90) + 7;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result1Aux2 result222 result333+5 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end