function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2
result1 = data - 25;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 508, 544, 20);
result4 = get_region(result4Aux, 804, 827, 15);

result5 = get_region(result1, 230, 267, 22);

result6Aux = get_region(result1, 502, 561, 16);
result6AAux = get_region(result6Aux, 804, 836, 30);
result6 = get_region(result6AAux, 1108, 1157, 22);

result7 = result5 - 1;
result7(700) = result7(700) + 3;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:938) - 13;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 230, 267, 17);
result44 = get_region(result44Aux, 804, 836, 15);
 
result55 = get_region(result11, 502, 561, 18);

result66Aux = get_region(result11, 502, 561, 15);
result66 = get_region(result66Aux, 804, 836, 20);

result77 = result44 - 2.8;
result77(470) = result77(470) + 5;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:644) + 20;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 230, 267, 25);
 
result555 = get_region(result111, 502, 561, 21);

result667Aux = get_region(result111, 230, 267, 20);
result667 = get_region(result667Aux, 502, 561, 25);

result777 = result555 + 4.6;
result777(165) = result777(165) + 10;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:367) + 30;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 230, 267, 25);

result5555 = result1111 + 11;
result5555(200) = result5555(200) + 10;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
