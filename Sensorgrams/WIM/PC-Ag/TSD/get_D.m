function [resultData, resultData2, resultData3, resultData4] = get_D(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = data + 19;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 852, 879, 22);
result4 = get_region(result4Aux, 1073, 1110, 15);

% result4 = result2;

result5 = get_region(result1, 538, 597, 23);

result6Aux = get_region(result1, 538, 600, 25);
result6AAux = get_region(result6Aux, 852, 879, 15);
result6 = get_region(result6AAux, 1073, 1110, 27);

result7 = result3 + 5;
result7(790) = result7(790) + 5;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:962) + 40;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 268, 293, 17);
result44 = get_region(result44Aux, 538, 597, 20);

result55 = get_region(result11, 852, 879, 26);

result66Aux = get_region(result11, 268, 293, 25);
result66 = get_region(result66Aux, 852, 879, 23);

result77 = result66 + 4.8;
result77(200) = result77(200);
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:680) - 47;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 268, 293, 22);

result555 = get_region(result111, 538, 597, 25);

result667Aux = get_region(result111, 268, 293, 26);
result667 = get_region(result667Aux, 538, 597, 24);

result777 = result111 - 6;
result777(370) = 620;

result1111 = data(1:410) + 50;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 268, 293, 25);

result5555 = result3333 + 5;
result5555(175) = result5555(175) + 6; 

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22 result33 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];