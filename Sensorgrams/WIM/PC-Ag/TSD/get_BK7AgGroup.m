function [groupB, groupC, groupD] = get_BK7AgGroup(data)

groupB = [data(1:111) - 1.2700; data(390:544); data(545:645); data(112:304); data(305:389); data(646:826); data(827:937); data(938:1134); data(1135:1210)]; 

groupC = [data(1:111) - 1.2700; data(390:544); data(545:645); data(646:826); data(827:937); data(112:304); data(305:389); data(938:1134); data(1135:1210)];

groupD = [data(1:111) - 0.5000; data(646:826); data(827:937); data(112:304); data(305:389); data(938:1134); data(1135:1210); data(390:544); data(545:645)];

groupC(659:666) = 506.4900;

groupD(403:410) = 506.4900;

groupD(954:962) = 506.7100;

end

