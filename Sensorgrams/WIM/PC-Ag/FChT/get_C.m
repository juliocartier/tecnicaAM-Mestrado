function [resultData, resultData1, resultData2] = get_C(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
result1 = data - 60;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 794, 853, 30);
result4 = get_region(result4Aux, 1108, 1153, 21);

result5 = get_region(result1, 230, 279, 7) - 5;

result6Aux = get_region(result1, 230, 279, 12);
result6AAux = get_region(result6Aux, 794, 853, 15);
result6 = get_region(result6AAux, 1108, 1153, 25);

result7 = result1 - 3;
result7(743) = result7(743) + 12;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:936) + 45;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 230, 279, 25);
result44 = get_region(result44Aux, 526, 549, 19);
 
result55 = get_region(result11, 794, 853, 26);
 
result66Aux = get_region(result11, 230, 279, 26);
result66 = get_region(result66Aux, 794, 853, 28);

result77 = result22 + 2;
result77(340) = result77(340) + 8;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:666) + 10;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 230, 279, 22);
 
result555 = get_region(result111, 526, 549, 23);
 
result667Aux = get_region(result111, 230, 279, 20);
result667 = get_region(result667Aux, 526, 549, 18);

result777 = result667 + 5;
result777(180) = result777(180) + 5;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];