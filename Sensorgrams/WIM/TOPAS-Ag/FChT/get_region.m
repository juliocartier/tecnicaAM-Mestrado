function[result] = get_region(data, inicio, fim, janela)

receitaInicio = data(1:inicio-janela);
varDissociacao = data(inicio:fim);

valorRepetido = repelem(data(fim), (inicio - (inicio - janela)))';

result = cat(1, receitaInicio, varDissociacao, valorRepetido, data(fim + 2:end));