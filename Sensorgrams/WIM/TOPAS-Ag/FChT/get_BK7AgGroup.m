function [groupB, groupC, groupD] = get_BK7AgGroup(data)

groupB = [data(1:123); data(414:564); data(565:651); data(124:358); data(359:413); data(652:860); data(861:951); data(952:1128); data(1129:1250)]; 

groupC = [data(1:123); data(414:564); data(565:651); data(652:860); data(861:951); data(124:358); data(359:414); data(952:1128); data(1129:1250)];

groupD = [data(1:123); data(652:860); data(861:951); data(952:1128); data(1129:1250); data(124:358); data(359:413); data(414:564); data(565:651)];

end

