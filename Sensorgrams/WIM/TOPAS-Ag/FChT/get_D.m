function [resultData, resultData2, resultData3, resultData4] = get_D(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = data + 29;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 582, 601, 22);
result4 = get_region(result4Aux, 1121, 1164, 25);

result5 = get_region(result1, 881, 958, 23);

result6Aux = get_region(result1, 302, 337, 25);
result6AAux = get_region(result6Aux, 881, 958, 25);
result6 = get_region(result6AAux, 1121, 1164, 27);

result7 = result3 + 5;
result7(671) = result7(671) + 7.10;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:1013) + 40;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 302, 337, 37);
result44 = get_region(result44Aux, 881, 958, 30);

result55 = get_region(result11, 582, 601, 36);

result66Aux = get_region(result11, 582, 601, 25);
result66 = get_region(result66Aux, 881, 958, 33);

result77 = result66 + 4.8;
result77(200) = result77(200) + 7.6;
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:720) - 47;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 302, 337, 22);

result555 = get_region(result111, 582, 601, 25);

result667Aux = get_region(result111, 302, 337, 26);
result667 = get_region(result667Aux, 582, 601, 34);

result777 = result111 - 6;
result777(370) = result777(370) + 6.2;

result1111 = data(1:424) + 50;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 302, 337, 15);

result5555 = result3333 + 5;
result5555(239) = result5555(239) + 7.45; 

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22-0.4 result33+0.3 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];