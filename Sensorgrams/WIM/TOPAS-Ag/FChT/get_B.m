function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2
result1 = data - 25;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 232, 275, 20);
result4 = get_region(result4Aux, 1110, 1129, 25);

result5 = get_region(result1, 830, 849, 22);

result6Aux = get_region(result1, 232, 275, 16);
result6AAux = get_region(result6Aux, 830, 849, 35);
result6 = get_region(result6AAux, 1110, 1129, 22);

result7 = result5 - 6;
result7(753) = result7(753) + 10;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:952) - 13;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 232, 275, 17);
result44 = get_region(result44Aux, 830, 849, 35);
 
result55 = get_region(result11, 520, 601, 18);

result66Aux = get_region(result11, 232, 275, 25);
result66 = get_region(result66Aux, 520, 601, 30);

result77 = result44 - 2.8;
result77(474) = result77(474) + 10;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:651) + 20;
result111(447:449) = data(445) + 20;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 232, 275, 25);
 
result555 = get_region(result111, 520, 601, 21);

result667Aux = get_region(result111, 232, 275, 20);
result667 = get_region(result667Aux, 520, 601, 25);

result777 = result555 + 4.6;
result777(165) = result777(155) + 10;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:358) + 30;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 232, 275, 45);

result5555 = result1111 + 11;
result5555(200) = result5555(200) + 12;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
