function [resultData, resultData1, resultData2, resultData3] = get_A(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = data;
%result1 = awgn(result1Aux, 75, 'measured') + 2;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 522, 565, 30);
result4 = get_region(result4Aux, 830, 861, 30);

result5 = get_region(result1, 1110, 1129, 20);

result6Aux = get_region(result1, 522, 565, 25);
result6AAux = get_region(result6Aux, 830, 861, 20);
result6 = get_region(result6AAux, 1110, 1129, 25);

result7 = result5 + 3.5;
result7(790) = result7(790) + 5;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:952) + 8;

result11 = resultAux1;

[result22, result33] = get_create(resultAux1);

result44Aux = get_region(resultAux1, 282, 359, 15);
result44 = get_region(result44Aux, 522, 565, 20);

result55 = get_region(resultAux1, 830, 861, 28);

result66Aux = get_region(resultAux1, 282, 359, 25);
result66 = get_region(result66Aux, 830, 861, 30);

result77 = result66 - 2.5;
result77(500) = result77(500) + 5;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result1Aux2 = data(1:648) - 25;

[result222, result333] = get_create(result1Aux2);

result444 = get_region(result1Aux2, 282, 359, 23);

result555 = get_region(result1Aux2, 522, 565, 28);

result667 = get_region(result1Aux2, 282, 359, 22); 
%result667 = get_region(result667Aux, 522, 259, 15);

result777 = result333 + 5;
result777(200) = result777(200) + 4.5;
%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:414) + 20;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 282, 359, 50);

result5555 = result2222 + 2;
result5555(90) = result5555(90) + 8.2;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result1Aux2 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end