function [result] = get_region_time(data, inicio, fim, janela)

receitaInicio = data(1:inicio-janela);
varDissociacao = data(inicio:fim);

result = cat(1, receitaInicio, varDissociacao, data(fim + 2:end));