function [resultData, resultData1, resultData2] = get_C(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
result1 = data - 60;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 820, 897, 30);
result4 = get_region(result4Aux, 1111, 1130, 21);

result5 = get_region(result1, 540, 571, 27);

result6Aux = get_region(result1, 232, 275, 22);
result6AAux = get_region(result6Aux, 820, 897, 15);
result6 = get_region(result6AAux, 1111, 1130, 35);

result7 = result1 - 3;
result7(600) = result7(600) + 15;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:957) + 45;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 232, 275, 25);
result44 = get_region(result44Aux, 540, 571, 34);
 
result55 = get_region(result11, 820, 897, 26);
 
result66Aux = get_region(result11, 232, 275, 26);
result66 = get_region(result66Aux, 820, 897, 28);

result77 = result22 + 2;
result77(340) = result77(340) + 15;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:661) + 10;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 232, 275, 32);
 
result555 = get_region(result111, 540, 571, 23);
 
result667Aux = get_region(result111, 232, 275, 20);
result667 = get_region(result667Aux, 540, 571, 18);

result777 = result667 + 5;
result777(200) = result777(200) + 15;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];
end