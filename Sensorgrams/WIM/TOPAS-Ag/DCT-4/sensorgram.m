close all;
clear all;
clc;

load receitaTOPASAg.txt;


dataTOPASAg = [repmat(receitaTOPASAg(1), 60, 1); receitaTOPASAg(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AgGroup(dataTOPASAg);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataTOPASAg);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3, resultD4] = get_D(dataBK7Au_D);

% figure(123)
% subplot(2,2,1)
% plot(resultA1(:,1:1), 'r');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(a) Grupo 1');
% subplot(2,2,2)
% plot(resultA2(:,1:1) - 13, 'b');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(e) Grupo 5');
% subplot(2,2,3)
% plot(resultA3(:,1:1) + 25, 'k');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(i) Grupo 9');
% subplot(2,2,4)
% plot(resultA4(:,1:1) - 20, 'g')
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(m) Grupo 13');
% 
% 
% figure(1234)
% subplot(2,2,1)
% plot(resultB1(:,1:1) + 25, 'b');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(b) Grupo 2');
% subplot(2,2,2)
% plot(resultB2(:,1:1) + 13, 'r');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(f) Grupo 6');
% subplot(2,2,3)
% plot(resultB3(:,1:1) - 20, 'k');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(j) Grupo 10');
% subplot(2,2,4)
% plot(resultB4(:,1:1) - 30, 'g');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(n) Grupo 14');
% 
% figure(12234)
% subplot(2,2,1)
% plot(resultC1(:,1:1) + 60, 'b');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(c) Grupo 3');
% subplot(2,2,2)
% plot(resultC2(:,1:1) - 45, 'r');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(g) Grupo 7');
% subplot(2,2,3)
% plot(resultC3(:,1:1) - 10, 'k');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(k) Grupo 11');
% 
% figure(122334)
% subplot(2,2,1)
% plot(resultD1(:,1:1) - 29, 'b');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(d) Grupo 4');
% subplot(2,2,2)
% plot(resultD2(:,1:1) - 40, 'r');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(h) Grupo 8');
% subplot(2,2,3)
% plot(resultD3(:,1:1) + 47, 'k');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(l) Grupo 12');
% subplot(2,2,4)
% plot(resultD4(:,1:1) - 50, 'g');
% xlabel('Tempo (s)');
% ylabel('\lambda_r (nm)')
% title('(o) Grupo 15');


% figure(696)
% plot(resultD4);

resultDCTA1 = get_dct(resultA1)';
resultDCTA2 = get_dct(resultA2)';
resultDCTA3 = get_dct(resultA3)';
resultDCTA4 = get_dct(resultA4)';

resultDCTB1 = get_dct(resultB1)';
resultDCTB2 = get_dct(resultB2)';
resultDCTB3 = get_dct(resultB3)';
resultDCTB4 = get_dct(resultB4)';

resultDCTC1 = get_dct(resultC1)';
resultDCTC2 = get_dct(resultC2)';
resultDCTC3 = get_dct(resultC3)';

resultDCTD1 = get_dct(resultD1)';
resultDCTD2 = get_dct(resultD2)';
resultDCTD3 = get_dct(resultD3)';
resultDCTD4 = get_dct(resultD4)';

tamanho = 4;
if tamanho <= 549

resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultDCTA3(:,2:tamanho)) repelem(3,7)'; abs(resultDCTA4(:,2:tamanho)) repelem(4,5)';
             abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; abs(resultDCTB2(:,2:tamanho)) repelem(6,7)';
             abs(resultDCTB3(:,2:tamanho)) repelem(7,7)'; abs(resultDCTB4(:,2:tamanho)) repelem(8,5)';
             abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultDCTC3(:,2:tamanho)) repelem(11,7)'; abs(resultDCTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultDCTD2(:,2:tamanho)) repelem(13,7)'; abs(resultDCTD3(:,2:tamanho)) repelem(14,7)';
             abs(resultDCTD4(:,2:tamanho)) repelem(15,5)'];
         
elseif tamanho > 550 && tamanho <= 1230
    
resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultDCTA3(:,2:tamanho)) repelem(3,7)'; abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; 
             abs(resultDCTB2(:,2:tamanho)) repelem(6,7)'; abs(resultDCTB3(:,2:tamanho)) repelem(7,7)'; 
             abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultDCTC3(:,2:tamanho)) repelem(11,7)'; abs(resultDCTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultDCTD2(:,2:tamanho)) repelem(13,7)'; abs(resultDCTD3(:,2:tamanho)) repelem(14,7)'];
         
elseif tamanho > 1231 && tamanho <= 1741
    
resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; abs(resultDCTB2(:,2:tamanho)) repelem(6,7)';  
             abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultDCTD1(:,2:tamanho)) repelem(12,7)'; abs(resultDCTD2(:,2:tamanho)) repelem(13,7)'];
        
else
   
    resultDCT = [abs(resultDCTA1(:,2:tamanho)) repelem(1,7)'; abs(resultDCTB1(:,2:tamanho)) repelem(5,7)'; 
                 abs(resultDCTC1(:,2:tamanho)) repelem(9,7)'; abs(resultDCTD1(:,2:tamanho)) repelem(12,7)'];
         
end

figure(4532)
plot3(resultDCT(1:7,1:1), resultDCT(1:7,2:2), resultDCT(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultDCT(27:33,1:1), resultDCT(27:33,2:2), resultDCT(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
hold on;
plot3(resultDCT(53:59,1:1), resultDCT(53:59,2:2), resultDCT(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
hold on;
plot3(resultDCT(74:80,1:1), resultDCT(74:80,2:2), resultDCT(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
hold on;
plot3(resultDCT(8:14,1:1), resultDCT(8:14,2:2), resultDCT(8:14,3:3), 'ob', 'MarkerSize', 8);
hold on;
plot3(resultDCT(34:40,1:1), resultDCT(34:40,2:2), resultDCT(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
hold on;
plot3(resultDCT(60:66,1:1), resultDCT(60:66,2:2), resultDCT(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
hold on;
plot3(resultDCT(81:87,1:1), resultDCT(81:87,2:2), resultDCT(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
hold on;
plot3(resultDCT(15:21,1:1), resultDCT(15:21,2:2), resultDCT(15:21,3:3), '+g', 'MarkerSize', 8);
hold on;
plot3(resultDCT(41:47,1:1), resultDCT(41:47,2:2), resultDCT(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
hold on;
plot3(resultDCT(67:73,1:1), resultDCT(67:73,2:2), resultDCT(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
hold on;
plot3(resultDCT(88:94,1:1), resultDCT(88:94,2:2), resultDCT(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
hold on;
plot3(resultDCT(22:26,1:1), resultDCT(22:26,2:2), resultDCT(22:26,3:3), '*k', 'MarkerSize', 8);
hold on;
plot3(resultDCT(48:52,1:1), resultDCT(48:52,2:2), resultDCT(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
hold on;
plot3(resultDCT(95:99,1:1), resultDCT(95:99,2:2), resultDCT(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
xlabel('1� Coeficiente');
ylabel('2� Coeficiente');
zlabel('3� Coeficiente');
legend('Grupo 1', 'Grupo 2', 'Grupo 3', 'Grupo 4', 'Grupo 5', 'Grupo 6', 'Grupo 7', 'Grupo 8', ...
    'Grupo 9', 'Grupo 10', 'Grupo 11', 'Grupo 12', 'Grupo 13', 'Grupo 14', 'Grupo 15');

accuracyC = [];
res = [];
acc = [];
% class = [];

for i = 1:1:length(resultDCT(:,1:1))
    a = resultDCT(i:i,1:end);
    result = setdiff(resultDCT, a, 'stable', 'rows');
    
    mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 5, 'Distance' , 'cosine');
    res = predict(mdl, a(:,1:end-1));
    
    acc = [acc; res];
    
    class = a(:,end:end);
      
    accuracyC = [accuracyC; sum(class == res) / numel(class)];
    
end
 
accuracy = sum(accuracyC)./length(resultDCT(:,1:1))

B = 2000;

ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student'); 

mediaDCT4 = mean(ci);
errorDCT4 = ci(1) - mediaDCT4;
         
figure(6887)
errorbar(mediaDCT4, errorDCT4, 'b');
ylabel('Intervalo de Confian�a');


