close all;
clear all;
clc;

load dataBKAu.txt;

dataBK7Au_A = [repmat(dataBKAu(1), 120, 1); dataBKAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(dataBK7Au_A);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataBK7Au_A);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3, resultD4] = get_D(dataBK7Au_D);

figure(213454)
plot(resultA1(:,1:1), 'r');
hold on;
plot(resultA2(:,1:1), 'y');
hold on;
plot(resultA3(:,1:1), 'Color', [0.6350 0.0780 0.1840]);
hold on;
plot(resultA3(:,1:1), 'k');
hold on;
plot(resultB1(:,1:1), 'g');
hold on;
plot(resultB2(:,1:1), 'm');
hold on;
plot(resultB3(:,1:1), 'Color', [0.4660 0.6740 0.1880]);
hold on;
plot(resultB4(:,1:1), 'g');
hold on;
plot(resultC1(:,1:1), 'k');
hold on;
plot(resultC2(:,1:1), 'c');
hold on;
plot(resultC3(:,1:1), 'Color', [0.3010 0.7450 0.9330]);
hold on;
plot(resultD1(:,1:1), 'b');
hold on;
plot(resultD2(:,1:1), 'Color', [0.4940 0.1840 0.5560]);
hold on;
plot(resultD3(:,1:1), 'Color', [0 0.4470 0.7410]);
hold on;
plot(resultD4(:,1:1), 'Color', [0.9290 0.6940 0.1250]);

% figure(4567897)
% plot(resultD4)

DTSA1 = [];
DTSB1 = [];
DTSC1 = [];
DTSD1 = [];

DTSA2 = [];
DTSB2 = [];
DTSC2 = [];
DTSD2 = [];

DTSA3 = [];
DTSB3 = [];
DTSC3 = [];
DTSD3 = [];

DTSA4 = [];
DTSB4 = [];
DTSD4 = [];

 for i = 1:1:7
   
         DTSA1 = [DTSA1; get_MonolayerSPR(resultA1(:,i:i), 0.005)];
         DTSA2 = [DTSA2; get_MonolayerSPR(resultA2(:,i:i), 0.005)];
         DTSA3 = [DTSA3; get_MonolayerSPR(resultA3(:,i:i), 0.005)];
         
         DTSB1 = [DTSB1; get_MonolayerSPR(resultB1(:,i:i), 0.005)];
         DTSB2 = [DTSB2; get_MonolayerSPR(resultB2(:,i:i), 0.005)];
         DTSB3 = [DTSB3; get_MonolayerSPR(resultB3(:,i:i), 0.005)];
         
         DTSC1 = [DTSC1; get_MonolayerSPR(resultC1(:,i:i), 0.005)];
         DTSC2 = [DTSC2; get_MonolayerSPR(resultC2(:,i:i), 0.005)];
         DTSC3 = [DTSC3; get_MonolayerSPR(resultC3(:,i:i), 0.005)];
         
         DTSD1 = [DTSD1; get_MonolayerSPR(resultD1(:,i:i), 0.005)];
         DTSD2 = [DTSD2; get_MonolayerSPR(resultD2(:,i:i), 0.005)];
         DTSD3 = [DTSD3; get_MonolayerSPR(resultD3(:,i:i), 0.005)];
   
 end
 
 for j = 1:1:5
       DTSA4 = [DTSA4; get_MonolayerSPR(resultA4(:,j:j), 0.005)];
       DTSB4 = [DTSB4; get_MonolayerSPR(resultB4(:,j:j), 0.005)];
       DTSD4 = [DTSD4; get_MonolayerSPR(resultD4(:,j:j), 0.005)];
 end
 
 figure(3222)
 plot(real(DTSA4(:,1:1)), imag(DTSA4(:,1:1)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(DTSB4(:,1:1)), imag(DTSB4(:,1:1)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(DTSD4(:,1:1)), imag(DTSD4(:,1:1)), '.g', 'MarkerSize', 20);
 
 figure(3442)
 plot(real(DTSA4(:,2:2)), imag(DTSA4(:,2:2)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(DTSB4(:,2:2)), imag(DTSB4(:,2:2)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(DTSD4(:,2:2)), imag(DTSD4(:,2:2)), '.g', 'MarkerSize', 20);
 
 figure(3662)
 plot(real(DTSA4(:,3:3)), imag(DTSA4(:,3:3)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(DTSB4(:,3:3)), imag(DTSB4(:,3:3)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(DTSD4(:,3:3)), imag(DTSD4(:,3:3)), '.g', 'MarkerSize', 20);
 
resultTotDescrt = [real(DTSA1(:,1:3)) repelem(1, 7)'; real(DTSA2(:,1:3)) repelem(2, 7)';
             real(DTSA3(:,1:3)) repelem(3, 7)'; real(DTSA4(:,1:3)) repelem(4, 5)';
             real(DTSB1(:,1:3)) repelem(5, 7)'; real(DTSB2(:,1:3)) repelem(6, 7)';
             real(DTSB3(:,1:3)) repelem(7, 7)'; real(DTSB4(:,1:3)) repelem(8, 5)';
             real(DTSC1(:,1:3)) repelem(9, 7)'; real(DTSC2(:,1:3)) repelem(10, 7)';
             real(DTSC3(:,1:3)) repelem(11, 7)';real(DTSD1(:,1:3)) repelem(12, 7)';
             real(DTSD2(:,1:3)) repelem(13, 7)'; real(DTSD3(:,1:3)) repelem(14, 7)';
             real(DTSD4(:,1:3)) repelem(15, 5)'];
         
% result = [];
% accuracyC = [];
% acc = [];
% 
% c = cvpartition(resultTotDescrt(:,1:1),'LeaveOut');
% 
%     for i = 1:1:c.NumTestSets
%         a = resultTotDescrt(i:i,1:end);
%         result = setdiff(resultTotDescrt, a, 'stable', 'rows');
% 
%         mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 3, 'Distance' , 'cosine');
%         res = predict(mdl, a(:,1:end-1));
%         
%         acc = [acc; res];
% 
%         class = a(:,end:end);
% 
%         accuracyC = [accuracyC; sum(class == res) / numel(class)];
%         %accuracyC = sum(class == res) / numel(class);
% 
%     end
% 
%     accuracyCAux = sum(accuracyC)./length(resultTotDescrt(:,1:1))
%     
% B = 2000;
% 
% ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');
% 
% mediaMonolayerSPR = mean(ci);
% errorMonolayerSPR = ci(1) - mediaMonolayerSPR;
%          
% figure(6887)
% errorbar(mediaMonolayerSPR, errorMonolayerSPR, 'b');
% ylabel('Confidence Interval');
%    