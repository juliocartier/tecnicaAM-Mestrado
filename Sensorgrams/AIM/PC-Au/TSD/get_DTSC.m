function [ result ] = get_DTSC( data )
%GET_DTSB Summary of this function goes here
%   Detailed explanation goes here

result = [complex(mean(data(1:187,1:1)), length(data(1:187,1:1)));
                   complex(mean(data(188:214,1:1)), length(data(188:214,1:1)));
                   complex(mean(data(215:351,1:1)), length(data(215:351,1:1)));
                   complex(mean(data(352:406,1:1)), length(data(352:406,1:1)));
                   complex(mean(data(407:514,1:1)), length(data(407:514,1:1)));
                   complex(mean(data(515:538,1:1)), length(data(515:538,1:1)));
                   complex(mean(data(539:692,1:1)), length(data(539:692,1:1)));
                   complex(mean(data(693:795,1:1)), length(data(693:795,1:1)));
                   complex(mean(data(796:878,1:1)), length(data(796:878,1:1)));
                   complex(mean(data(879:1051,1:1)), length(data(879:1051,1:1)));
                   complex(mean(data(1052:1263,1:1)), length(data(1052:1263,1:1)));
                   complex(mean(data(1264:1393,1:1)), length(data(1264:1393,1:1)));
                   complex(mean(data(1394:1556,1:1)), length(data(1394:1556,1:1)));
                   complex(mean(data(1557:1579,1:1)), length(data(1557:1579,1:1)));
                   complex(mean(data(1580:1850,1:1)), length(data(1580:1850,1:1)));
                   complex(mean(data(1851:1917,1:1)), length(data(1851:1917,1:1)));
                   complex(mean(data(1918:2069,1:1)), length(data(1918:2069,1:1)))];


end

