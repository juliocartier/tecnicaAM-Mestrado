function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2

result1 = data - 1;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 233, 270, 20);
result4 = get_region(result4Aux, 1059, 1139, 20);

result5 = get_region(result1, 812, 883, 15);

result6 = get_region(result1, 580, 611, 20);
% result6AAux = get_region(result6Aux, 812, 883, 25);
% result6 = get_region(result6AAux, 1059, 1139, 50);

result7 = result5 - 0.2;
result7(500) = result7(500) + 0.104;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:912) + 1.5;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 580, 611, 20);
result44 = get_region(result44Aux, 812, 883, 20);
 
result55 = get_region(result11, 223, 270, 20);

result66Aux = get_region(result11, 223, 270, 20);
result66 = get_region(result66Aux, 812, 883, 20);


result77 = result44;
result77(514) = result77(514) + 0.09;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:717) - 1.5;

[result222, result333] = get_create(result111);

%result444 = get_region(result111, 233, 270, 10);
result444 = result111;
 
result555 = get_region(result111, 580, 611, 20);
result555 = result111;

% result667Aux = get_region(result111, 233, 270, 20);
% result667 = get_region(result667Aux, 580, 611, 20);
result667 = result222;

result777 = result555 + 0.1;
result777(155) = result777(155) + 0.13;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:405) + 4;

[result2222, result3333] = get_create(result1111);

%result4444 = get_region(result1111, 233, 270, );
result4444 = result1111;

result5555 = result1111 + 0.1;
result5555(81) = result5555(81) + 0.12;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
