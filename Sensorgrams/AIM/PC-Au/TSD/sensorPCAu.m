close all;
clear all;
clc;

load dataPC.txt;

% dataPCAuA = [repmat(sensorgramPCAu(1), 60, 1); sensorgramPCAu(:, 1)];
% 
% dataPCAuA(1:60,1:1) = awgn(dataPCAuA(1:60,1:1), 60);
% 
% csvwrite('dataPC.txt', dataPCAuA);

% figure(952)
% plot(dataPC);

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_PCAuGroup(dataPC);

% figure(654)
% plot(dataPC);

% figure(653)
% plot(dataPC);

[resultA1, resultA2, resultA3, resultA4] = get_A(dataPC);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3, resultD4] = get_D(dataBK7Au_D);


% figure(123)
% subplot(2,2,1)
% plot(resultA1(:,1:1), 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultA2(:,1:1) + 0.6, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultA3(:,1:1) + 1, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultA4(:,1:1) - 1, 'g')
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% 
% figure(1234)
% subplot(2,2,1)
% plot(resultB1(:,1:1) + 1.0600, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultB2(:,1:1) - 1.4400, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultB3(:,1:1) + 1.5600, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultB4(:,1:1) - 3.9400, 'g');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% figure(12234)
% subplot(2,2,1)
% plot(resultC1(:,1:1) + 2.0600, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultC2(:,1:1) - 1.9400, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultC3(:,1:1) + 2.5600, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% figure(122334)
% subplot(2,2,1)
% plot(resultD1(:,1:1) - 3.2000, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultD2(:,1:1) + 3, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultD3(:,1:1) - 3.5000, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultD4(:,1:1) + 4.5670, 'g');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');

DTSA1 = [];
DTSB1 = [];
DTSC1 = [];
DTSD1 = [];

DTSA2 = [];
DTSB2 = [];
DTSC2 = [];
DTSD2 = [];

DTSA3 = [];
DTSB3 = [];
DTSC3 = [];
DTSD3 = [];

DTSA4 = [];
DTSB4 = [];
DTSD4 = [];

 for i = 1:1:7
   
         DTSA1 = [DTSA1; get_MonolayerSPR(resultA1(:,i:i), 0.003)];
         DTSA2 = [DTSA2; get_MonolayerSPR(resultA2(:,i:i), 0.003)];
         DTSA3 = [DTSA3; get_MonolayerSPR(resultA3(:,i:i), 0.003)];
%          
         DTSB1 = [DTSB1; get_MonolayerSPR(resultB1(:,i:i), 0.003)];
         DTSB2 = [DTSB2; get_MonolayerSPR(resultB2(:,i:i), 0.003)];
         DTSB3 = [DTSB3; get_MonolayerSPR(resultB3(:,i:i), 0.00295)];
%          
         DTSC1 = [DTSC1; get_MonolayerSPR(resultC1(:,i:i), 0.003)];
         DTSC2 = [DTSC2; get_MonolayerSPR(resultC2(:,i:i), 0.003)];
         DTSC3 = [DTSC3; get_MonolayerSPR(resultC3(:,i:i), 0.003)];
%          
         DTSD1 = [DTSD1; get_MonolayerSPR(resultD1(:,i:i), 0.003)];
         DTSD2 = [DTSD2; get_MonolayerSPR(resultD2(:,i:i), 0.003)];
         DTSD3 = [DTSD3; get_MonolayerSPR(resultD3(:,i:i), 0.003)];
   
 end
 
 for j = 1:1:5
       DTSA4 = [DTSA4; get_MonolayerSPR(resultA4(:,j:j), 0.003)];
       DTSB4 = [DTSB4; get_MonolayerSPR(resultB4(:,j:j), 0.002)];
       DTSD4 = [DTSD4; get_MonolayerSPR(resultD4(:,j:j), 0.002)];
 end
 
str1 = '#000000';
color1 = sscanf(str1(2:end),'%2x%2x%2x',[1 3])/255;

str2 = '#77AC30';
color2 = sscanf(str2(2:end),'%2x%2x%2x',[1 3])/255;

str3 = '#BEBE00';
color3 = sscanf(str3(2:end),'%2x%2x%2x',[1 3])/255;
 
 figure(3222)
 plot(real(DTSA4(:,1:1)), length(resultA4(1:108,:)), '*', 'MarkerSize', 10, 'Color', color1);
 hold on;
 plot(real(DTSB4(:,1:1)), length(resultB4(1:108,:)), '^', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
 hold on;
 plot(real(DTSD4(:,1:1)), length(resultA4(1:108,:)), 'x', 'MarkerSize', 10, 'Color', color3);
 
figure(3442)
    plot(real(DTSA4(1:3,2:2)), length(resultA4(121:202,1:3)), '*r', 'MarkerSize', 10, 'Color', color1);
    hold on;
    plot(real(DTSA4(4:4,2:2)), length(resultA4(121:183,4:4)), '*r', 'MarkerSize', 10, 'Color', color1);
    hold on;
    plot(real(DTSA4(5:5,2:2)), length(resultA4(121:202,5:5)), '*r', 'MarkerSize', 10, 'Color', color1);
    hold on;
    plot(real(DTSB4(1:3,2:2)), length(resultB4(135:234,1:3)), '^b', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
    hold on;
    plot(real(DTSB4(4:4,2:2)), length(resultB4(135:214,4:4)), '^b', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
    hold on;
    plot(real(DTSB4(5:5,2:2)), length(resultB4(135:234,5:5)), '^b', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
    hold on;
    plot(real(DTSD4(1:3,2:2)), length(resultD4(172:285,1:3)), 'xg', 'MarkerSize', 10, 'Color', color3);
    hold on;
    plot(real(DTSD4(4:4,2:2)), length(resultD4(172:265,4:4)), 'xg', 'MarkerSize', 10, 'Color', color3);
    hold on;
    plot(real(DTSD4(5:5,2:2)), length(resultD4(172:285,5:5)), 'xg', 'MarkerSize', 10, 'Color', color3);
 
 figure(3662)
    plot(real(DTSA4(1:3,2:2)), length(resultA4(234:276,1:3)), '*r', 'MarkerSize', 10, 'Color', color1);
    hold on;
    plot(real(DTSA4(4:4,2:2)), length(resultA4(224:276,4:4)), '*r', 'MarkerSize', 10, 'Color', color1);
    hold on;
    plot(real(DTSA4(5:5,2:2)), length(resultA4(234:276,5:5)), '*r', 'MarkerSize', 10, 'Color', color1);
    hold on;
    plot(real(DTSB4(1:3,2:2)), length(resultB4(270:405,1:3)), '^b', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
    hold on;
    plot(real(DTSB4(4:4,2:2)), length(resultB4(250:405,4:4)), '^b', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
    hold on;
    plot(real(DTSB4(5:5,2:2)), length(resultB4(270:405,5:5)), '^b', 'MarkerSize', 10, 'Color', color2, 'MarkerFaceColor', color2);
    hold on;
    plot(real(DTSD4(1:3,2:2)), length(resultD4(312:341,1:3)), 'xg', 'MarkerSize', 10, 'Color', color3);
    hold on;
    plot(real(DTSD4(4:4,2:2)), length(resultD4(282:341,4:4)), 'xg', 'MarkerSize', 10, 'Color', color3);
    hold on;
    plot(real(DTSD4(5:5,2:2)), length(resultD4(312:341,5:5)), 'xg', 'MarkerSize', 10, 'Color', color3);
 
 resultTotDescrt = [real(DTSA1(:,1:3)) repelem(1, 7)'; real(DTSA2(:,1:3)) repelem(2, 7)';
              real(DTSA3(:,1:3)) repelem(3, 7)'; real(DTSA4(:,1:3)) repelem(4, 5)';
             real(DTSB1(:,1:3)) repelem(5, 7)'; real(DTSB2(:,1:3)) repelem(6, 7)';
             real(DTSB3(:,1:3)) repelem(7, 7)'; real(DTSB4(:,1:3)) repelem(8, 5)';
             real(DTSC1(:,1:3)) repelem(9, 7)'; real(DTSC2(:,1:3)) repelem(10, 7)';
             real(DTSC3(:,1:3)) repelem(11, 7)';real(DTSD1(:,1:3)) repelem(12, 7)';
             real(DTSD2(:,1:3)) repelem(13, 7)'; real(DTSD3(:,1:3)) repelem(14, 7)';
             real(DTSD4(:,1:3)) repelem(15, 5)'];
         
% result = [];
% accuracyC = [];
% acc = [];
% 
% c = cvpartition(resultTotDescrt(:,1:1),'LeaveOut');
% 
%     for i = 1:1:c.NumTestSets
%         a = resultTotDescrt(i:i,1:end);
%         result = setdiff(resultTotDescrt, a, 'stable', 'rows');
% 
%         mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 5, 'Distance' , 'cosine');
%         res = predict(mdl, a(:,1:end-1));
%         
%         acc = [acc; res];
% 
%         class = a(:,end:end);
% 
%         accuracyC = [accuracyC; sum(class == res) / numel(class)];
%         %accuracyC = sum(class == res) / numel(class);
% 
%     end
% 
%     accuracyCAux = sum(accuracyC)./length(resultTotDescrt(:,1:1))
%     
% B = 2000;
% 
% ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');
% 
% mediaMonolayerSPR = mean(ci);
% errorMonolayerSPR = ci(1) - mediaMonolayerSPR;
%          
% figure(6887)
% errorbar(mediaMonolayerSPR, errorMonolayerSPR, 'b');
% ylabel('Confidence Interval');