function [resultData, resultData2, resultData3, resultData4] = get_D(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = data + 3.2;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 762, 842, 20);
result4 = get_region(result4Aux, 1033, 1075, 20);

result5 = get_region(result1, 484, 512, 20);

result6 = get_region(result1, 484, 512, 20);
% result6AAux = get_region(result6Aux, 762, 842, 25);
% result6 = get_region(result6AAux, 1033, 1075, 35);

result7 = result3;
result7(860) = result7(860) + 0.2;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:906) - 3;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 201, 258, 20);
result44 = get_region(result44Aux, 484, 512, 20);

result55 = get_region(result11, 762, 842, 20);

result66Aux = get_region(result11, 201, 258, 20);
result66 = get_region(result66Aux, 762, 842, 20);

result77 = result66 + 0.3;
result77(257) = result77(257) + 0.12;
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:618) + 3.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 201, 258, 20);

result555 = get_region(result111, 484, 512, 20);

result667Aux = get_region(result111, 201, 258, 20);
result667 = get_region(result667Aux, 484, 512, 20);

result777 = result111 - 0.1;
result777(275) = result777(275) + 0.14;

% Sensorgramas com 3 Regimes

sensorgram3R = [data(1:109) - 0.0670; data(310:541)];

result1111 = sensorgram3R - 4.5;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 201, 258, 20);

result5555 = result2222 + 0.05;
result5555(229) = result5555(229) + 0.13;

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22-0.4 result33+0.3 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];