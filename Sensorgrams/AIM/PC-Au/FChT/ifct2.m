function f=ifct2(a, l)
%x=-cos(pi/N*((0:N-1)'+1/2)) 
k=size(a); N=k(1);

f=idct(sqrt(N/2)*[a(1,:)*sqrt(2); a(2:end,:)], l);

end

