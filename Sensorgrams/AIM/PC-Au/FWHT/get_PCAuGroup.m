function [groupB, groupC, groupD] = get_PCAuGroup(data)

groupB = [data(1:107) - 0.0600; data(306:498); data(499:602); data(603:808); data(809:916);data(108:275); data(276:305); data(917:1138); data(1139:1204)]; 

groupC = [data(1:107) - 0.0600; data(306:498); data(499:602); data(108:275); data(276:305); data(603:808); data(809:916); data(917:1138); data(1139:1204)];

groupD = [data(1:107); data(108:275); data(276:305); data(603:808); data(809:916); data(917:1138); data(1139:1204); data(306:498) - 0.0350; data(499:602) - 0.0350];

