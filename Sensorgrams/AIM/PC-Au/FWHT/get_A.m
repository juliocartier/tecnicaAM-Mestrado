function [resultData, resultData1, resultData2, resultData3] = get_A(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = data;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 432, 468, 20);
result4 = get_region(result4Aux, 1058, 1139, 30);

result5 = get_region(data, 778, 813, 30);

result6Aux = get_region(data, 432, 468, 30);
result6AAux = get_region(result6Aux, 778, 813, 20);
result6 = get_region(result6AAux, 1058, 1139, 20);

result7 = result5 + 0.1;
%result7 = data + 3.5;
result7(1160) = result7(1160) + 0.1;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:915) - 0.5;

result11 = resultAux1;

[result22, result33] = get_create(resultAux1);

result44Aux = get_region(resultAux1, 432, 468, 30);
result44 = get_region(result44Aux, 778, 813, 18);

result55 = get_region(resultAux1, 201, 258, 25);

result66Aux = get_region(resultAux1, 201, 258, 25);
result66 = get_region(result66Aux, 778, 813, 26);

result77 = result66 + 0.2;
result77(542) = result77(542) + 0.15;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result111 = data(1:603) - 1;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 201, 258, 18);

result555 = get_region(result111, 432, 468, 25);

result667Aux = get_region(result111, 201, 258, 15); 
result667 = get_region(result667Aux, 432, 468, 20);

result777 = result333;
result777(525) = result777(525) + 0.14;

%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:276) + 1;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 201, 258, 30);

result5555 = result2222 + 0.2;
result5555(60) = result5555(60) + 0.18;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22+0.1 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end