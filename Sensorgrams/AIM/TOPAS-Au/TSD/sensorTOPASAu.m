close all;
clear all;
clc;

load sensorgramTOPASAu.txt;

%dataTOPASAu = [repmat(sensorgramTOPASAu(1), 120, 1); sensorgramTOPASAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(sensorgramTOPASAu);

% figure(952)
% plot(dataBK7Au_B);


[resultA1, resultA2, resultA3, resultA4] = get_A(sensorgramTOPASAu);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3, resultD4] = get_D(dataBK7Au_D);

% figure(123)
% subplot(2,2,1)
% plot(resultA1(:,1:1), 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultA2(:,1:1) + 0.6, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultA3(:,1:1) + 1, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultA4(:,1:1) - 1, 'g')
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% 
% figure(1234)
% subplot(2,2,1)
% plot(resultB1(:,1:1) + 1, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultB2(:,1:1) - 1.5000, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultB3(:,1:1) + 1.500, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultB4(:,1:1) - 4, 'g');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% figure(12234)
% subplot(2,2,1)
% plot(resultC1(:,1:1) + 2, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultC2(:,1:1) - 2, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultC3(:,1:1) + 2.5000, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% figure(122334)
% subplot(2,2,1)
% plot(resultD1(:,1:1) - 3.2000, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultD2(:,1:1) + 3, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultD3(:,1:1) - 3.5000, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultD4(:,1:1) + 4.5000, 'g');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');

% 
% DTS9 = get_DTS(resultA1(:,1:1));
% iDTS100 = get_MonolayerSPR(resultA1(:,1:1), 0.00001);
% iDTS200 = get_MonolayerSPR(resultA1(:,1:1), 0.009);
% iDTS300 = get_MonolayerSPR(resultA1(:,1:1), 0.07);
% 
% 
% iDTS9 = get_descrit_inver(DTS9);
% 
% idx = iDTS9 > 0;
% result_inv_A = idx.*iDTS9;
% 
% idx2 = iDTS100 > 0;
% result_inv100 = idx2.*iDTS100;
% 
% idx3 = iDTS200 > 0;
% result_inv200 = idx3.*iDTS200;
% 
% idx4 = iDTS300 > 0;
% result_inv300 = idx4.*iDTS300;
% 
% errors = [0.025; 0.0009; 0.0001; 0.00005];
% 
% names = {'9'; '100'; '200'; '300'};
% 
% figure(264115)
% plot(errors,'.r');
% set(gca,'xtick',[1:4],'xticklabel',names)
% xlabel('Qtd. Coeficientes');
% ylabel('$ERM (\hat{\Theta})$','interpreter','latex')
% 
% figure(222)
% %plot(resultA1(:,1:1));
% plot(find(idx~=0), result_inv_A(idx~=0))
% hold on;
% plot(find(idx2~=0), result_inv100(idx2~=0));
% hold on;
% plot(find(idx3~=0), result_inv200(idx3~=0));
% hold on;
% plot(find(idx4~=0), result_inv300(idx4~=0));
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');

DTSA1 = [];
DTSB1 = [];
DTSC1 = [];
DTSD1 = [];

DTSA2 = [];
DTSB2 = [];
DTSC2 = [];
DTSD2 = [];

DTSA3 = [];
DTSB3 = [];
DTSC3 = [];
DTSD3 = [];

DTSA4 = [];
DTSB4 = [];
DTSD4 = [];

 for i = 1:1:7
   
         DTSA1 = [DTSA1; get_MonolayerSPR(resultA1(:,i:i), 0.02)];
         DTSA2 = [DTSA2; get_MonolayerSPR(resultA2(:,i:i), 0.02)];
         DTSA3 = [DTSA3; get_MonolayerSPR(resultA3(:,i:i), 0.02)];
         
         DTSB1 = [DTSB1; get_MonolayerSPR(resultB1(:,i:i), 0.02)];
         DTSB2 = [DTSB2; get_MonolayerSPR(resultB2(:,i:i), 0.02)];
         DTSB3 = [DTSB3; get_MonolayerSPR(resultB3(:,i:i), 0.02)];
         
         DTSC1 = [DTSC1; get_MonolayerSPR(resultC1(:,i:i), 0.02)];
         DTSC2 = [DTSC2; get_MonolayerSPR(resultC2(:,i:i), 0.02)];
         DTSC3 = [DTSC3; get_MonolayerSPR(resultC3(:,i:i), 0.02)];
         
         DTSD1 = [DTSD1; get_MonolayerSPR(resultD1(:,i:i), 0.02)];
         DTSD2 = [DTSD2; get_MonolayerSPR(resultD2(:,i:i), 0.02)];
         DTSD3 = [DTSD3; get_MonolayerSPR(resultD3(:,i:i), 0.02)];
   
 end
 
 for j = 1:1:5
       DTSA4 = [DTSA4; get_MonolayerSPR(resultA4(:,j:j), 0.02)];
       DTSB4 = [DTSB4; get_MonolayerSPR(resultB4(:,j:j), 0.02)];
       DTSD4 = [DTSD4; get_MonolayerSPR(resultD4(:,j:j), 0.02)];
 end
 
 figure(3222)
 plot(real(DTSA4(:,1:1)), imag(DTSA4(:,1:1)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(DTSB4(:,1:1)), imag(DTSB4(:,1:1)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(DTSD4(:,1:1)), imag(DTSD4(:,1:1)), '.g', 'MarkerSize', 20);
 
 figure(3442)
 plot(real(DTSA4(:,2:2)), imag(DTSA4(:,2:2)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(DTSB4(:,2:2)), imag(DTSB4(:,2:2)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(DTSD4(:,2:2)), imag(DTSD4(:,2:2)), '.g', 'MarkerSize', 20);
 
 figure(3662)
 plot(real(DTSA4(:,3:3)), imag(DTSA4(:,3:3)), '.r', 'MarkerSize', 20);
 hold on;
 plot(real(DTSB4(:,3:3)), imag(DTSB4(:,3:3)), '.b', 'MarkerSize', 20);
 hold on;
 plot(real(DTSD4(:,3:3)), imag(DTSD4(:,3:3)), '.g', 'MarkerSize', 20);
 
 resultTotDescrt = [real(DTSA1(:,1:3)) repelem(1, 7)'; real(DTSA2(:,1:3)) repelem(2, 7)';
              real(DTSA3(:,1:3)) repelem(3, 7)'; real(DTSA4(:,1:3)) repelem(4, 5)';
             real(DTSB1(:,1:3)) repelem(5, 7)'; real(DTSB2(:,1:3)) repelem(6, 7)';
             real(DTSB3(:,1:3)) repelem(7, 7)'; real(DTSB4(:,1:3)) repelem(8, 5)';
             real(DTSC1(:,1:3)) repelem(9, 7)'; real(DTSC2(:,1:3)) repelem(10, 7)';
             real(DTSC3(:,1:3)) repelem(11, 7)';real(DTSD1(:,1:3)) repelem(12, 7)';
             real(DTSD2(:,1:3)) repelem(13, 7)'; real(DTSD3(:,1:3)) repelem(14, 7)';
             real(DTSD4(:,1:3)) repelem(15, 5)'];

%  resultTotDescrt = [real(DTSA1(:,1:5)) repelem(1, 7)'; real(DTSA2(:,1:5)) repelem(2, 7)';
%              real(DTSA3(:,1:5)) repelem(3, 7)';
%              real(DTSB1(:,1:5)) repelem(5, 7)'; real(DTSB2(:,1:5)) repelem(6, 7)';
%              real(DTSB3(:,1:5)) repelem(7, 7)';
%              real(DTSC1(:,1:5)) repelem(9, 7)'; real(DTSC2(:,1:5)) repelem(10, 7)';
%              real(DTSC3(:,1:5)) repelem(11, 7)';real(DTSD1(:,1:5)) repelem(12, 7)';
%              real(DTSD2(:,1:5)) repelem(13, 7)'; real(DTSD3(:,1:5)) repelem(14, 7)'];
         
% result = [];
% accuracyC = [];
% acc = [];
% 
% c = cvpartition(resultTotDescrt(:,1:1),'LeaveOut');
% 
%     for i = 1:1:c.NumTestSets
%         a = resultTotDescrt(i:i,1:end);
%         result = setdiff(resultTotDescrt, a, 'stable', 'rows');
% 
%         mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 3, 'Distance' , 'cosine');
%         res = predict(mdl, a(:,1:end-1));
%         
%         acc = [acc; res];
% 
%         class = a(:,end:end);
% 
%         accuracyC = [accuracyC; sum(class == res) / numel(class)];
%         %accuracyC = sum(class == res) / numel(class);
% 
%     end
% 
%     accuracyCAux = sum(accuracyC)./length(resultTotDescrt(:,1:1))
%     
% B = 2000;
% 
% ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student');
% 
% mediaMonolayerSPR = mean(ci);
% errorMonolayerSPR = ci(1) - mediaMonolayerSPR;
%          
% figure(6887)
% errorbar(mediaMonolayerSPR, errorMonolayerSPR, 'b');
% ylabel('Intervalo de Confian�a');