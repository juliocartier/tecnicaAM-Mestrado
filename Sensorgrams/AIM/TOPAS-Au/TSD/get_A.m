function [resultData, resultData1, resultData2, resultData3] = get_A(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = data;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 702, 741, 15);
result4 = get_region(result4Aux, 1094, 1149, 25);

result5 = get_region(data, 368, 387, 35);

result6Aux = get_region(data, 368, 387, 25);
result6AAux = get_region(result6Aux, 702, 741, 20);
result6 = get_region(result6AAux, 1094, 1149, 50);

result7 = result5 + 0.1;
%result7 = data + 3.5;
result7(1180) = result7(1180) + 0.2;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:863) - 0.5;

result11 = resultAux1;

[result22, result33] = get_create(resultAux1);

result44 = get_region(resultAux1, 165, 220, 15);
% result44 = get_region(result44Aux, 368, 387, 18);

result55 = get_region(resultAux1, 368, 387, 25);

result66Aux = get_region(resultAux1, 165, 220, 30);
result66 = get_region(result66Aux, 702, 741, 30);

result77 = result66 + 0.2;
result77(455) = result77(455) + 0.3;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result111 = data(1:462) - 1;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 165, 220, 25);

result555 = get_region(result111, 368, 387, 15);

result667 = get_region(result111, 165, 220, 35); 
% result667 = get_region(result667Aux, 368, 387, 20);

result777 = result333;
result777(250) = result777(250) + 0.3;

%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:259) + 1;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 165, 220, 30);

result5555 = result2222 + 0.2;
result5555(30) = result5555(30) + 0.5;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22+0.1 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end