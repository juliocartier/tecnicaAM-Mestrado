function [ result ] = get_DTSB( data )
%GET_DTSB Summary of this function goes here
%   Detailed explanation goes here

result = [complex(mean(data(1:187,1:1)), length(data(1:187,1:1)));
                   complex(mean(data(188:214,1:1)), length(data(188:214,1:1)));
                   complex(mean(data(215:351,1:1)), length(data(215:351,1:1)));
                   complex(mean(data(352:406,1:1)), length(data(352:406,1:1)));
                   complex(mean(data(407:514,1:1)), length(data(407:514,1:1)));
                   complex(mean(data(515:690,1:1)), length(data(515:690,1:1)));
                   complex(mean(data(691:902,1:1)), length(data(691:902,1:1)));
                   complex(mean(data(903:1046,1:1)), length(data(903:1046,1:1)));
                   complex(mean(data(1047:1197,1:1)), length(data(1047:1197,1:1)));
                   complex(mean(data(1198:1219,1:1)), length(data(1198:1219,1:1)));
                   complex(mean(data(1220:1373,1:1)), length(data(1220:1373,1:1)));
                   complex(mean(data(1374:1456,1:1)), length(data(1374:1456,1:1)));
                   complex(mean(data(1457:1555,1:1)), length(data(1457:1555,1:1)));
                   complex(mean(data(1556:1579,1:1)), length(data(1556:1579,1:1)));
                   complex(mean(data(1580:1850,1:1)), length(data(1580:1850,1:1)));
                   complex(mean(data(1851:1917,1:1)), length(data(1851:1917,1:1)));
                   complex(mean(data(1918:2069,1:1)), length(data(1918:2069,1:1)))];


end

