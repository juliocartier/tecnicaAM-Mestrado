function [resultData, resultData1, resultData2] = get_C(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
result1 = data - 2;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 703, 749, 40);
result4 = get_region(result4Aux, 1096, 1156, 50);

result5 = get_region(result1, 703, 749, 50);

result6Aux = get_region(result1, 177, 194, 25);
result6AAux = get_region(result6Aux, 703, 749, 50);
result6 = get_region(result6AAux, 1096, 1156, 50);

result7 = result1 - 0.2;
% result7(600) = 610;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:863) + 2;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 177, 194, 30);
result44 = get_region(result44Aux, 380, 435, 50);
 
result55 = get_region(result11, 117, 194, 40);
 
result66Aux = get_region(result11, 177, 194, 40);
result66 = get_region(result66Aux, 703, 749, 30);

result77 = result22 - 0.1;
result77(227) = result77(227) + 0.12;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:474) - 2.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 117, 194, 24);
 
result555 = get_region(result111, 380, 435, 30);
 
result667Aux = get_region(result111, 117, 194, 20);
result667 = get_region(result667Aux, 380, 435, 26);

result777 = result667 + 0.15;
result777(121) = result777(121) + 0.2;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];