function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2

result1 = data - 1;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 509, 548, 35);
result4 = get_region(result4Aux, 1096, 1149, 25);

result5 = get_region(result1, 767, 810, 25);

result6Aux = get_region(result1, 509, 548, 30);
result6AAux = get_region(result6Aux, 767, 810, 40);
result6 = get_region(result6AAux, 1096, 1149, 40);

result7 = result5 - 0.2;
result7(1200) = result7(1200) + 0.2;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:861) + 1.5;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 509, 548, 20);
result44 = get_region(result44Aux, 767, 810, 26);
 
result55 = get_region(result11, 175, 194, 35);

result66Aux = get_region(result11, 175, 194, 25);
result66 = get_region(result66Aux, 767, 810, 50);


result77 = result44;
result77(830) = result77(830) + 0.3;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:676) - 1.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 175, 194, 60);
 
result555 = get_region(result111, 509, 548, 15);

result667Aux = get_region(result111, 175, 194, 10);
result667 = get_region(result667Aux, 509, 548, 50);

result777 = result555 + 0.6;
result777(165) = result777(155) + 0.2;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:266) + 4;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 175, 194, 34);

result5555 = result1111 + 0.1;
result5555(230) = result5555(230) + 0.2;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
