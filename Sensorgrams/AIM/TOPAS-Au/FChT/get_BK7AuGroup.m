function [groupB, groupC, groupD] = get_BK7AuGroup(data)

groupB = [data(1:67); data(261:386); data(387:475); data(476:740); data(741:862);data(68:219); data(220:260); data(863:1148); data(1149:1236)]; 

groupC = [data(1:67); data(261:386); data(387:475); data(68:219); data(220:260); data(476:740); data(741:862); data(863:1148); data(1149:1236)];

groupD = [data(1:67); data(68:219); data(220:260); data(476:740); data(741:862); data(863:1148); data(1149:1236); data(261:386); data(387:475) + 0.0080];

groupB(669:676) = 68.4890;

groupD(1022:1022) = 68.4090;

