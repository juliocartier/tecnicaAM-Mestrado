function [resultData, resultData2, resultData3, resultData4] = get_D(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 4
result1 = data + 3.2;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 881, 941, 50);
result4 = get_region(result4Aux, 1131, 1148, 50);

result5 = get_region(result1, 488, 534, 35);

result6Aux = get_region(result1, 488, 534, 50);
result6AAux = get_region(result6Aux, 881, 941, 50);
result6 = get_region(result6AAux, 1131, 1131, 35);

result7 = result3 + 0.2;
result7(790) = result7(790) + 0.2;
%Fim dos Sensorgramas com o tempo todo do grupo 4

%Inicio dos Sensorgramas com o tempo 1557 do grupo 4
result11 = data(1:1024) - 3;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 488, 534, 40);
result44 = get_region(result44Aux, 881, 941, 30);

result55 = get_region(result11, 165, 220, 20);

result66Aux = get_region(result11, 165, 220, 50);
result66 = get_region(result66Aux, 881, 941, 30);

result77 = result66 + 0.3;
result77(200) = result77(200) + 0.2;
%Fim dos Sensorgramas com o tempo 1557 do grupo 4

%Inicio dos Sensorgramas do grupo 4 com o tempo 1028
result111 = data(1:647) + 3.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 165, 220, 50);

result555 = get_region(result111, 488, 534, 45);

result667Aux = get_region(result111, 165, 220, 40);
result667 = get_region(result667Aux, 488, 534, 35);

result777 = result111 - 0.6;
result777(408) = result777(408) + 0.2;

% Sensorgramas com 3 Regimes

sensorgram3R = [data(1:67); data(259:646)];

result1111 = sensorgram3R - 4.5;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 293, 348, 40);

result5555 = result2222 + 0.05;
result5555(192) = result5555(192) + 0.3;

%Fim dos Sensorgramas do grupo 4 com o tempo 1028

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData2 = [result11 result22-0.4 result33+0.3 result44 result55 result66 result77];

resultData3 = [result111 result222 result333 result444 result555 result667 result777];

resultData4 = [result1111 result2222 result3333 result4444 result5555];