close all;
clear all;
clc;

load sensorgramTOPASAu.txt;

%dataTOPASAu = [repmat(sensorgramTOPASAu(1), 120, 1); sensorgramTOPASAu(:, 1)];

[dataBK7Au_B, dataBK7Au_C, dataBK7Au_D] = get_BK7AuGroup(sensorgramTOPASAu);

% figure(952)
% plot(dataBK7Au_B);


[resultA1, resultA2, resultA3, resultA4] = get_A(sensorgramTOPASAu);

[resultB1, resultB2, resultB3, resultB4] = get_B(dataBK7Au_B);

[resultC1, resultC2, resultC3] = get_C(dataBK7Au_C);

[resultD1, resultD2, resultD3, resultD4] = get_D(dataBK7Au_D);

% figure(123)
% subplot(2,2,1)
% plot(resultA1(:,1:1), 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultA2(:,1:1) + 0.6, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultA3(:,1:1) + 1, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultA4(:,1:1) - 1, 'g')
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% 
% figure(1234)
% subplot(2,2,1)
% plot(resultB1(:,1:1) + 1, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultB2(:,1:1) - 1.5000, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultB3(:,1:1) + 1.500, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultB4(:,1:1) - 4, 'g');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% figure(12234)
% subplot(2,2,1)
% plot(resultC1(:,1:1) + 2, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultC2(:,1:1) - 2, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultC3(:,1:1) + 2.5000, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% 
% figure(122334)
% subplot(2,2,1)
% plot(resultD1(:,1:1) - 3.2000, 'b');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,2)
% plot(resultD2(:,1:1) + 3, 'r');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,3)
% plot(resultD3(:,1:1) - 3.5000, 'k');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');
% subplot(2,2,4)
% plot(resultD4(:,1:1) + 4.5000, 'g');
% xlabel('Tempo (s)');
% ylabel('\theta_r (�)');

resultWHTA1 = fwht(resultA1, 2048, 'hadamard')';
resultWHTA2 = fwht(resultA2, 1024, 'hadamard')';
resultWHTA3 = fwht(resultA3, 1024, 'hadamard')';
resultWHTA4 = fwht(resultA4, 512, 'hadamard')';

resultWHTB1 = fwht(resultB1, 2048, 'hadamard')';
resultWHTB2 = fwht(resultB2, 1024, 'hadamard')';
resultWHTB3 = fwht(resultB3, 1024, 'hadamard')';
resultWHTB4 = fwht(resultB4, 512, 'hadamard')';

resultWHTC1 = fwht(resultC1, 2048, 'hadamard')';
resultWHTC2 = fwht(resultC2, 1024, 'hadamard')';
resultWHTC3 = fwht(resultC3, 1024, 'hadamard')';

resultWHTD1 = fwht(resultD1, 2048, 'hadamard')';
resultWHTD2 = fwht(resultD2, 1024, 'hadamard')';
resultWHTD3 = fwht(resultD3, 1024, 'hadamard')';
resultWHTD4 = fwht(resultD4, 512, 'hadamard')';

tamanho = 4;

if tamanho <= 241

resultWHT = [abs(resultWHTA1(:,2:tamanho)) repelem(1,7)'; abs(resultWHTA2(:,2:tamanho)) repelem(2,7)';
             abs(resultWHTA3(:,2:tamanho)) repelem(3,7)'; abs(resultWHTA4(:,2:tamanho)) repelem(4,5)';
             abs(resultWHTB1(:,2:tamanho)) repelem(5,7)'; abs(resultWHTB2(:,2:tamanho)) repelem(6,7)';
             abs(resultWHTB3(:,2:tamanho)) repelem(7,7)'; abs(resultWHTB4(:,2:tamanho)) repelem(8,5)';
             abs(resultWHTC1(:,2:tamanho)) repelem(9,7)'; abs(resultWHTC2(:,2:tamanho)) repelem(10,7)';
             abs(resultWHTC3(:,2:tamanho)) repelem(11,7)'; abs(resultWHTD1(:,2:tamanho)) repelem(12,7)';
             abs(resultWHTD2(:,2:tamanho)) repelem(13,7)'; abs(resultWHTD3(:,2:tamanho)) repelem(14,7)';
             abs(resultWHTD4(:,2:tamanho)) repelem(15,5)']; 
         
end

figure(4532)
plot3(resultWHT(1:7,1:1), resultWHT(1:7,2:2), resultWHT(1:7,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(resultWHT(27:33,1:1), resultWHT(27:33,2:2), resultWHT(27:33,3:3), 'x', 'MarkerSize', 8, 'Color', [0.929; 0.694; 0.125]);
hold on;
plot3(resultWHT(53:59,1:1), resultWHT(53:59,2:2), resultWHT(53:59,3:3), 'v', 'MarkerSize', 8, 'Color', [1; 0; 1]);
hold on;
plot3(resultWHT(74:80,1:1), resultWHT(74:80,2:2), resultWHT(74:80,3:3), 'p', 'MarkerSize', 8, 'Color', [0.851; 0.325; 0.098]);
hold on;
plot3(resultWHT(8:14,1:1), resultWHT(8:14,2:2), resultWHT(8:14,3:3), 'ob', 'MarkerSize', 8);
hold on;
plot3(resultWHT(34:40,1:1), resultWHT(34:40,2:2), resultWHT(34:40,3:3), 's', 'MarkerSize', 8, 'Color', [0.204; 0.302; 0.494]);
hold on;
plot3(resultWHT(60:66,1:1), resultWHT(60:66,2:2), resultWHT(60:66,3:3), '>', 'MarkerSize', 8, 'Color', [0.635; 0.078; 0.184]);
hold on;
plot3(resultWHT(81:87,1:1), resultWHT(81:87,2:2), resultWHT(81:87,3:3), 'h', 'MarkerSize', 8, 'Color', [0.302; 0.745; 0.933]);
hold on;
plot3(resultWHT(15:21,1:1), resultWHT(15:21,2:2), resultWHT(15:21,3:3), '+g', 'MarkerSize', 8);
hold on;
plot3(resultWHT(41:47,1:1), resultWHT(41:47,2:2), resultWHT(41:47,3:3), 'd', 'MarkerSize', 8, 'Color', [0.231; 0.443; 0.337]);
hold on;
plot3(resultWHT(67:73,1:1), resultWHT(67:73,2:2), resultWHT(67:73,3:3), '<', 'MarkerSize', 8, 'Color', [0.4; 0; 0]);
hold on;
plot3(resultWHT(88:94,1:1), resultWHT(88:94,2:2), resultWHT(88:94,3:3), '.g', 'MarkerSize', 20, 'Color', [0.494; 0.184; 0.557]);
hold on;
plot3(resultWHT(22:26,1:1), resultWHT(22:26,2:2), resultWHT(22:26,3:3), '*k', 'MarkerSize', 8);
hold on;
plot3(resultWHT(48:52,1:1), resultWHT(48:52,2:2), resultWHT(48:52,3:3), '^', 'MarkerSize', 8, 'Color', [1; 1; 0]);
hold on;
plot3(resultWHT(95:99,1:1), resultWHT(95:99,2:2), resultWHT(95:99,3:3), 'x', 'MarkerSize', 8, 'Color', [1; 0.843; 0]);
xlabel('1� Coeficiente');
ylabel('2� Coeficiente');
zlabel('3� Coeficiente');
legend('Grupo 1', 'Grupo 2', 'Grupo 3', 'Grupo 4', 'Grupo 5', 'Grupo 6', 'Grupo 7', 'Grupo 8', ...
    'Grupo 9', 'Grupo 10', 'Grupo 11', 'Grupo 12', 'Grupo 13', 'Grupo 14', 'Grupo 15');

result = [];
accuracyC = [];

for i = 1:1:length(resultWHT(:,1:1))
    a = resultWHT(i:i,1:end);
    result = setdiff(resultWHT, a, 'stable', 'rows');
    
    mdl = fitcknn(result(:,1:end-1), result(:,end:end), 'NumNeighbors', 5, 'Distance' , 'cosine');
    res = predict(mdl, a(:,1:end-1));
    
    class = a(:,end:end);
    
%      [Train, Test] = crossvalind('LeaveMOut', res, 1);
  
    accuracyC = [accuracyC; sum(class == res) / numel(class)];
    

end

accuracy = sum(accuracyC)./length(resultWHT(:,1:1))

B = 2000;

ci = bootci(B, {@mean, accuracyC}, 'alpha', 0.05, 'type','student'); 

mediaFChT = mean(ci);
errorFChT = ci(1) - mediaFChT;
         
figure(6887)
errorbar(mediaFChT, errorFChT, 'b');
ylabel('Confidence Interval');
