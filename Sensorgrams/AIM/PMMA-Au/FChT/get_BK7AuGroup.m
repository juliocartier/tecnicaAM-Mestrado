function [groupB, groupC, groupD] = get_BK7AuGroup(data)

groupB = [data(1:183) - 0.0140; data(572:901); data(902:1051); data(1052:1618); data(1619:1817);data(184:504); data(505:571); data(1818:2334); data(2335:2658)]; 

groupC = [data(1:183) - 0.0140; data(572:901); data(902:1051); data(184:504); data(505:571); data(1052:1618); data(1619:1817); data(1818:2334); data(2335:2658)];

groupD = [data(1:183); data(184:504); data(505:571); data(1052:1618); data(1619:1817); data(1818:2334); data(2335:2658); data(572:901) - 0.0280; data(902:1051) - 0.0280];

groupB(1430:1434) = 67.9350;

%groupD(1742:1746) = 68.08;
