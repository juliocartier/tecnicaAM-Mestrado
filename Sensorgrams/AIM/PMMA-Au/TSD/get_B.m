function [resultData, resultData1, resultData2, resultData3] = get_B(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 2

result1 = data - 1;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 1112, 1296, 50);
result4 = get_region(result4Aux, 2042, 2335, 50);

result5 = get_region(result1, 1598, 1750, 50);

result6Aux = get_region(result1, 1112, 1280, 30);
result6AAux = get_region(result6Aux, 1598, 1750, 50);
result6 = get_region(result6AAux, 2042, 2335, 50);

result7 = result5 - 0.2;
result7(1500) = result7(1500) + 0.2;

%Fim dos Sensorgramas com o tempo todo do grupo 2

%Inicio dos Sensorgramas com o tempo 661 do grupo 2
result11 = data(1:1817) + 1.5;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 408, 514, 30);
result44 = get_region(result44Aux, 1598, 1750, 60);
 
result55 = get_region(result11, 1112, 1286, 60);

result66Aux = get_region(result11, 408, 514, 25);
result66 = get_region(result66Aux, 1112, 1286, 50);


result77 = result44;
result77(1509) = result77(1509) + 0.2;

%Fim dos Sensorgramas com o tempo 661 do grupo 2

%Inicio dos Sensorgramas com o tempo 448 do grupo 2
result111 = data(1:1434) - 1.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 408, 514, 30);
 
result555 = get_region(result111, 1112, 1286, 25);

result667Aux = get_region(result111, 408, 514, 20);
result667 = get_region(result667Aux, 1112, 1286, 45);

result777 = result555 + 0.6;
result777(572) = result777(572) + 0.3;
%Fim dos Sensorgramas com o tempo 448 do grupo 2

result1111 = data(1:664) + 4;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 408, 514, 40);

result5555 = result1111 + 0.1;
result5555(281) = result5555(281) + 0.2;

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];

end
