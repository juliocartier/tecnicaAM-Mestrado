function [resultData, resultData1, resultData2, resultData3] = get_A(data)

%Inicio dos Sensorgramas do grupo 1 com o tempo todo
result1 = data;

[result2, result3] = get_create(data);

result4Aux = get_region(data, 796, 902, 30);
result4 = get_region(result4Aux, 2042, 2335, 50);

result5 = get_region(data, 1500, 1619, 50);

result6Aux = get_region(data, 796, 902, 50);
result6AAux = get_region(result6Aux, 1500, 1619, 50);
result6 = get_region(result6AAux, 2042, 2335, 70);

result7 = result5 + 0.1;
%result7 = data + 3.5;
result7(1402) = result7(1402) + 0.1;

%Fim dos Sensorgramas do grupo 1 com o tempo todo

%Inicio dos Sensorgramas do grupo 1 com o tempo 661
resultAux1 = result2(1:1817) - 0.5;

result11 = resultAux1;

[result22, result33] = get_create(resultAux1);

result44Aux = get_region(resultAux1, 352, 504, 50);
result44 = get_region(result44Aux, 796, 902, 40);

result55 = get_region(resultAux1, 1500, 1619, 50);

result66Aux = get_region(resultAux1, 796, 902, 50);
result66 = get_region(result66Aux, 1500, 1619, 60);

result77 = result66 + 0.2;
result77(520) = result77(455) + 0.2;
%Fim dos Sensorgramas do grupo 1 com o tempo 661

%Inicio dos Sensorgramas do grupo 1 com o tempo 448
result111 = data(1:1051) - 1;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 352, 504, 25);

result555 = get_region(result111, 796, 902, 25);

result667Aux = get_region(result111, 352, 504, 35); 
result667 = get_region(result667Aux, 796, 902, 40);

result777 = result333;
result777(640) = result777(640) + 0.2;

%Fim dos Sensorgramas do grupo 1 com o tempo 448

result1111 = data(1:571) + 1;

[result2222, result3333] = get_create(result1111);

result4444 = get_region(result1111, 352, 504, 30);

result5555 = result2222 + 0.2;
result5555(90) = result5555(90) + 0.3;


resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22+0.1 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];

resultData3 = [result1111 result2222 result3333 result4444 result5555];
end