function [ result ] = get_DTSD( data )
%GET_INVDTS Summary of this function goes here
%   Detailed explanation goes here

result = [complex(mean(data(1:187,1:1)), length(data(1:187,1:1)));
          complex(mean(data(188:212,1:1)), length(data(188:212,1:1)));
          complex(mean(data(213:365,1:1)), length(data(213:365,1:1)));
          complex(mean(data(366:470,1:1)), length(data(366:470,1:1)));
          complex(mean(data(471:551,1:1)), length(data(471:551,1:1)));
          complex(mean(data(552:724,1:1)), length(data(552:724,1:1)));
          complex(mean(data(725:936,1:1)), length(data(725:936,1:1)));
          complex(mean(data(937:1035,1:1)), length(data(937:1035,1:1)));
          complex(mean(data(1036:1229,1:1)), length(data(1036:1229,1:1)));
          complex(mean(data(1230:1251,1:1)), length(data(1230:1251,1:1)));
          complex(mean(data(1252:1523,1:1)), length(data(1252:1523,1:1)));
          complex(mean(data(1524:1590,1:1)), length(data(1524:1590,1:1)));
          complex(mean(data(1591:1745,1:1)), length(data(1591:1745,1:1)));
          complex(mean(data(1746:1769,1:1)), length(data(1746:1769,1:1)));
          complex(mean(data(1770:1906,1:1)), length(data(1770:1906,1:1)));
          complex(mean(data(1907:1961,1:1)), length(data(1907:1961,1:1)));
          complex(mean(data(1962:2069,1:1)), length(data(1962:2069,1:1)))];

end

