function [resultData, resultData1, resultData2] = get_C(data)

%Inicio dos Sensorgramas com o tempo todo do grupo 3
result1 = data - 2;

[result2, result3] = get_create(result1);

result4Aux = get_region(result1, 1500, 1674, 40);
result4 = get_region(result4Aux, 2042, 2335, 50);

result5 = get_region(result1, 832, 971, 50);

result6Aux = get_region(result1, 832, 971, 50);
result6AAux = get_region(result6Aux, 1500, 1674, 50);
result6 = get_region(result6AAux, 2042, 2335, 50);

result7 = result1 - 0.2;
% result7(600) = 610;
%Fim dos Sensorgramas com o tempo todo do grupo 3

%Inicio dos Sensorgramas com o tempo 1557 do grupo 3
result11 = data(1:1818) + 2;

[result22, result33] = get_create(result11);

result44Aux = get_region(result11, 408, 514, 30);
result44 = get_region(result44Aux, 832, 971, 50);
 
result55 = get_region(result11, 1500, 1674, 70);
 
result66Aux = get_region(result11, 832, 971, 60);
result66 = get_region(result66Aux, 1500, 1674, 40);

result77 = result22 - 0.5;
result77(272) = result77(272) + 0.4;
%Fim dos Sensorgramas com o tempo 1557 do grupo 3

%Inicio dos Sensorgramas com o tempo 1028 do grupo 3
result111 = data(1:1056) - 2.5;

[result222, result333] = get_create(result111);

result444 = get_region(result111, 408, 514, 40);
 
result555 = get_region(result111, 832, 971, 30);
 
result667Aux = get_region(result111, 408, 514, 20);
result667 = get_region(result667Aux, 832, 971, 40);

result777 = result667 + 0.15;
result777(90) = result777(90) + 0.2;
%Fim dos Sensorgramas com o tempo 1028 do grupo 3

resultData = [result1 result2 result3 result4 result5 result6 result7];

resultData1 = [result11 result22 result33 result44 result55 result66 result77];

resultData2 = [result111 result222 result333 result444 result555 result667 result777];