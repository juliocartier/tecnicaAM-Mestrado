function [ result] = verificaTamanho( data, winSize)
% Esse algoritmo faz o inverso do outro
% algoritmo em que verifica o tamanho.
% Ele converte o numero complexo da parte real
% para armazenar no vetor em colunas e linhas.

result = zeros(length(data(:,1:1)), winSize);

for i = 1:1:winSize
   for j = 1:1:length(data(1:1,:))         
       result(i,j) = real(data(i,j));
   end
end

result = result(:,1:1:end);

end

